package blue.dao.utils;

import blue.erp.model.BlueStudent;
import blue.erp.model.BlueTeacher;
import blue.erp.model.SysUser;
import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;

public class SysUserUtils  implements Serializable {

    public static BlueTeacher convert2Teacher(SysUser sysUser){

        if(sysUser==null){
            return null;
        }

        BlueTeacher teacher=JSONObject.parseObject(JSONObject.toJSONString(sysUser),BlueTeacher.class);
        teacher.setId(null);
        teacher.setUserid(sysUser.getId());
        teacher.setClassids(sysUser.getClassIds());
        teacher.setGradeids(sysUser.getGroupIds());
        teacher.setCourseids(sysUser.getCourseIds());
        return teacher;
    }

    public static BlueStudent convert2Student(SysUser sysUser){

        if(sysUser==null){
            return null;
        }

        BlueStudent student=JSONObject.parseObject(JSONObject.toJSONString(sysUser),BlueStudent.class);
        student.setId(null);
        student.setUserid(sysUser.getId());
        return student;

    }

}