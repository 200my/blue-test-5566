package blue.dao.uploadexcel;

import java.util.HashMap;

public class ExcelVirtualGroup extends ExcelBase {

    private String userno;
    /**
     * 用户加入的组织，比如学生分层划分，教师组的划分
     */
    private String groupIds;

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(String groupIds) {
        this.groupIds = groupIds;
    }

    public HashMap<String,String> aliasMappint(){

        HashMap<String,String> aliasMapping=new HashMap();

        aliasMapping.put("学号(必填)","userno");
        aliasMapping.put("虚拟班组(必填)","groupIds");
        return aliasMapping;
    }


    @Override
    public Long getId() {
        return null;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setStatus(Byte status) {

    }
}
