package blue.dao.uploadexcel;

import blue.dao.base.BaseModel;
import cn.hutool.core.convert.Convert;
import cn.hutool.poi.excel.ExcelReader;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ExcelBase extends BaseModel {


    public abstract HashMap<String,String> aliasMappint();

    public ExcelReader convertAlias2Db(ExcelReader excelReader){

         if(this.aliasMappint()!=null){
             excelReader.setHeaderAlias(this.aliasMappint());
         }

         return excelReader;
    }

    public <T extends Object> List convertExcel2Object(ExcelReader excelReader,Class<T> objectClass){

        excelReader=convertAlias2Db(excelReader);
        List<Map<String, Object>> excelData=excelReader.readAll();
        if(CollectionUtils.isEmpty(excelData)){
            return null;
        }

        return JSONArray.parseArray(Convert.toDBC(JSONObject.toJSONString(excelData)),objectClass);

    }


}
