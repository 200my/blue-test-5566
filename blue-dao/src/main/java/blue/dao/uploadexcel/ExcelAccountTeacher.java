package blue.dao.uploadexcel;

import java.util.HashMap;

public class ExcelAccountTeacher extends ExcelBase {

    private String realName;
    private String userno;
    private String grade;
    private String gradeName;
    private String className;
    private String sex;
    private String mobile;
    private String email;
    private String courseNames;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCourseNames() {
        return courseNames;
    }

    public void setCourseNames(String courseNames) {
        this.courseNames = courseNames;
    }

    public HashMap<String,String> aliasMappint(){

        HashMap<String,String> aliasMapping=new HashMap();

        aliasMapping.put("姓名(必填)","realName");
        aliasMapping.put("教师编号(必填)","userno");
        aliasMapping.put("年级名称","gradeName");
        aliasMapping.put("年级","grade");
        aliasMapping.put("班级","className");
        aliasMapping.put("任课","courseNames");
        aliasMapping.put("性别","sex");
        aliasMapping.put("手机号","mobile");
        aliasMapping.put("邮箱","email");

        return aliasMapping;
    }


    @Override
    public Long getId() {
        return null;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setStatus(Byte status) {

    }
}
