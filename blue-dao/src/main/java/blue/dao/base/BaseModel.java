package blue.dao.base;

import java.io.Serializable;
import java.util.Date;

abstract public class BaseModel <T extends Object> implements Serializable {


    private Date updateTime;

    private Date createTime;

    private Long schoolid;

    abstract public Long getId();

    abstract public void setId(Long id);

    abstract public void setStatus(Byte status);

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }
}
