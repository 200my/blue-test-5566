package blue.dao.base;

import java.util.List;

abstract public class BaseGradeModel extends BaseModel {

    //单个场景下的名称
    private String gradeName;

    //列表场景
    private List gradeNameList;

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public List getGradeNameList() {
        return gradeNameList;
    }

    public void setGradeNameList(List gradeNameList) {
        this.gradeNameList = gradeNameList;
    }
}
