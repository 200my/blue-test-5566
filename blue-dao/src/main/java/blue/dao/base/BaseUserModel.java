package blue.dao.base;

import blue.commons.models.KeyValue;

import java.util.List;

abstract public class BaseUserModel extends BaseModel {

    //列表场景
    private List userNameList;

    private String courseIds;

    private List<KeyValue> roles;

    private List<KeyValue> grades;

    private List<KeyValue> classes;

    private List<KeyValue> groups;


    public List getUserNameList() {
        return userNameList;
    }

    public void setUserNameList(List userNameList) {
        this.userNameList = userNameList;
    }


    public String getCourseIds() {
        return courseIds;
    }

    public void setCourseIds(String courseIds) {
        this.courseIds = courseIds;
    }

    public List<KeyValue> getRoles() {
        return roles;
    }

    public void setRoles(List<KeyValue> roles) {
        this.roles = roles;
    }

    public List<KeyValue> getGrades() {
        return grades;
    }

    public void setGrades(List<KeyValue> grades) {
        this.grades = grades;
    }

    public List<KeyValue> getClasses() {
        return classes;
    }

    public void setClasses(List<KeyValue> classes) {
        this.classes = classes;
    }

    public List<KeyValue> getGroups() {
        return groups;
    }

    public void setGroups(List<KeyValue> groups) {
        this.groups = groups;
    }
}
