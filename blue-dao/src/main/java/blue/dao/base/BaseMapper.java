package blue.dao.base;


import java.util.List;

public interface BaseMapper<T extends BaseModel> {

    int updateByPrimaryKeySelective(T record);

    int deleteByPrimaryKey(Object id);

    int deleteByExample(Object example);

    int insert(T record);

    int insertSelective(T record);

    T selectByPrimaryKey(Object id);

    int updateByPrimaryKey(T record);

    List<T> selectByExample(Object example);

}