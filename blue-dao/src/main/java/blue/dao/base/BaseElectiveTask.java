package blue.dao.base;

import blue.erp.model.BlueCourse;

import java.util.List;

abstract public class BaseElectiveTask extends BaseModel{

    private List<BlueCourse> list;//课程列表

    private String gradeType;//年级类型

    public List<BlueCourse> getList() {
        return list;
    }

    public void setList(List<BlueCourse> list) {
        this.list = list;
    }

    public String getGradeType() {
        return gradeType;
    }

    public void setGradeType(String gradeType) {
        this.gradeType = gradeType;
    }
}
