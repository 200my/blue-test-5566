package blue.dao.base;

import blue.erp.model.BlueCourse;

import java.util.List;

abstract public class BaseCourseModel extends BaseClassModel {

    //单个course场景下的名称
    private String courseName;

    //course列表场景
    private  List<BlueCourse> courseNameList;

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public  List<BlueCourse> getCourseNameList() {
        return courseNameList;
    }

    public void setCourseNameList(List<BlueCourse> courseNameList) {
        this.courseNameList = courseNameList;
    }
}
