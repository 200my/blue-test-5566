package blue.dao.base;

import blue.commons.models.KeyValue;

import java.util.List;

abstract public class BaseClassModel extends BaseGradeModel {

    //单个场景下的名称
    private String className;

    //列表场景
    private List classNameList;

    private List<KeyValue> classes;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List getClassNameList() {
        return classNameList;
    }

    public void setClassNameList(List classNameList) {
        this.classNameList = classNameList;
    }

    public List<KeyValue> getClasses() {
        return classes;
    }

    public void setClasses(List<KeyValue> classes) {
        this.classes = classes;
    }
}
