package blue.splitclass.dao;

import blue.dao.base.BaseMapper;
import blue.splitclass.model.SplitClassGroup;
import blue.splitclass.model.SplitClassGroupExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SplitClassGroupMapper extends BaseMapper {
    long countByExample(SplitClassGroupExample example);

    int deleteByExample(SplitClassGroupExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SplitClassGroup record);

    int insertSelective(SplitClassGroup record);

    List<SplitClassGroup> selectByExample(SplitClassGroupExample example);

    SplitClassGroup selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SplitClassGroup record, @Param("example") SplitClassGroupExample example);

    int updateByExample(@Param("record") SplitClassGroup record, @Param("example") SplitClassGroupExample example);

    int updateByPrimaryKeySelective(SplitClassGroup record);

    int updateByPrimaryKey(SplitClassGroup record);
}