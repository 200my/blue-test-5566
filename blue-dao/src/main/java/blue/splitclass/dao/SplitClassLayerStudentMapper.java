package blue.splitclass.dao;

import blue.dao.base.BaseMapper;
import blue.splitclass.model.SplitClassLayerStudent;
import blue.splitclass.model.SplitClassLayerStudentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SplitClassLayerStudentMapper extends BaseMapper {
    long countByExample(SplitClassLayerStudentExample example);

    int deleteByExample(SplitClassLayerStudentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SplitClassLayerStudent record);

    int insertSelective(SplitClassLayerStudent record);

    List<SplitClassLayerStudent> selectByExample(SplitClassLayerStudentExample example);

    SplitClassLayerStudent selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SplitClassLayerStudent record, @Param("example") SplitClassLayerStudentExample example);

    int updateByExample(@Param("record") SplitClassLayerStudent record, @Param("example") SplitClassLayerStudentExample example);

    int updateByPrimaryKeySelective(SplitClassLayerStudent record);

    int updateByPrimaryKey(SplitClassLayerStudent record);
}