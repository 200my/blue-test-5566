package blue.splitclass.dao;

import blue.dao.base.BaseMapper;
import blue.splitclass.model.SplitClassLayerInfo;
import blue.splitclass.model.SplitClassLayerInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SplitClassLayerInfoMapper extends BaseMapper {
    long countByExample(SplitClassLayerInfoExample example);

    int deleteByExample(SplitClassLayerInfoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SplitClassLayerInfo record);

    int insertSelective(SplitClassLayerInfo record);

    List<SplitClassLayerInfo> selectByExample(SplitClassLayerInfoExample example);

    SplitClassLayerInfo selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SplitClassLayerInfo record, @Param("example") SplitClassLayerInfoExample example);

    int updateByExample(@Param("record") SplitClassLayerInfo record, @Param("example") SplitClassLayerInfoExample example);

    int updateByPrimaryKeySelective(SplitClassLayerInfo record);

    int updateByPrimaryKey(SplitClassLayerInfo record);
}