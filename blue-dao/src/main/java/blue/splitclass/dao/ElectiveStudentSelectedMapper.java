package blue.splitclass.dao;

import blue.dao.base.BaseMapper;
import blue.splitclass.model.ElectiveStudentSelected;
import blue.splitclass.model.ElectiveStudentSelectedExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ElectiveStudentSelectedMapper extends BaseMapper {
    long countByExample(ElectiveStudentSelectedExample example);

    int deleteByExample(ElectiveStudentSelectedExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ElectiveStudentSelected record);

    int insertSelective(ElectiveStudentSelected record);

    List<ElectiveStudentSelected> selectByExample(ElectiveStudentSelectedExample example);

    ElectiveStudentSelected selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ElectiveStudentSelected record, @Param("example") ElectiveStudentSelectedExample example);

    int updateByExample(@Param("record") ElectiveStudentSelected record, @Param("example") ElectiveStudentSelectedExample example);

    int updateByPrimaryKeySelective(ElectiveStudentSelected record);

    int updateByPrimaryKey(ElectiveStudentSelected record);
}