package blue.splitclass.dao;

import blue.dao.base.BaseMapper;
import blue.splitclass.model.ElectiveTask;
import blue.splitclass.model.ElectiveTaskExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ElectiveTaskMapper extends BaseMapper {
    long countByExample(ElectiveTaskExample example);

    int deleteByExample(ElectiveTaskExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ElectiveTask record);

    int insertSelective(ElectiveTask record);

    List<ElectiveTask> selectByExample(ElectiveTaskExample example);

    ElectiveTask selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ElectiveTask record, @Param("example") ElectiveTaskExample example);

    int updateByExample(@Param("record") ElectiveTask record, @Param("example") ElectiveTaskExample example);

    int updateByPrimaryKeySelective(ElectiveTask record);

    int updateByPrimaryKey(ElectiveTask record);
}