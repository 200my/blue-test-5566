package blue.splitclass.dao;

import blue.dao.base.BaseMapper;
import blue.splitclass.model.ElectiveRule;
import blue.splitclass.model.ElectiveRuleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ElectiveRuleMapper extends BaseMapper {
    long countByExample(ElectiveRuleExample example);

    int deleteByExample(ElectiveRuleExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ElectiveRule record);

    int insertSelective(ElectiveRule record);

    List<ElectiveRule> selectByExample(ElectiveRuleExample example);

    ElectiveRule selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ElectiveRule record, @Param("example") ElectiveRuleExample example);

    int updateByExample(@Param("record") ElectiveRule record, @Param("example") ElectiveRuleExample example);

    int updateByPrimaryKeySelective(ElectiveRule record);

    int updateByPrimaryKey(ElectiveRule record);
}