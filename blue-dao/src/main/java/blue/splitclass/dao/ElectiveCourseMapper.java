package blue.splitclass.dao;

import blue.dao.base.BaseMapper;
import blue.splitclass.model.ElectiveCourse;
import blue.splitclass.model.ElectiveCourseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ElectiveCourseMapper extends BaseMapper {
    long countByExample(ElectiveCourseExample example);

    int deleteByExample(ElectiveCourseExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ElectiveCourse record);

    int insertSelective(ElectiveCourse record);

    List<ElectiveCourse> selectByExample(ElectiveCourseExample example);

    ElectiveCourse selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ElectiveCourse record, @Param("example") ElectiveCourseExample example);

    int updateByExample(@Param("record") ElectiveCourse record, @Param("example") ElectiveCourseExample example);

    int updateByPrimaryKeySelective(ElectiveCourse record);

    int updateByPrimaryKey(ElectiveCourse record);
}