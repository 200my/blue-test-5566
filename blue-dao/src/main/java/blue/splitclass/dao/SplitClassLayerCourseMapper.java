package blue.splitclass.dao;

import blue.dao.base.BaseMapper;
import blue.splitclass.model.SplitClassLayerCourse;
import blue.splitclass.model.SplitClassLayerCourseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SplitClassLayerCourseMapper extends BaseMapper {
    long countByExample(SplitClassLayerCourseExample example);

    int deleteByExample(SplitClassLayerCourseExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SplitClassLayerCourse record);

    int insertSelective(SplitClassLayerCourse record);

    List<SplitClassLayerCourse> selectByExample(SplitClassLayerCourseExample example);

    SplitClassLayerCourse selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SplitClassLayerCourse record, @Param("example") SplitClassLayerCourseExample example);

    int updateByExample(@Param("record") SplitClassLayerCourse record, @Param("example") SplitClassLayerCourseExample example);

    int updateByPrimaryKeySelective(SplitClassLayerCourse record);

    int updateByPrimaryKey(SplitClassLayerCourse record);
}