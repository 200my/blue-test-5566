package blue.splitclass.dao;

import blue.dao.base.BaseMapper;
import blue.splitclass.model.ElectivePublishCourse;
import blue.splitclass.model.ElectivePublishCourseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ElectivePublishCourseMapper extends BaseMapper {
    long countByExample(ElectivePublishCourseExample example);

    int deleteByExample(ElectivePublishCourseExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ElectivePublishCourse record);

    int insertSelective(ElectivePublishCourse record);

    List<ElectivePublishCourse> selectByExample(ElectivePublishCourseExample example);

    ElectivePublishCourse selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ElectivePublishCourse record, @Param("example") ElectivePublishCourseExample example);

    int updateByExample(@Param("record") ElectivePublishCourse record, @Param("example") ElectivePublishCourseExample example);

    int updateByPrimaryKeySelective(ElectivePublishCourse record);

    int updateByPrimaryKey(ElectivePublishCourse record);
}