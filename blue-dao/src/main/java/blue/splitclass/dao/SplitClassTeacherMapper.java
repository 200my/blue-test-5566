package blue.splitclass.dao;

import blue.dao.base.BaseMapper;
import blue.splitclass.model.SplitClassTeacher;
import blue.splitclass.model.SplitClassTeacherExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SplitClassTeacherMapper extends BaseMapper {
    long countByExample(SplitClassTeacherExample example);

    int deleteByExample(SplitClassTeacherExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SplitClassTeacher record);

    int insertSelective(SplitClassTeacher record);

    List<SplitClassTeacher> selectByExample(SplitClassTeacherExample example);

    SplitClassTeacher selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SplitClassTeacher record, @Param("example") SplitClassTeacherExample example);

    int updateByExample(@Param("record") SplitClassTeacher record, @Param("example") SplitClassTeacherExample example);

    int updateByPrimaryKeySelective(SplitClassTeacher record);

    int updateByPrimaryKey(SplitClassTeacher record);
}