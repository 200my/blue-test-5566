package blue.splitclass.dao;

import blue.dao.base.BaseMapper;
import blue.splitclass.model.SplitClassTime;
import blue.splitclass.model.SplitClassTimeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SplitClassTimeMapper extends BaseMapper {
    long countByExample(SplitClassTimeExample example);

    int deleteByExample(SplitClassTimeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SplitClassTime record);

    int insertSelective(SplitClassTime record);

    List<SplitClassTime> selectByExample(SplitClassTimeExample example);

    SplitClassTime selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SplitClassTime record, @Param("example") SplitClassTimeExample example);

    int updateByExample(@Param("record") SplitClassTime record, @Param("example") SplitClassTimeExample example);

    int updateByPrimaryKeySelective(SplitClassTime record);

    int updateByPrimaryKey(SplitClassTime record);
}