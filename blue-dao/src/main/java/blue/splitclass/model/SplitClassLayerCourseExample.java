package blue.splitclass.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SplitClassLayerCourseExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SplitClassLayerCourseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Long value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Long value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Long value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Long value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Long value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Long value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Long> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Long> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Long value1, Long value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Long value1, Long value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andTaskIdIsNull() {
            addCriterion("task_id is null");
            return (Criteria) this;
        }

        public Criteria andTaskIdIsNotNull() {
            addCriterion("task_id is not null");
            return (Criteria) this;
        }

        public Criteria andTaskIdEqualTo(Long value) {
            addCriterion("task_id =", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdNotEqualTo(Long value) {
            addCriterion("task_id <>", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdGreaterThan(Long value) {
            addCriterion("task_id >", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdGreaterThanOrEqualTo(Long value) {
            addCriterion("task_id >=", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdLessThan(Long value) {
            addCriterion("task_id <", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdLessThanOrEqualTo(Long value) {
            addCriterion("task_id <=", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdIn(List<Long> values) {
            addCriterion("task_id in", values, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdNotIn(List<Long> values) {
            addCriterion("task_id not in", values, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdBetween(Long value1, Long value2) {
            addCriterion("task_id between", value1, value2, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdNotBetween(Long value1, Long value2) {
            addCriterion("task_id not between", value1, value2, "taskId");
            return (Criteria) this;
        }

        public Criteria andLayerCountIsNull() {
            addCriterion("layer_count is null");
            return (Criteria) this;
        }

        public Criteria andLayerCountIsNotNull() {
            addCriterion("layer_count is not null");
            return (Criteria) this;
        }

        public Criteria andLayerCountEqualTo(Integer value) {
            addCriterion("layer_count =", value, "layerCount");
            return (Criteria) this;
        }

        public Criteria andLayerCountNotEqualTo(Integer value) {
            addCriterion("layer_count <>", value, "layerCount");
            return (Criteria) this;
        }

        public Criteria andLayerCountGreaterThan(Integer value) {
            addCriterion("layer_count >", value, "layerCount");
            return (Criteria) this;
        }

        public Criteria andLayerCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("layer_count >=", value, "layerCount");
            return (Criteria) this;
        }

        public Criteria andLayerCountLessThan(Integer value) {
            addCriterion("layer_count <", value, "layerCount");
            return (Criteria) this;
        }

        public Criteria andLayerCountLessThanOrEqualTo(Integer value) {
            addCriterion("layer_count <=", value, "layerCount");
            return (Criteria) this;
        }

        public Criteria andLayerCountIn(List<Integer> values) {
            addCriterion("layer_count in", values, "layerCount");
            return (Criteria) this;
        }

        public Criteria andLayerCountNotIn(List<Integer> values) {
            addCriterion("layer_count not in", values, "layerCount");
            return (Criteria) this;
        }

        public Criteria andLayerCountBetween(Integer value1, Integer value2) {
            addCriterion("layer_count between", value1, value2, "layerCount");
            return (Criteria) this;
        }

        public Criteria andLayerCountNotBetween(Integer value1, Integer value2) {
            addCriterion("layer_count not between", value1, value2, "layerCount");
            return (Criteria) this;
        }

        public Criteria andLayerConfigIsNull() {
            addCriterion("layer_config is null");
            return (Criteria) this;
        }

        public Criteria andLayerConfigIsNotNull() {
            addCriterion("layer_config is not null");
            return (Criteria) this;
        }

        public Criteria andLayerConfigEqualTo(String value) {
            addCriterion("layer_config =", value, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerConfigNotEqualTo(String value) {
            addCriterion("layer_config <>", value, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerConfigGreaterThan(String value) {
            addCriterion("layer_config >", value, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerConfigGreaterThanOrEqualTo(String value) {
            addCriterion("layer_config >=", value, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerConfigLessThan(String value) {
            addCriterion("layer_config <", value, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerConfigLessThanOrEqualTo(String value) {
            addCriterion("layer_config <=", value, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerConfigLike(String value) {
            addCriterion("layer_config like", value, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerConfigNotLike(String value) {
            addCriterion("layer_config not like", value, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerConfigIn(List<String> values) {
            addCriterion("layer_config in", values, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerConfigNotIn(List<String> values) {
            addCriterion("layer_config not in", values, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerConfigBetween(String value1, String value2) {
            addCriterion("layer_config between", value1, value2, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerConfigNotBetween(String value1, String value2) {
            addCriterion("layer_config not between", value1, value2, "layerConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigIsNull() {
            addCriterion("layer_result_config is null");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigIsNotNull() {
            addCriterion("layer_result_config is not null");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigEqualTo(String value) {
            addCriterion("layer_result_config =", value, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigNotEqualTo(String value) {
            addCriterion("layer_result_config <>", value, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigGreaterThan(String value) {
            addCriterion("layer_result_config >", value, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigGreaterThanOrEqualTo(String value) {
            addCriterion("layer_result_config >=", value, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigLessThan(String value) {
            addCriterion("layer_result_config <", value, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigLessThanOrEqualTo(String value) {
            addCriterion("layer_result_config <=", value, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigLike(String value) {
            addCriterion("layer_result_config like", value, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigNotLike(String value) {
            addCriterion("layer_result_config not like", value, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigIn(List<String> values) {
            addCriterion("layer_result_config in", values, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigNotIn(List<String> values) {
            addCriterion("layer_result_config not in", values, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigBetween(String value1, String value2) {
            addCriterion("layer_result_config between", value1, value2, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andLayerResultConfigNotBetween(String value1, String value2) {
            addCriterion("layer_result_config not between", value1, value2, "layerResultConfig");
            return (Criteria) this;
        }

        public Criteria andSelectedCountIsNull() {
            addCriterion("selected_count is null");
            return (Criteria) this;
        }

        public Criteria andSelectedCountIsNotNull() {
            addCriterion("selected_count is not null");
            return (Criteria) this;
        }

        public Criteria andSelectedCountEqualTo(Integer value) {
            addCriterion("selected_count =", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountNotEqualTo(Integer value) {
            addCriterion("selected_count <>", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountGreaterThan(Integer value) {
            addCriterion("selected_count >", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("selected_count >=", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountLessThan(Integer value) {
            addCriterion("selected_count <", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountLessThanOrEqualTo(Integer value) {
            addCriterion("selected_count <=", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountIn(List<Integer> values) {
            addCriterion("selected_count in", values, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountNotIn(List<Integer> values) {
            addCriterion("selected_count not in", values, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountBetween(Integer value1, Integer value2) {
            addCriterion("selected_count between", value1, value2, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountNotBetween(Integer value1, Integer value2) {
            addCriterion("selected_count not between", value1, value2, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountIsNull() {
            addCriterion("selected_male_count is null");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountIsNotNull() {
            addCriterion("selected_male_count is not null");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountEqualTo(Integer value) {
            addCriterion("selected_male_count =", value, "selectedMaleCount");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountNotEqualTo(Integer value) {
            addCriterion("selected_male_count <>", value, "selectedMaleCount");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountGreaterThan(Integer value) {
            addCriterion("selected_male_count >", value, "selectedMaleCount");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("selected_male_count >=", value, "selectedMaleCount");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountLessThan(Integer value) {
            addCriterion("selected_male_count <", value, "selectedMaleCount");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountLessThanOrEqualTo(Integer value) {
            addCriterion("selected_male_count <=", value, "selectedMaleCount");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountIn(List<Integer> values) {
            addCriterion("selected_male_count in", values, "selectedMaleCount");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountNotIn(List<Integer> values) {
            addCriterion("selected_male_count not in", values, "selectedMaleCount");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountBetween(Integer value1, Integer value2) {
            addCriterion("selected_male_count between", value1, value2, "selectedMaleCount");
            return (Criteria) this;
        }

        public Criteria andSelectedMaleCountNotBetween(Integer value1, Integer value2) {
            addCriterion("selected_male_count not between", value1, value2, "selectedMaleCount");
            return (Criteria) this;
        }

        public Criteria andExtendIsNull() {
            addCriterion("extend is null");
            return (Criteria) this;
        }

        public Criteria andExtendIsNotNull() {
            addCriterion("extend is not null");
            return (Criteria) this;
        }

        public Criteria andExtendEqualTo(String value) {
            addCriterion("extend =", value, "extend");
            return (Criteria) this;
        }

        public Criteria andExtendNotEqualTo(String value) {
            addCriterion("extend <>", value, "extend");
            return (Criteria) this;
        }

        public Criteria andExtendGreaterThan(String value) {
            addCriterion("extend >", value, "extend");
            return (Criteria) this;
        }

        public Criteria andExtendGreaterThanOrEqualTo(String value) {
            addCriterion("extend >=", value, "extend");
            return (Criteria) this;
        }

        public Criteria andExtendLessThan(String value) {
            addCriterion("extend <", value, "extend");
            return (Criteria) this;
        }

        public Criteria andExtendLessThanOrEqualTo(String value) {
            addCriterion("extend <=", value, "extend");
            return (Criteria) this;
        }

        public Criteria andExtendLike(String value) {
            addCriterion("extend like", value, "extend");
            return (Criteria) this;
        }

        public Criteria andExtendNotLike(String value) {
            addCriterion("extend not like", value, "extend");
            return (Criteria) this;
        }

        public Criteria andExtendIn(List<String> values) {
            addCriterion("extend in", values, "extend");
            return (Criteria) this;
        }

        public Criteria andExtendNotIn(List<String> values) {
            addCriterion("extend not in", values, "extend");
            return (Criteria) this;
        }

        public Criteria andExtendBetween(String value1, String value2) {
            addCriterion("extend between", value1, value2, "extend");
            return (Criteria) this;
        }

        public Criteria andExtendNotBetween(String value1, String value2) {
            addCriterion("extend not between", value1, value2, "extend");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNull() {
            addCriterion("operator is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNotNull() {
            addCriterion("operator is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorEqualTo(String value) {
            addCriterion("operator =", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotEqualTo(String value) {
            addCriterion("operator <>", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThan(String value) {
            addCriterion("operator >", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThanOrEqualTo(String value) {
            addCriterion("operator >=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThan(String value) {
            addCriterion("operator <", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThanOrEqualTo(String value) {
            addCriterion("operator <=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLike(String value) {
            addCriterion("operator like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotLike(String value) {
            addCriterion("operator not like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorIn(List<String> values) {
            addCriterion("operator in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotIn(List<String> values) {
            addCriterion("operator not in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorBetween(String value1, String value2) {
            addCriterion("operator between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotBetween(String value1, String value2) {
            addCriterion("operator not between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andSchoolidIsNull() {
            addCriterion("schoolId is null");
            return (Criteria) this;
        }

        public Criteria andSchoolidIsNotNull() {
            addCriterion("schoolId is not null");
            return (Criteria) this;
        }

        public Criteria andSchoolidEqualTo(Long value) {
            addCriterion("schoolId =", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotEqualTo(Long value) {
            addCriterion("schoolId <>", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidGreaterThan(Long value) {
            addCriterion("schoolId >", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidGreaterThanOrEqualTo(Long value) {
            addCriterion("schoolId >=", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidLessThan(Long value) {
            addCriterion("schoolId <", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidLessThanOrEqualTo(Long value) {
            addCriterion("schoolId <=", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidIn(List<Long> values) {
            addCriterion("schoolId in", values, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotIn(List<Long> values) {
            addCriterion("schoolId not in", values, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidBetween(Long value1, Long value2) {
            addCriterion("schoolId between", value1, value2, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotBetween(Long value1, Long value2) {
            addCriterion("schoolId not between", value1, value2, "schoolid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}