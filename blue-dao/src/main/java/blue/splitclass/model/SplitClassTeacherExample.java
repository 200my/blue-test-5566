package blue.splitclass.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SplitClassTeacherExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SplitClassTeacherExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCourseNameIsNull() {
            addCriterion("course_name is null");
            return (Criteria) this;
        }

        public Criteria andCourseNameIsNotNull() {
            addCriterion("course_name is not null");
            return (Criteria) this;
        }

        public Criteria andCourseNameEqualTo(String value) {
            addCriterion("course_name =", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotEqualTo(String value) {
            addCriterion("course_name <>", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameGreaterThan(String value) {
            addCriterion("course_name >", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameGreaterThanOrEqualTo(String value) {
            addCriterion("course_name >=", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLessThan(String value) {
            addCriterion("course_name <", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLessThanOrEqualTo(String value) {
            addCriterion("course_name <=", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLike(String value) {
            addCriterion("course_name like", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotLike(String value) {
            addCriterion("course_name not like", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameIn(List<String> values) {
            addCriterion("course_name in", values, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotIn(List<String> values) {
            addCriterion("course_name not in", values, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameBetween(String value1, String value2) {
            addCriterion("course_name between", value1, value2, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotBetween(String value1, String value2) {
            addCriterion("course_name not between", value1, value2, "courseName");
            return (Criteria) this;
        }

        public Criteria andSelectedCountIsNull() {
            addCriterion("selected_count is null");
            return (Criteria) this;
        }

        public Criteria andSelectedCountIsNotNull() {
            addCriterion("selected_count is not null");
            return (Criteria) this;
        }

        public Criteria andSelectedCountEqualTo(Long value) {
            addCriterion("selected_count =", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountNotEqualTo(Long value) {
            addCriterion("selected_count <>", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountGreaterThan(Long value) {
            addCriterion("selected_count >", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountGreaterThanOrEqualTo(Long value) {
            addCriterion("selected_count >=", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountLessThan(Long value) {
            addCriterion("selected_count <", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountLessThanOrEqualTo(Long value) {
            addCriterion("selected_count <=", value, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountIn(List<Long> values) {
            addCriterion("selected_count in", values, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountNotIn(List<Long> values) {
            addCriterion("selected_count not in", values, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountBetween(Long value1, Long value2) {
            addCriterion("selected_count between", value1, value2, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andSelectedCountNotBetween(Long value1, Long value2) {
            addCriterion("selected_count not between", value1, value2, "selectedCount");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityIsNull() {
            addCriterion("classroom_capacity is null");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityIsNotNull() {
            addCriterion("classroom_capacity is not null");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityEqualTo(Integer value) {
            addCriterion("classroom_capacity =", value, "classroomCapacity");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityNotEqualTo(Integer value) {
            addCriterion("classroom_capacity <>", value, "classroomCapacity");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityGreaterThan(Integer value) {
            addCriterion("classroom_capacity >", value, "classroomCapacity");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityGreaterThanOrEqualTo(Integer value) {
            addCriterion("classroom_capacity >=", value, "classroomCapacity");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityLessThan(Integer value) {
            addCriterion("classroom_capacity <", value, "classroomCapacity");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityLessThanOrEqualTo(Integer value) {
            addCriterion("classroom_capacity <=", value, "classroomCapacity");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityIn(List<Integer> values) {
            addCriterion("classroom_capacity in", values, "classroomCapacity");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityNotIn(List<Integer> values) {
            addCriterion("classroom_capacity not in", values, "classroomCapacity");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityBetween(Integer value1, Integer value2) {
            addCriterion("classroom_capacity between", value1, value2, "classroomCapacity");
            return (Criteria) this;
        }

        public Criteria andClassroomCapacityNotBetween(Integer value1, Integer value2) {
            addCriterion("classroom_capacity not between", value1, value2, "classroomCapacity");
            return (Criteria) this;
        }

        public Criteria andClassroomCountIsNull() {
            addCriterion("classroom_count is null");
            return (Criteria) this;
        }

        public Criteria andClassroomCountIsNotNull() {
            addCriterion("classroom_count is not null");
            return (Criteria) this;
        }

        public Criteria andClassroomCountEqualTo(Integer value) {
            addCriterion("classroom_count =", value, "classroomCount");
            return (Criteria) this;
        }

        public Criteria andClassroomCountNotEqualTo(Integer value) {
            addCriterion("classroom_count <>", value, "classroomCount");
            return (Criteria) this;
        }

        public Criteria andClassroomCountGreaterThan(Integer value) {
            addCriterion("classroom_count >", value, "classroomCount");
            return (Criteria) this;
        }

        public Criteria andClassroomCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("classroom_count >=", value, "classroomCount");
            return (Criteria) this;
        }

        public Criteria andClassroomCountLessThan(Integer value) {
            addCriterion("classroom_count <", value, "classroomCount");
            return (Criteria) this;
        }

        public Criteria andClassroomCountLessThanOrEqualTo(Integer value) {
            addCriterion("classroom_count <=", value, "classroomCount");
            return (Criteria) this;
        }

        public Criteria andClassroomCountIn(List<Integer> values) {
            addCriterion("classroom_count in", values, "classroomCount");
            return (Criteria) this;
        }

        public Criteria andClassroomCountNotIn(List<Integer> values) {
            addCriterion("classroom_count not in", values, "classroomCount");
            return (Criteria) this;
        }

        public Criteria andClassroomCountBetween(Integer value1, Integer value2) {
            addCriterion("classroom_count between", value1, value2, "classroomCount");
            return (Criteria) this;
        }

        public Criteria andClassroomCountNotBetween(Integer value1, Integer value2) {
            addCriterion("classroom_count not between", value1, value2, "classroomCount");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsIsNull() {
            addCriterion("teacher_ids is null");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsIsNotNull() {
            addCriterion("teacher_ids is not null");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsEqualTo(String value) {
            addCriterion("teacher_ids =", value, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsNotEqualTo(String value) {
            addCriterion("teacher_ids <>", value, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsGreaterThan(String value) {
            addCriterion("teacher_ids >", value, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsGreaterThanOrEqualTo(String value) {
            addCriterion("teacher_ids >=", value, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsLessThan(String value) {
            addCriterion("teacher_ids <", value, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsLessThanOrEqualTo(String value) {
            addCriterion("teacher_ids <=", value, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsLike(String value) {
            addCriterion("teacher_ids like", value, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsNotLike(String value) {
            addCriterion("teacher_ids not like", value, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsIn(List<String> values) {
            addCriterion("teacher_ids in", values, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsNotIn(List<String> values) {
            addCriterion("teacher_ids not in", values, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsBetween(String value1, String value2) {
            addCriterion("teacher_ids between", value1, value2, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andTeacherIdsNotBetween(String value1, String value2) {
            addCriterion("teacher_ids not between", value1, value2, "teacherIds");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Long value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Long value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Long value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Long value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Long value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Long value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Long> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Long> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Long value1, Long value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Long value1, Long value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNull() {
            addCriterion("operator is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNotNull() {
            addCriterion("operator is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorEqualTo(String value) {
            addCriterion("operator =", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotEqualTo(String value) {
            addCriterion("operator <>", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThan(String value) {
            addCriterion("operator >", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThanOrEqualTo(String value) {
            addCriterion("operator >=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThan(String value) {
            addCriterion("operator <", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThanOrEqualTo(String value) {
            addCriterion("operator <=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLike(String value) {
            addCriterion("operator like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotLike(String value) {
            addCriterion("operator not like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorIn(List<String> values) {
            addCriterion("operator in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotIn(List<String> values) {
            addCriterion("operator not in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorBetween(String value1, String value2) {
            addCriterion("operator between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotBetween(String value1, String value2) {
            addCriterion("operator not between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andSchoolidIsNull() {
            addCriterion("schoolId is null");
            return (Criteria) this;
        }

        public Criteria andSchoolidIsNotNull() {
            addCriterion("schoolId is not null");
            return (Criteria) this;
        }

        public Criteria andSchoolidEqualTo(Long value) {
            addCriterion("schoolId =", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotEqualTo(Long value) {
            addCriterion("schoolId <>", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidGreaterThan(Long value) {
            addCriterion("schoolId >", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidGreaterThanOrEqualTo(Long value) {
            addCriterion("schoolId >=", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidLessThan(Long value) {
            addCriterion("schoolId <", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidLessThanOrEqualTo(Long value) {
            addCriterion("schoolId <=", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidIn(List<Long> values) {
            addCriterion("schoolId in", values, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotIn(List<Long> values) {
            addCriterion("schoolId not in", values, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidBetween(Long value1, Long value2) {
            addCriterion("schoolId between", value1, value2, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotBetween(Long value1, Long value2) {
            addCriterion("schoolId not between", value1, value2, "schoolid");
            return (Criteria) this;
        }

        public Criteria andTaskIdIsNull() {
            addCriterion("task_id is null");
            return (Criteria) this;
        }

        public Criteria andTaskIdIsNotNull() {
            addCriterion("task_id is not null");
            return (Criteria) this;
        }

        public Criteria andTaskIdEqualTo(Long value) {
            addCriterion("task_id =", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdNotEqualTo(Long value) {
            addCriterion("task_id <>", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdGreaterThan(Long value) {
            addCriterion("task_id >", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdGreaterThanOrEqualTo(Long value) {
            addCriterion("task_id >=", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdLessThan(Long value) {
            addCriterion("task_id <", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdLessThanOrEqualTo(Long value) {
            addCriterion("task_id <=", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdIn(List<Long> values) {
            addCriterion("task_id in", values, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdNotIn(List<Long> values) {
            addCriterion("task_id not in", values, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdBetween(Long value1, Long value2) {
            addCriterion("task_id between", value1, value2, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdNotBetween(Long value1, Long value2) {
            addCriterion("task_id not between", value1, value2, "taskId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}