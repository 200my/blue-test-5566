package blue.splitclass.model;

import blue.dao.base.BaseCourseModel;

import java.io.Serializable;
import java.util.Date;

public class SplitClassLayerCourse extends BaseCourseModel implements Serializable {
    private Long id;

    /**
     * 层次名称
     */
    private Long courseId;

    private Long taskId;

    private Integer layerCount;

    private String layerConfig;

    private String layerResultConfig;

    /**
     * 选课人数
     */
    private Integer selectedCount;

    /**
     * 男生选课人数
     */
    private Integer selectedMaleCount;

    private String extend;

    private Byte status;

    private Date createTime;

    private Date updateTime;

    private String operator;

    private Long schoolid;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Integer getLayerCount() {
        return layerCount;
    }

    public void setLayerCount(Integer layerCount) {
        this.layerCount = layerCount;
    }

    public String getLayerConfig() {
        return layerConfig;
    }

    public void setLayerConfig(String layerConfig) {
        this.layerConfig = layerConfig == null ? null : layerConfig.trim();
    }

    public String getLayerResultConfig() {
        return layerResultConfig;
    }

    public void setLayerResultConfig(String layerResultConfig) {
        this.layerResultConfig = layerResultConfig == null ? null : layerResultConfig.trim();
    }

    public Integer getSelectedCount() {
        return selectedCount;
    }

    public void setSelectedCount(Integer selectedCount) {
        this.selectedCount = selectedCount;
    }

    public Integer getSelectedMaleCount() {
        return selectedMaleCount;
    }

    public void setSelectedMaleCount(Integer selectedMaleCount) {
        this.selectedMaleCount = selectedMaleCount;
    }

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend == null ? null : extend.trim();
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", courseId=").append(courseId);
        sb.append(", taskId=").append(taskId);
        sb.append(", layerCount=").append(layerCount);
        sb.append(", layerConfig=").append(layerConfig);
        sb.append(", layerResultConfig=").append(layerResultConfig);
        sb.append(", selectedCount=").append(selectedCount);
        sb.append(", selectedMaleCount=").append(selectedMaleCount);
        sb.append(", extend=").append(extend);
        sb.append(", status=").append(status);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", operator=").append(operator);
        sb.append(", schoolid=").append(schoolid);
        sb.append("]");
        return sb.toString();
    }
}