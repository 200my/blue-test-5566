package blue.splitclass.model;

import java.util.List;

public class CourseTime {
    private String courseName;
    private Long courseId;
    private Integer time;
    private Long selectedCount;
    private List<CourseTime> selectedCourseInfo;


    public CourseTime() {
    }


    public CourseTime(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Long getSelectedCount() {
        return selectedCount;
    }

    public void setSelectedCount(Long selectedCount) {
        this.selectedCount = selectedCount;
    }

    public List<CourseTime> getSelectedCourseInfo() {
        return selectedCourseInfo;
    }

    public void setSelectedCourseInfo(List<CourseTime> selectedCourseInfo) {
        this.selectedCourseInfo = selectedCourseInfo;
    }
}
