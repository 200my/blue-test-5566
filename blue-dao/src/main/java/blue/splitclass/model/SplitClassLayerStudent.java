package blue.splitclass.model;

import blue.dao.uploadexcel.ExcelBase;
import cn.hutool.poi.excel.ExcelReader;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;

import java.util.*;

public class SplitClassLayerStudent extends ExcelBase {

    private Long id;

    /**
     * 层次名称
     */
    private Long userid;

    private String userno;

    private String username;

    private Long taskId;

    private Long schoolid;

    private String layerInfo;

    private Long classId;

    //用于班级名称显示
    private String className;

    private Byte status;

    private Date createTime;

    private Date updateTime;

    private String operator;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno == null ? null : userno.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getLayerInfo() {
        return layerInfo;
    }

    public void setLayerInfo(String layerInfo) {
        this.layerInfo = layerInfo == null ? null : layerInfo.trim();
    }

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOperator() {
        return operator;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userid=").append(userid);
        sb.append(", userno=").append(userno);
        sb.append(", username=").append(username);
        sb.append(", taskId=").append(taskId);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", layerInfo=").append(layerInfo);
        sb.append(", classId=").append(classId);
        sb.append(", status=").append(status);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", operator=").append(operator);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public HashMap<String, String> aliasMappint() {
        return null;
    }


    public List<SplitClassLayerStudent> convertExcel2Object(ExcelReader excelReader){

        List<Map<String, Object>> excelData=excelReader.readAll();
        if(CollectionUtils.isEmpty(excelData)){
            return null;
        }
        List<SplitClassLayerStudent> result=new LinkedList();
        for(Map<String, Object> item:excelData){
            SplitClassLayerStudent splitClassLayerStudent=new SplitClassLayerStudent();
            if(item.get("学号")==null){
                continue;
            }
            splitClassLayerStudent.setUserno((String) item.get("学号"));

            if(item.get("姓名")!=null){
                splitClassLayerStudent.setUsername((String) item.get("姓名"));
            }
            item.remove("学号");
            item.remove("姓名");

            //去除备注部分
            for(String key:item.keySet()){
                if(key.contains("备注:")){
                    item.remove(key);
                }
            }


            splitClassLayerStudent.setLayerInfo(JSONObject.toJSONString(item));
            result.add(splitClassLayerStudent);
        }


        return result;

    }
}