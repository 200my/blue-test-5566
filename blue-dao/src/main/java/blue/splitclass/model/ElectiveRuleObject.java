package blue.splitclass.model;

import java.util.List;

public class ElectiveRuleObject {

    private ElectiveRule mainRule;
    private List<Long> requiredCourse;

    public ElectiveRule getMainRule() {
        return mainRule;
    }

    public void setMainRule(ElectiveRule mainRule) {
        this.mainRule = mainRule;
    }

    public List<Long> getRequiredCourse() {
        return requiredCourse;
    }

    public void setRequiredCourse(List<Long> requiredCourse) {
        this.requiredCourse = requiredCourse;
    }
}
