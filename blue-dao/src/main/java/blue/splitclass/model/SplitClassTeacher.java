package blue.splitclass.model;

import blue.dao.base.BaseModel;
import blue.dao.base.BaseUserModel;

import java.util.Date;

public class SplitClassTeacher extends BaseUserModel implements Comparable<SplitClassTeacher> {
    private Long id;

    private String courseName;

    private Long selectedCount;

    private Integer classroomCapacity;

    private Integer classroomCount;

    private String teacherIds;

    private Long courseId;

    private Date createTime;

    private Date updateTime;

    private String operator;

    private Long schoolid;

    private Long taskId;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void setStatus(Byte status) {

    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public Long getSelectedCount() {
        return selectedCount;
    }

    public void setSelectedCount(Long selectedCount) {
        this.selectedCount = selectedCount;
    }

    public Integer getClassroomCapacity() {
        return classroomCapacity;
    }

    public void setClassroomCapacity(Integer classroomCapacity) {
        this.classroomCapacity = classroomCapacity;
    }

    public Integer getClassroomCount() {
        return classroomCount;
    }

    public void setClassroomCount(Integer classroomCount) {
        this.classroomCount = classroomCount;
    }

    public String getTeacherIds() {
        return teacherIds;
    }

    public void setTeacherIds(String teacherIds) {
        this.teacherIds = teacherIds == null ? null : teacherIds.trim();
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", courseName=").append(courseName);
        sb.append(", selectedCount=").append(selectedCount);
        sb.append(", classroomCapacity=").append(classroomCapacity);
        sb.append(", classroomCount=").append(classroomCount);
        sb.append(", teacherIds=").append(teacherIds);
        sb.append(", courseId=").append(courseId);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", operator=").append(operator);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", taskId=").append(taskId);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public int compareTo(SplitClassTeacher o) {
        return this.getCourseName().compareToIgnoreCase(o.getCourseName());
    }
}