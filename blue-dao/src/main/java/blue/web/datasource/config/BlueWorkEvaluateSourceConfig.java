package blue.web.datasource.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
// 扫描 Mapper 接口并容器管理
@MapperScan(basePackages = BlueWorkEvaluateSourceConfig.PACKAGE, sqlSessionFactoryRef = "blueWorkEvaluateSqlSessionFactory")
public class BlueWorkEvaluateSourceConfig {

    // 精确到 master 目录，以便跟其他数据源隔离
    static final String PACKAGE = "blue.workevaluate.dao";
    static final String MAPPER_LOCATION = "classpath:blue/workevaluate/mapping/*.xml";

    @Value("${blue-workevaluate.datasource.url}")
    private String url;

    @Value("${blue-workevaluate.datasource.username}")
    private String user;

    @Value("${blue-workevaluate.datasource.password}")
    private String password;

    @Value("${blue-workevaluate.datasource.driver-class-name}")
    private String driverClass;

    @Bean(name = "blueWorkEvaluateDataSource")
    public DataSource blueWorkEvaluateDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driverClass);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean(name = "blueWorkEvaluateTransactionManager")
    public DataSourceTransactionManager blueScTransactionManager() {
        return new DataSourceTransactionManager(blueWorkEvaluateDataSource());
    }

    @Bean(name = "blueWorkEvaluateSqlSessionFactory")
    public SqlSessionFactory blueScSqlSessionFactory(@Qualifier("blueWorkEvaluateDataSource") DataSource blueScDataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(blueScDataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(BlueWorkEvaluateSourceConfig.MAPPER_LOCATION));
        return sessionFactory.getObject();
    }
}