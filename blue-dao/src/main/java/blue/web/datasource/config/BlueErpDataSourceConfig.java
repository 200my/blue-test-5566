package blue.web.datasource.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;


@Configuration
// 扫描 Mapper 接口并容器管理
@MapperScan(basePackages = BlueErpDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "blueErpSqlSessionFactory")
public class BlueErpDataSourceConfig {

    // 精确到 master 目录，以便跟其他数据源隔离
    static final String PACKAGE = "blue.erp.dao";
    static final String MAPPER_LOCATION = "classpath:blue/erp/mapping/*.xml";

    @Value("${blue-erp.datasource.url}")
    private String url;

    @Value("${blue-erp.datasource.username}")
    private String user;

    @Value("${blue-erp.datasource.password}")
    private String password;

    @Value("${blue-erp.datasource.driver-class-name}")
    private String driverClass;

    @Bean(name = "blueErpDataSource")
    @Primary
    public DataSource blueErpDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driverClass);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean(name = "blueErpTransactionManager")
    @Primary
    public DataSourceTransactionManager blueErpTransactionManager() {
        return new DataSourceTransactionManager(blueErpDataSource());
    }

    @Bean(name = "blueErpSqlSessionFactory")
    @Primary
    public SqlSessionFactory blueErpSqlSessionFactory(@Qualifier("blueErpDataSource") DataSource blueErpDataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(blueErpDataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(BlueErpDataSourceConfig.MAPPER_LOCATION));
        return sessionFactory.getObject();
    }
}