package blue.web.datasource.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
// 扫描 Mapper 接口并容器管理
@MapperScan(basePackages = BlueSplitClassSourceConfig.PACKAGE, sqlSessionFactoryRef = "blueSplitClassSqlSessionFactory")
public class BlueSplitClassSourceConfig {

    // 精确到 master 目录，以便跟其他数据源隔离
    static final String PACKAGE = "blue.splitclass.dao";
    static final String MAPPER_LOCATION = "classpath:blue/splitclass/mapping/*.xml";

    @Value("${blue-splitclass.datasource.url}")
    private String url;

    @Value("${blue-splitclass.datasource.username}")
    private String user;

    @Value("${blue-splitclass.datasource.password}")
    private String password;

    @Value("${blue-splitclass.datasource.driver-class-name}")
    private String driverClass;

    @Bean(name = "blueSplitClassDataSource")
    public DataSource blueScDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driverClass);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean(name = "blueSplitClassTransactionManager")
    public DataSourceTransactionManager blueScTransactionManager() {
        return new DataSourceTransactionManager(blueScDataSource());
    }

    @Bean(name = "blueSplitClassSqlSessionFactory")
    public SqlSessionFactory blueScSqlSessionFactory(@Qualifier("blueSplitClassDataSource") DataSource blueScDataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(blueScDataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(BlueSplitClassSourceConfig.MAPPER_LOCATION));
        return sessionFactory.getObject();
    }
}