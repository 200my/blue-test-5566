package blue.web.dao.service;

import blue.dao.base.BaseMapper;
import blue.dao.base.BaseModel;
import blue.erp.model.BlueTeacher;
import blue.erp.model.SysUser;
import blue.splitclass.dao.SplitClassTeacherMapper;
import blue.splitclass.model.CourseTime;
import blue.splitclass.model.SplitClassTeacher;
import blue.splitclass.model.SplitClassTeacherExample;
import blue.splitclass.model.SplitClassTime;
import blue.tools.commons.BlueStringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Service
public class SplitClassTeacherService extends BaseService {

    @Autowired
    private SplitClassTeacherMapper splitClassTeacherMapper;

    @Autowired
    private SplitClassTimeService splitClassTimeService;

    @Autowired
    private BlueTeacherService blueTeacherService;

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        SplitClassTeacherExample example=new SplitClassTeacherExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(SplitClassTeacher record) {
        if(record==null || (record.getTaskId()==null && record.getId()==null)){
            return 0;
        }

        SplitClassTeacherExample example=new SplitClassTeacherExample();
        SplitClassTeacherExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }

        if(record.getTaskId()!=null){
            criteria.andTaskIdEqualTo(record.getTaskId());
        }
        if(record.getCourseId()!=null){
            criteria.andCourseIdEqualTo(record.getCourseId());
        }
        if(StringUtils.isNotBlank(record.getCourseName())){
            criteria.andCourseNameEqualTo(record.getCourseName());
        }

        clearCache(record.getSchoolid());
        return this.baseSave(record,example);

    }


    public List<SplitClassTeacher> selectByObject(SplitClassTeacher record,boolean showUserName){

        if(record.getTaskId()==null){
            return null;
        }

        SplitClassTeacherExample example=new SplitClassTeacherExample();
        SplitClassTeacherExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid()).andTaskIdEqualTo(record.getTaskId());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }

        List<SplitClassTeacher> result= this.select(example);
        if(CollectionUtils.isEmpty(result)){
            initDb(record.getTaskId(),record.getSchoolid());
            result=this.select(example);
        }

        if(CollectionUtils.isNotEmpty(result) && showUserName){
            for(SplitClassTeacher splitClassTeacher:result){
                if(StringUtils.isNotBlank(splitClassTeacher.getTeacherIds())){
                    List<Long> teacherIdList=BlueStringUtils.convertString2Num(splitClassTeacher.getTeacherIds(),",");
                    List<String> teacherNameList=new LinkedList<>();
                    for(Long teacherId:teacherIdList){
                        BaseModel blueTeacher=  blueTeacherService.selectByPrimaryKey(teacherId,record.getSchoolid());
                        if(blueTeacher!=null){
                            teacherNameList.add(((BlueTeacher) blueTeacher).getRealName());
                        }
                    }
                    splitClassTeacher.setUserNameList(teacherNameList);
                }
            }
        }

        return result;
    }

    //初始化表数据，一般只初始化课程名称和选课人数 lg:物理——优秀 55
    public void initDb(Long taskId,Long schoolId){

        SplitClassTime splitClassTime=new SplitClassTime();
        splitClassTime.setTaskId(taskId);
        splitClassTime.setSchoolid(schoolId);
        List<CourseTime> courseTimeList= splitClassTimeService.queryCompose(splitClassTime);

        HashMap<String,CourseTime> name2CourstTime=new HashMap();
        if(CollectionUtils.isEmpty(courseTimeList)){
            return;
        }

        for(CourseTime courseTime:courseTimeList){
            if(courseTime==null || CollectionUtils.isEmpty(courseTime.getSelectedCourseInfo())){
                continue;
            }
            for(CourseTime subCourseTime:courseTime.getSelectedCourseInfo()){
                if(name2CourstTime.get(subCourseTime.getCourseName())==null){
                    name2CourstTime.put(subCourseTime.getCourseName(),subCourseTime);
                }
            }
        }
        List<SplitClassTeacher> splitClassTeachers=new LinkedList<>();
        for(String courseName:name2CourstTime.keySet()){
            CourseTime courseTime=name2CourstTime.get(courseName);
            SplitClassTeacher splitClassTeacher=new SplitClassTeacher();
            splitClassTeacher.setCourseName(courseTime.getCourseName());
            splitClassTeacher.setCourseId(courseTime.getCourseId());
            splitClassTeacher.setSelectedCount(courseTime.getSelectedCount());
            splitClassTeacher.setTaskId(taskId);
            splitClassTeacher.setSchoolid(schoolId);
            splitClassTeachers.add(splitClassTeacher);
        }
        //按照课程名称排序
        Collections.sort(splitClassTeachers);

        for(SplitClassTeacher splitClassTeacher:splitClassTeachers){
            this.save(splitClassTeacher);
        }


    }



    @Override
    public BaseMapper getBaseMapper() {
        return this.splitClassTeacherMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }

}
