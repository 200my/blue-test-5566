package blue.web.dao.service;

import blue.commons.enums.StatusEnum;
import blue.dao.base.BaseMapper;
import blue.erp.model.SysUser;
import blue.tools.commons.BlueStringUtils;
import blue.tools.constants.BlueContants;
import blue.workevaluate.dao.WorkTaskStudentSummerMapper;
import blue.workevaluate.model.WorkTaskStudentSummer;
import blue.workevaluate.model.WorkTaskStudentSummerExample;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
@Lazy
@Service
public class WorkTaskStudentSummerService extends BaseService {

    @Autowired
    private WorkTaskStudentSummerMapper workTaskStudentSummerMapper;
    @Lazy
    @Autowired
    private WorkStudentQuestionService workStudentQuestionService;

    private final String prefix_student_summer="WorkTaskStudentSummerService";

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        WorkTaskStudentSummerExample example=new WorkTaskStudentSummerExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(WorkTaskStudentSummer record,boolean deleteCache) {
        if(record==null || record.getTaskId()==null){
            return 0;
        }

        WorkTaskStudentSummerExample example=new WorkTaskStudentSummerExample();
        WorkTaskStudentSummerExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid()).andTaskIdEqualTo(record.getTaskId());

        if(record.getUserId()!=null){
            criteria.andUserIdEqualTo(record.getUserId());
        }
        if(deleteCache){
            clearCache(record.getSchoolid(),record.getTaskId(),record.getUserId());
        }
        return this.baseSave(record,example);

    }


    public WorkTaskStudentSummer selectUserTask(Long taskId,Long userId,Long schoolId){

        if(taskId==null || userId==null || schoolId==null){
            return null;
        }

        Object cacheObject=redisService.getValue(String.format("%s_%s_%s_%s",prefix_student_summer,schoolId,taskId,userId));
        if(cacheObject!=null){
            WorkTaskStudentSummer result=JSONObject.parseObject(cacheObject.toString(),WorkTaskStudentSummer.class);
            if(result.getStatus() < StatusEnum.Complete.getValue()){
                result.setCompleteCount(workStudentQuestionService.getTaskTagCount(taskId,schoolId,userId));
                redisService.setValue(String.format("%s_%s_%s_%s",prefix_student_summer,schoolId,taskId,userId),JSONObject.toJSONString(result), BlueContants.week);
                this.save(result,false);
            }
            return result;
        }

        WorkTaskStudentSummer studentSummer=new WorkTaskStudentSummer();
        studentSummer.setUserId(userId);
        studentSummer.setSchoolid(schoolId);
        studentSummer.setTaskId(taskId);

        List selectList=this.selectByObject(studentSummer);
        if(CollectionUtils.isNotEmpty(selectList) || selectList.size()==1){

            WorkTaskStudentSummer result=(WorkTaskStudentSummer) selectList.get(0);
            if(result.getStatus() < StatusEnum.Complete.getValue()){
                result.setCompleteCount(workStudentQuestionService.getTaskTagCount(taskId,schoolId,userId));
                this.save(result,false);
            }

            return result;
        }

        return null;
    }


    public List<WorkTaskStudentSummer> selectByObject(WorkTaskStudentSummer record){

        if(record.getTaskId()==null){
            return null;
        }

        WorkTaskStudentSummerExample example=new WorkTaskStudentSummerExample();
        WorkTaskStudentSummerExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid()).andTaskIdEqualTo(record.getTaskId());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }
        if(record.getUserId()!=null && record.getUserId()>0){
            criteria.andUserIdEqualTo(record.getUserId());
        }

        List<WorkTaskStudentSummer>  layerCourseList=this.select(example);


        return layerCourseList;
    }


    public List<WorkTaskStudentSummer> getCompletedList(Long taskId,Long schoolId){
        List<WorkTaskStudentSummer> result=new LinkedList<>();
        if(taskId==null || schoolId==null){
            return result;
        }

        WorkTaskStudentSummer taskStudentSummer=new WorkTaskStudentSummer();
        taskStudentSummer.setTaskId(taskId);
        taskStudentSummer.setSchoolid(schoolId);
        taskStudentSummer.setStatus((byte) StatusEnum.Complete.getValue());
        return this.selectByObject(taskStudentSummer);
    }



    @Override
    public BaseMapper getBaseMapper() {
        return this.workTaskStudentSummerMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {
        redisService.vagueDelete(String.format("%s_%s_%s_%s",prefix_student_summer,schoolId,extend[0],extend[1]));
    }

}
