package blue.web.dao.service;


import blue.commons.enums.PermissionEnum;
import blue.dao.base.BaseMapper;
import blue.erp.dao.SysResourceMapper;
import blue.erp.model.SysResource;
import blue.erp.model.SysResourceExample;
import blue.erp.model.SysResourceExtend;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class SysResourceService extends BaseService{

    @Autowired
    private SysResourceMapper sysResourceMapper;



    public int save(SysResource record) {

        if(record==null || StringUtils.isBlank(record.getName())){
            return 0;
        }

        SysResourceExample example=new SysResourceExample();
        example.createCriteria().andNameEqualTo(record.getName()).andPermissionEqualTo(record.getPermission());

        return this.baseSave(record,example);
    }


    public void putJson2Db(SysResource parent, String parents, JSONArray children) {

        for (int i = 0; i < children.size(); i++) {
            JSONObject jsonObject = children.getJSONObject(i);
            SysResource sysResource = JSONObject.parseObject(jsonObject.toJSONString(), SysResource.class);

            sysResource.setParentIds(parents);
            sysResource.setStatus((byte) 1);
            sysResource.setType("menu");
            if (parent != null) {
                sysResource.setParentId(parent.getId());
                sysResource.setUrl(String.format("%s/%s", parent.getUrl(), jsonObject.getString("path")));
            } else {
                sysResource.setUrl(String.format("/%s", jsonObject.getString("path")));
                sysResource.setParentId(0l);
            }

            JSONArray childrenArray = jsonObject.getJSONArray("children");

            if(childrenArray==null || childrenArray.size()==0){
                for (PermissionEnum permissionEnum : PermissionEnum.values()) {
                    sysResource.setPermission(String.format("%s:%s", jsonObject.getString("path"), permissionEnum.getValue()));
                    sysResource.setName(String.format("%s-%s", jsonObject.getString("name"), permissionEnum.getDesc()));
                    save(sysResource);
                }
            }else {
                sysResource.setPermission(String.format("%s:%s", jsonObject.getString("path"), PermissionEnum.All.getValue()));
                sysResource.setName(String.format("%s", jsonObject.getString("name")));
                save(sysResource);
            }




            SysResourceExample example = new SysResourceExample();
            example.createCriteria().andNameLike("%" + jsonObject.getString("name") + "%").andUrlEqualTo(sysResource.getUrl());

            List<SysResource> sysResourceList = select(example);

            SysResource masterSysResource = new SysResource();
            for (SysResource resource : sysResourceList) {
                if (resource.getPermission().contains("*")) {
                    masterSysResource = resource;
                }
            }



            if (childrenArray != null && childrenArray.size() > 0) {
                putJson2Db(masterSysResource, parents + "/" + masterSysResource.getId(), childrenArray);
            }
        }

    }


    public List<SysResourceExtend> getDb2Json(Long parentId){

        List<SysResourceExtend> result=new LinkedList();

        SysResourceExample example=new SysResourceExample();

        example.createCriteria().andParentIdEqualTo(parentId==null?0l:parentId);

        List<SysResource> sysResourceList=this.select(example);


        for(SysResource sysResource:sysResourceList){

            SysResourceExtend sysResourceExtend=new SysResourceExtend();
            sysResourceExtend.setTitle(sysResource.getName());
            sysResourceExtend.setKey(sysResource.getId().toString());

            List childrenList=getDb2Json(sysResource.getId());

            if(CollectionUtils.isNotEmpty(childrenList)){
                sysResourceExtend.setChildren(childrenList);
            }


            result.add(sysResourceExtend);

        }

        return result;

    }


    @Override
    public BaseMapper getBaseMapper() {
        return this.sysResourceMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }
}
