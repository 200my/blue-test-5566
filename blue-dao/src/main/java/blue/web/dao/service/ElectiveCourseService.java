package blue.web.dao.service;

import blue.tools.constants.BlueContants;
import blue.dao.base.BaseMapper;
import blue.splitclass.dao.ElectiveCourseMapper;
import blue.splitclass.model.ElectiveCourse;
import blue.splitclass.model.ElectiveCourseExample;
import blue.tools.commons.BlueStringUtils;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ElectiveCourseService extends BaseService {

    @Autowired
    private ElectiveCourseMapper electiveCourseMapper;

    public int deleteBatch(String ids,Long schoolId) {

        List idList= BlueStringUtils.convertString2Num(ids,",");
        ElectiveCourseExample example=new ElectiveCourseExample();
        example.createCriteria().andIdIn(idList);
        clearCache(schoolId);
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(ElectiveCourse record) {

        if(record==null || record.getCourseId()==null){
            return 0;
        }

        ElectiveCourseExample example=new ElectiveCourseExample();
        example.createCriteria().andTaskIdEqualTo(record.getTaskId()).andCourseIdEqualTo(record.getCourseId()).andSchoolidEqualTo(record.getSchoolid());

        return this.baseSave(record,example);

    }


    public ElectiveCourse selectByPrimaryKey(Long id,Long schoolId,boolean flush){

        Object gradeCache=this.baseSelectByPrimaryKey(id,BlueContants.prefix_courseId,schoolId,flush);

        if(gradeCache!=null){
            return JSONObject.parseObject(gradeCache.toString(),ElectiveCourse.class);
        }

        return null;

    }

    public List<ElectiveCourse> selectByTaskId(Long taskId,Long schoolId){

        ElectiveCourseExample example=new ElectiveCourseExample();
        example.createCriteria().andTaskIdEqualTo(taskId).andSchoolidEqualTo(schoolId);


        return this.select(example);

    }

    @Override
    public BaseMapper getBaseMapper() {
        return this.electiveCourseMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {
        redisService.vagueDelete(BlueContants.prefix_courseId+schoolId);
    }
}
