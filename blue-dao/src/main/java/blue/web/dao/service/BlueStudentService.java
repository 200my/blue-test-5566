package blue.web.dao.service;

import blue.tools.constants.BlueContants;
import blue.dao.base.BaseMapper;
import blue.erp.dao.BlueStudentMapper;
import blue.erp.model.BlueStudent;
import blue.erp.model.BlueStudentExample;
import blue.tools.commons.BlueStringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlueStudentService extends BaseService {

    @Autowired
    private BlueStudentMapper blueStudentMapper;


    public int deleteBatch(String ids) {

        List idList= BlueStringUtils.convertString2Num(ids,BlueContants.separator);
        BlueStudentExample example=new BlueStudentExample();
        example.createCriteria().andIdIn(idList);

        return blueStudentMapper.deleteByExample(example);
    }

    public int deleteByUserIds(String ids,Long schoolId){

        List idList= BlueStringUtils.convertString2Num(ids,",");
        BlueStudentExample example=new BlueStudentExample();
        example.createCriteria().andUseridIn(idList).andSchoolidEqualTo(schoolId);

        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(BlueStudent record) {

        if(record==null || (record.getUserid()==null && record.getId()==null)){
            return 0;
        }

        BlueStudentExample example=new BlueStudentExample();
        BlueStudentExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }

        if(record.getUserid()!=null){
            criteria.andUseridEqualTo(record.getUserid());
        }

        return this.baseSave(record,example);

    }


    public  List<BlueStudent> selectByClassList(List classIds,Long schoolId) {

        if(CollectionUtils.isEmpty(classIds)){
            return null;
        }

        BlueStudentExample example=new BlueStudentExample();

        BlueStudentExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(schoolId);

        criteria.andClassidIn(classIds);

        return blueStudentMapper.selectByExample(example);
    }


    public  List<BlueStudent> selectByClassIdPrivate(Long classId) {

        if(classId==null){
            return null;
        }

        BlueStudentExample example=new BlueStudentExample();

        BlueStudentExample.Criteria criteria=example.createCriteria().andClassidEqualTo(classId);

        return blueStudentMapper.selectByExample(example);
    }




    @Override
    public BaseMapper getBaseMapper() {
        return this.blueStudentMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }
}
