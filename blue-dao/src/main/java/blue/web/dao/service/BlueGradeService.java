package blue.web.dao.service;

import blue.tools.constants.BlueContants;
import blue.dao.base.BaseMapper;
import blue.erp.dao.BlueGradeMapper;
import blue.erp.model.BlueGrade;
import blue.erp.model.BlueGradeExample;
import blue.tools.commons.BlueStringUtils;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Service
public class BlueGradeService extends BaseService{

    @Autowired
    private BlueGradeMapper blueGradeMapper;

    public int deleteBatch(String ids,Long schoolId) {

        List idList= BlueStringUtils.convertString2Num(ids,",");
        BlueGradeExample example=new BlueGradeExample();
        example.createCriteria().andIdIn(idList).andSchoolidEqualTo(schoolId);

        BlueGrade blueGrade=new BlueGrade();
        blueGrade.setSchoolid(schoolId);

        clearCache(schoolId);

        return blueGradeMapper.deleteByExample(example);
    }


    public int save(BlueGrade record) {

        if(record==null || StringUtils.isBlank(record.getName())){
            return 0;
        }

        BlueGradeExample example=new BlueGradeExample();
        example.createCriteria().andNameEqualTo(record.getName()).andSchoolidEqualTo(record.getSchoolid());
        clearCache(record.getSchoolid());
        return this.baseSave(record,example);
    }




    public  List<BlueGrade> selectBySchoolId(Long schoolId) {

        if(schoolId==null || schoolId<=0){
            return null;
        }

        BlueGradeExample example=new BlueGradeExample();
        example.createCriteria().andSchoolidEqualTo(schoolId);

        return blueGradeMapper.selectByExample(example);
    }


    public List<String> queryGradeLevels(Long schoolId){
        if(schoolId==null || schoolId<=0){
            return null;
        }
        List<BlueGrade> blueGradeList= this.selectBySchoolIdCache(schoolId,false);

        Set<String> gradeLevelSet= new TreeSet();
        if(CollectionUtils.isNotEmpty(blueGradeList)){
            for(BlueGrade blueGrade:blueGradeList){
                if(StringUtils.isNotBlank(blueGrade.getGradeLevel())){
                    gradeLevelSet.add(blueGrade.getGradeLevel());
                }

            }
        }

        return new LinkedList<>(gradeLevelSet);

    }



    public List<BlueGrade> selectBySchoolIdCache(Long schoolId,boolean directFormDb){

        Object gradeCache=redisService.getValue(BlueContants.prefix_school_gradeList + schoolId.toString());

        if(!directFormDb && gradeCache!=null){

            return JSONArray.parseArray(gradeCache.toString(),BlueGrade.class);
        }else {
            List<BlueGrade> blueGradeList= selectBySchoolId(schoolId);
            if(CollectionUtils.isNotEmpty(blueGradeList)){
                redisService.setValue(BlueContants.prefix_school_gradeList + schoolId.toString(),JSONArray.toJSONString(blueGradeList));
            }
            return blueGradeList;
        }
    }

    public BlueGrade selectedByName(String name,Long schoolId,boolean directFormDb){

        if(StringUtils.isBlank(name) || schoolId==null){
            return null;
        }

        Object gradeCache=redisService.getValue(BlueContants.prefix_gradeName + schoolId + name.toString());
        if(!directFormDb && gradeCache!=null){
            return JSONObject.parseObject(gradeCache.toString(),BlueGrade.class);
        }else {

            BlueGradeExample example=new BlueGradeExample();
            example.createCriteria().andSchoolidEqualTo(schoolId).andNameEqualTo(name);
            List<BlueGrade> blueGradeList=this.select(example);
            if(CollectionUtils.isEmpty(blueGradeList)){
                return null;
            }

            if(blueGradeList.size()==1){
                redisService.setValue(BlueContants.prefix_gradeName  + schoolId + name.toString(),JSONObject.toJSONString(blueGradeList.get(0)));
            }


            return blueGradeList.get(0);
        }
    }


    public BlueGrade selectByPrimaryKey(Long id,Long schoolId,boolean flush){

       Object gradeCache=this.baseSelectByPrimaryKey(id,BlueContants.prefix_gradeId,schoolId,flush);

       if(gradeCache!=null){
           return JSONObject.parseObject(gradeCache.toString(),BlueGrade.class);
       }

       return null;
    }


    public String getTypeByGradeId(Long gradeId,Long schooId){

        BlueGrade blueGrade=selectByPrimaryKey(gradeId,schooId,false);
        String type="高中";
        if(StringUtils.isNotBlank(blueGrade.getType())){
            type=blueGrade.getType();
        }

        return type;
    }

    @Override
    public BaseMapper getBaseMapper() {
        return this.blueGradeMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {
        redisService.vagueDelete(BlueContants.prefix_school_gradeList+schoolId);
    }
}
