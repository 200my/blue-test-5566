package blue.web.dao.service;

import blue.dao.base.BaseMapper;
import blue.erp.model.SysUser;
import blue.splitclass.dao.SplitClassLayerCourseMapper;
import blue.splitclass.model.SplitClassLayerCourse;
import blue.splitclass.model.SplitClassLayerCourseExample;
import blue.tools.commons.BlueStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TempletService extends BaseService {

    @Autowired
    private SplitClassLayerCourseMapper splitClassLayerCourseMapper;

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        SplitClassLayerCourseExample example=new SplitClassLayerCourseExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(SplitClassLayerCourse record) {
        if(record==null || record.getTaskId()==null){
            return 0;
        }

        SplitClassLayerCourseExample example=new SplitClassLayerCourseExample();
        SplitClassLayerCourseExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        clearCache(record.getSchoolid());
        return this.baseSave(record,example);

    }


    public List<SplitClassLayerCourse> selectByObject(SplitClassLayerCourse record){

        if(record.getTaskId()==null){
            return null;
        }

        SplitClassLayerCourseExample example=new SplitClassLayerCourseExample();
        SplitClassLayerCourseExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid()).andTaskIdEqualTo(record.getTaskId());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }

        List<SplitClassLayerCourse>  resultList=this.select(example);


        return resultList;
    }



    @Override
    public BaseMapper getBaseMapper() {
        return this.splitClassLayerCourseMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }

}
