package blue.web.dao.service;

import blue.commons.models.KeyValue;
import blue.dao.base.BaseMapper;
import blue.erp.dao.BlueTeacherMapper;
import blue.erp.model.*;
import blue.erp.model.extend.BlueTeacherExtend;
import blue.tools.commons.BlueStringUtils;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class BlueTeacherService extends BaseService {

    @Autowired
    private BlueTeacherMapper blueTeacherMapper;

    @Autowired
    private BlueGradeService blueGradeService;

    @Autowired
    private BlueClassService blueClassService;

    @Autowired
    private BlueCourseService blueCourseService;

    @Autowired
    private SysUserService sysUserService;

    public int deleteBatch(String ids,Long schoolId) {

        List idList= BlueStringUtils.convertString2Num(ids,",");
        BlueTeacherExample example=new BlueTeacherExample();
        example.createCriteria().andIdIn(idList).andSchoolidEqualTo(schoolId);

        List<BlueTeacher> blueTeacherList=this.select(example);
        for(BlueTeacher blueTeacher:blueTeacherList){
            sysUserService.deleteByPrimaryKey(blueTeacher.getUserid(),schoolId);
        }

        return this.getBaseMapper().deleteByExample(example);
    }

    public int deleteByUserIds(String ids,Long schoolId){

        List idList= BlueStringUtils.convertString2Num(ids,",");
        BlueTeacherExample example=new BlueTeacherExample();
        example.createCriteria().andUseridIn(idList);

        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(BlueTeacher record) {
        if(record==null || (record.getUserid()==null && record.getId()==null)){
            return 0;
        }
        BlueTeacherExample example=new BlueTeacherExample();
        BlueTeacherExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }else {
            if(record.getUserid()!=null){
                criteria.andUseridEqualTo(record.getUserid());
            }
        }

        return this.baseSave(record,example);
    }


    public  List<BlueTeacher> select(BlueTeacher record) {

        BlueTeacherExample example=new BlueTeacherExample();
        BlueTeacherExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(StringUtils.isNotBlank(record.getGradeids())){
            criteria.andGradeidsLike("%"+record.getGradeids()+"%");
        }

        if(StringUtils.isNotBlank(record.getClassids())){
            criteria.andClassidsLike("%"+record.getClassids()+"%");
        }

        if(StringUtils.isNotBlank(record.getCourseids())){
            criteria.andCourseidsEqualTo(record.getCourseids());
        }

        if(StringUtils.isNotBlank(record.getRealName())){
            criteria.andRealNameLike("%"+record.getRealName()+"%");
        }

        if(record.getUserid()!=null){
            criteria.andUseridEqualTo(record.getUserid());
        }

        List<BlueTeacher> blueTeacherList=blueTeacherMapper.selectByExample(example);
        if(CollectionUtils.isNotEmpty(blueTeacherList)){
            for(BlueTeacher blueTeacher:blueTeacherList){
              addPropertyNameById(blueTeacher);
            }
        }

        return blueTeacherList;

    }

    public BlueTeacher addPropertyNameById(BlueTeacher record){

        if(record==null ){
            return null;
        }

        if(StringUtils.isNotBlank(record.getClassids())){
            List kvList=new LinkedList();
            for(Long classId:BlueStringUtils.convertString2Num(record.getClassids(),",")){
                BlueClass blueClass=blueClassService.selectByPrimaryKey(classId,record.getSchoolid(),false);
                if(blueClass!=null){
                    kvList.add(new KeyValue(blueClass.getId(),blueClass.getName()));
                }
            }

            record.setClassList(kvList);
        }

        if(StringUtils.isNotBlank(record.getGradeids())){
            List gradeList=new LinkedList();
            for(Long gradeId:BlueStringUtils.convertString2Num(record.getGradeids(),",")){
                BlueGrade blueGrade=blueGradeService.selectByPrimaryKey(gradeId,record.getSchoolid(),false);
                if(blueGrade!=null){
                    gradeList.add(new KeyValue(blueGrade.getId(),blueGrade.getName()));
                }
            }

            record.setGrades(gradeList);
        }

        if(StringUtils.isNotBlank(record.getCourseids())){
            List nameList=new LinkedList();
            List idList=new LinkedList();
            List kvList=new LinkedList();
            for(Long courseId:BlueStringUtils.convertString2Num(record.getCourseids(),",")){
                BlueCourse blueCourse=blueCourseService.selectByPrimaryKey(courseId,record.getSchoolid(),false);
                if(blueCourse!=null){
                    nameList.add(blueCourse.getName());
                    idList.add(blueCourse.getId());
                    KeyValue keyValue=new KeyValue();
                    keyValue.setKey(blueCourse.getId());
                    keyValue.setValue(blueCourse.getName());
                    kvList.add(keyValue);
                }
            }
            record.setCourseNames(nameList);
            record.setCourseIds(StringUtils.join(idList,","));
            record.setCourseList(kvList);
        }

        return record;

    }


    @Override
    public BaseMapper getBaseMapper() {
        return this.blueTeacherMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }
}
