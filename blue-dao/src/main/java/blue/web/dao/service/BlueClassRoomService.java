package blue.web.dao.service;

import blue.dao.base.BaseMapper;
import blue.erp.dao.BlueClassroomMapper;
import blue.erp.dao.BlueTeacherMapper;
import blue.erp.model.*;
import blue.tools.commons.BlueStringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Service
public class BlueClassRoomService extends BaseService {

    @Autowired
    private BlueClassroomMapper blueClassroomMapper;

    @Autowired
    private BlueClassService blueClassService;

    @Autowired
    private BlueGradeService blueGradeService;

    public int deleteBatch(String ids) {

        List idList= BlueStringUtils.convertString2Num(ids,",");
        BlueClassroomExample example=new BlueClassroomExample();
        example.createCriteria().andIdIn(idList);

        return blueClassroomMapper.deleteByExample(example);
    }

    public int save(BlueClassroom record) {

        if(record==null || StringUtils.isBlank(record.getName())){
            return 0;
        }

        BlueClassroomExample example=new BlueClassroomExample();
        BlueClassroomExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }

        if(StringUtils.isNotBlank(record.getRoomno())){
            criteria.andRoomnoEqualTo(record.getRoomno());
        }

        return this.baseSave(record,example);

    }


    public  List<BlueClassroom> select(BlueClassroom record) {

        BlueClassroomExample example=new BlueClassroomExample();
        BlueClassroomExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(StringUtils.isNotBlank(record.getUnderclassids())){
            criteria.andUnderclassidsIn(Arrays.asList(record.getUnderclassids().split(",")));
        }
        if(StringUtils.isNotBlank(record.getName())){
            criteria.andNameLike("%"+record.getName()+"%");
        }


        List<BlueClassroom> result=blueClassroomMapper.selectByExample(example);
        if(CollectionUtils.isNotEmpty(result)){
           for(BlueClassroom blueClassroom:result){

               if(blueClassroom.getUndergradeid()!=null){
                   BlueGrade blueGrade=blueGradeService.selectByPrimaryKey(blueClassroom.getUndergradeid(),record.getSchoolid(),false);
                   if(blueGrade!=null){
                       blueClassroom.setGradeName(blueGrade.getName());
                   }
               }

               if(StringUtils.isNotBlank(blueClassroom.getUnderclassids())){
                   List<String> classNameList=new LinkedList();
                   for(Long classId:BlueStringUtils.convertString2Num(blueClassroom.getUnderclassids(),",")){
                       BlueClass blueClass=blueClassService.selectByPrimaryKey(classId,record.getSchoolid(),false);
                       if(blueClass!=null){
                           classNameList.add(blueClass.getName());
                       }
                   }
                   blueClassroom.setClassNameList(classNameList);
               }

           }
        }

        return result;
    }


    @Override
    public BaseMapper getBaseMapper() {
        return this.blueClassroomMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }
}
