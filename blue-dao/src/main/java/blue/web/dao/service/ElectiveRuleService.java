package blue.web.dao.service;

import blue.commons.enums.StatusEnum;
import blue.dao.base.BaseMapper;
import blue.erp.model.SysUser;
import blue.splitclass.dao.ElectiveRuleMapper;
import blue.splitclass.model.ElectiveRule;
import blue.splitclass.model.ElectiveRuleExample;
import blue.splitclass.model.ElectiveTask;
import blue.tools.commons.BlueStringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class ElectiveRuleService extends BaseService {

    @Autowired
    private ElectiveRuleMapper electiveRuleMapper;

    @Autowired
    private BlueCourseService blueCourseService;

    @Autowired
    private ElectiveTaskService electiveTaskService;
    @Autowired
    private SplitClassLayerCourseService splitClassLayerCourseService;

    public int deleteBatch(String ids, SysUser sysUser) {

        List idList= BlueStringUtils.convertString2Num(ids,",");
        ElectiveRuleExample example=new ElectiveRuleExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(ElectiveRule record) {

        if(record==null || (StringUtils.isBlank(record.getName()) && record.getId()==null)){
            return 0;
        }

        ElectiveRuleExample example=new ElectiveRuleExample();
        ElectiveRuleExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }else {
            criteria.andNameEqualTo(record.getName());
        }

        ElectiveTask electiveTaskExtend=electiveTaskService.selectById(record.getTaskId(),record.getSchoolid());
        if(electiveTaskExtend!=null){
            electiveTaskExtend.setStatus((byte) StatusEnum.RuleAlter.getValue());
            electiveTaskService.save(electiveTaskExtend);
        }
        clearCache(record.getSchoolid(),record.getTaskId());
        return this.baseSave(record,example);

    }



    public List<ElectiveRule> selectByObject(ElectiveRule record,boolean isGetCourseName){

        if(record==null){
            return null;
        }

        ElectiveRuleExample example=new ElectiveRuleExample();
        ElectiveRuleExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }else {
            if(record.getTaskId()!=null){
                criteria.andTaskIdEqualTo(record.getTaskId());
            }
        }

        List<ElectiveRule> electiveRules=this.select(example);

        if(CollectionUtils.isEmpty(electiveRules)){
            return null;
        }

        if(isGetCourseName){
            List<ElectiveRule> ruleCourseList=new LinkedList<>();
            for(ElectiveRule electiveRule:electiveRules){
                if(StringUtils.isNotBlank(electiveRule.getCourseIds())){
                    electiveRule.setCourseNameList(blueCourseService.selectByIds(electiveRule.getCourseIds(),record.getSchoolid()));
                }
                ruleCourseList.add(electiveRule);
            }
            return ruleCourseList;

        }else {

            return electiveRules;
        }

    }



    @Override
    public BaseMapper getBaseMapper() {
        return this.electiveRuleMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {
        splitClassLayerCourseService.clearCache(schoolId,extend);
    }
}
