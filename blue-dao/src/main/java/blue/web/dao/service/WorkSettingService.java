package blue.web.dao.service;

import blue.commons.enums.OperatorEnum;
import blue.commons.enums.SettingTypeEnum;
import blue.commons.models.KeyValue;
import blue.commons.models.TitleValue;
import blue.dao.base.BaseMapper;
import blue.erp.model.BlueClass;
import blue.erp.model.BlueCourse;
import blue.erp.model.BlueGrade;
import blue.erp.model.SysUser;
import blue.tools.commons.BlueStringUtils;
import blue.tools.constants.BlueContants;
import blue.web.commons.service.TreeService;
import blue.workevaluate.dao.WorkSettingMapper;
import blue.workevaluate.model.WorkSetting;
import blue.workevaluate.model.WorkSettingExample;
import blue.workevaluate.model.WorkTask;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Service
public class WorkSettingService extends BaseService {

    @Autowired
    private WorkSettingMapper workSettingMapper;

    @Autowired
    private BlueCourseService blueCourseService;

    @Autowired
    private BlueClassService blueClassService;

    @Autowired
    private BlueGradeService blueGradeService;

    @Autowired
    private TreeService treeService;

    @Autowired
    private WorkTaskService workTaskService;

    private final String prefix_cache_setting="worksetting";

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        WorkSettingExample example=new WorkSettingExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(WorkSetting record,String parentId,String nodeName,Integer operateType,Integer nodeNum) {
        if(record==null){
            return 0;
        }

        WorkSettingExample example=new WorkSettingExample();
        WorkSettingExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getType()!=null){
            criteria.andTypeEqualTo(record.getType());
        }


        if(StringUtils.isNotBlank(record.getCoursename())){
            criteria.andCoursenameEqualTo(record.getCoursename());
        }

        if(StringUtils.isNotBlank(record.getGradename())){
            criteria.andGradenameEqualTo(record.getGradename());
        }

        clearCache(record.getSchoolid());

        if(StringUtils.isNotBlank(parentId) && StringUtils.isNotBlank(nodeName) && operateType!=null ){

            List<WorkSetting> workSettingList=this.select(example);
            if(workSettingList.size()==1){
                record=workSettingList.get(0);
            }
            String data=record.getData();
            if(OperatorEnum.ADD.getKey()==operateType){
                for(int i=1;i<=nodeNum;i++){
                    data=treeService.getResultTree(parentId,new TitleValue(String.format("节点%s",i),null,null,null),data,operateType);
                }
            }else {
                data=treeService.getResultTree(parentId,new TitleValue(nodeName,null,null,null),record.getData(),operateType);
            }

            record.setData(data);

        }

        return this.baseSave(record,example);

    }


    public HashMap selectByTaskId(Long taskId, Long schoolId){
        if(taskId==null || schoolId==null ) return new HashMap();

        Object cacheObject=redisService.getValue(String.format("%s_%s_%s",prefix_cache_setting,schoolId,taskId));
        if(cacheObject!=null){
            return JSONObject.parseObject(cacheObject.toString(),HashMap.class);
        }

        WorkTask workTask=workTaskService.selectById(taskId,schoolId);
        if(workTask==null || workTask.getCourseid()==null || StringUtils.isBlank(workTask.getClassIds())) return new HashMap();

        WorkSetting workSetting=new WorkSetting();
        workSetting.setCourseid(workTask.getCourseid());
        workSetting.setGradename(workTask.getGradeLevel());
        workSetting.setSchoolid(schoolId);
        List<WorkSetting> workSettingList=selectByObject(workSetting,false);
        HashMap result=new HashMap();
        if(CollectionUtils.isNotEmpty(workSettingList)){
            for(WorkSetting setting:workSettingList){
                result.put(SettingTypeEnum.getByKey(setting.getType()).getValue(),setting);
            }
        }

        redisService.setValue(String.format("%s_%s_%s",prefix_cache_setting,schoolId,taskId),JSONObject.toJSONString(result), BlueContants.twoDay);
        return result;

    }



    public List<WorkSetting> selectByObject(WorkSetting record,boolean showCourseName){


        WorkSettingExample example=new WorkSettingExample();
        WorkSettingExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }

        if(record.getType()!=null){
            criteria.andTypeEqualTo(record.getType());
        }

        if(record.getCourseid()!=null){
            BlueCourse blueCourse=blueCourseService.selectByPrimaryKey(record.getCourseid(),record.getSchoolid(),false);
            if(blueCourse!=null){
                record.setCoursename(blueCourse.getName());
            }
        }


        if(StringUtils.isNotBlank(record.getCoursename())){
            criteria.andCoursenameEqualTo(record.getCoursename());
        }

        if(StringUtils.isNotBlank(record.getGradename())){
            criteria.andGradenameEqualTo(record.getGradename());
        }

        List<WorkSetting>  layerCourseList=this.select(example);


        return layerCourseList;
    }



    @Override
    public BaseMapper getBaseMapper() {
        return this.workSettingMapper;
    }


    @Override
    public void clearCache(Long schoolId,Object... extend) {

        if(extend!=null && extend.length>0){
            redisService.vagueDelete(String.format("%s_%s_%s",prefix_cache_setting,schoolId,extend[0]));
        }else {
            redisService.vagueDelete(String.format("%s_%s",prefix_cache_setting,schoolId));
        }

    }

}
