package blue.web.dao.service;

import blue.dao.base.BaseMapper;
import blue.erp.model.SysUser;
import blue.tools.commons.BlueStringUtils;
import blue.workevaluate.dao.WorkTaskQuestionMapper;
import blue.workevaluate.model.WorkTaskQuestion;
import blue.workevaluate.model.WorkTaskQuestionExample;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkTaskQuestionService extends BaseService {

    @Autowired
    private WorkTaskQuestionMapper workTaskQuestionMapper;

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        WorkTaskQuestionExample example=new WorkTaskQuestionExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(WorkTaskQuestion record) {
        if(record==null || record.getTaskId()==null){
            return 0;
        }

        WorkTaskQuestionExample example=new WorkTaskQuestionExample();
        WorkTaskQuestionExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getTaskId()!=null){
            criteria.andTaskIdEqualTo(record.getTaskId());
        }

        if(StringUtils.isNotBlank(record.getCapabilities())){

        }

        return 0;
    }


    public List<WorkTaskQuestion> selectByObject(WorkTaskQuestion record,boolean showCourseName){

        if(record.getTaskId()==null){
            return null;
        }

        WorkTaskQuestionExample example=new WorkTaskQuestionExample();
        WorkTaskQuestionExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid()).andTaskIdEqualTo(record.getTaskId());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }

        List<WorkTaskQuestion>  layerCourseList=this.select(example);


        return layerCourseList;
    }



    @Override
    public BaseMapper getBaseMapper() {
        return this.workTaskQuestionMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }

}
