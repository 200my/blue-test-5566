package blue.web.dao.service;

import blue.dao.base.BaseMapper;
import blue.erp.model.SysUser;
import blue.splitclass.dao.SplitClassLayerInfoMapper;
import blue.splitclass.model.ElectiveTaskExample;
import blue.splitclass.model.SplitClassLayerInfo;
import blue.splitclass.model.SplitClassLayerInfoExample;
import blue.tools.commons.BlueStringUtils;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SplitClassLayerInfoService extends BaseService {

    @Autowired
    private SplitClassLayerInfoMapper splitClassLayerInfoMapper;

    @Autowired
    private SplitClassLayerCourseService splitClassLayerCourseService;

    private String prefixCache="SplitClassLayerInfoService_";

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        SplitClassLayerInfoExample example=new SplitClassLayerInfoExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(SplitClassLayerInfo record) {
        if(record==null || record.getTaskId()==null){
            return 0;
        }

        SplitClassLayerInfoExample example=new SplitClassLayerInfoExample();
        SplitClassLayerInfoExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getTaskId()!=null){
            criteria.andTaskIdEqualTo(record.getTaskId());
        }
        clearCache(record.getSchoolid(),record.getTaskId());
        return this.baseSave(record,example);

    }


    public List<SplitClassLayerInfo> selectByObject(SplitClassLayerInfo record){

        if(record==null){
            return null;
        }

        ElectiveTaskExample example=new ElectiveTaskExample();
        ElectiveTaskExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }

        return this.select(example);
    }

    public SplitClassLayerInfo selectByTaskId(Long taskId,Long schoolId){

        Object cacheObject=redisService.getValue(String.format("%s_%s_%s",prefixCache,schoolId,taskId));
        if(cacheObject==null){
            SplitClassLayerInfo splitClassLayerInfo=new SplitClassLayerInfo();
            splitClassLayerInfo.setTaskId(taskId);
            splitClassLayerInfo.setSchoolid(schoolId);
            List<SplitClassLayerInfo> layerInfoList=selectByObject(splitClassLayerInfo);
            if(CollectionUtils.isNotEmpty(layerInfoList) && layerInfoList.size()==1){
                redisService.setValue(String.format("%s_%s_%s",prefixCache,schoolId,taskId),JSONObject.toJSONString(layerInfoList.get(0)));
                return layerInfoList.get(0);
            }

            return null;

        }else {
            return JSONObject.parseObject(cacheObject.toString(),SplitClassLayerInfo.class);
        }

    }


    @Override
    public BaseMapper getBaseMapper() {
        return this.splitClassLayerInfoMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {
        redisService.vagueDelete(String.format("%s_%s",prefixCache,schoolId));
        splitClassLayerCourseService.clearCache(schoolId,extend);
    }
}
