package blue.web.dao.service;

import blue.commons.enums.GenderEnum;
import blue.dao.base.BaseMapper;
import blue.erp.model.BlueClass;
import blue.erp.model.BlueCourse;
import blue.erp.model.SysUser;
import blue.splitclass.dao.ElectiveStudentSelectedMapper;
import blue.splitclass.model.ElectiveStudentSelected;
import blue.splitclass.model.ElectiveStudentSelectedExample;
import blue.splitclass.model.ElectiveTask;
import blue.splitclass.model.SelectedInfo;
import blue.tools.commons.BlueStringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Service
public class ElectiveStudentSelectedService extends BaseService {

    @Autowired
    private ElectiveStudentSelectedMapper electiveStudentSelectedMapper;

    @Autowired
    private BlueCourseService blueCourseService;

    @Autowired
    private BlueClassService blueClassService;

    @Autowired
    private ElectiveTaskService electiveTaskService;

    @Autowired
    private SysUserService sysUserService;

    public int deleteBatch(String ids) {

        List idList= BlueStringUtils.convertString2Num(ids,",");
        ElectiveStudentSelectedExample example=new ElectiveStudentSelectedExample();
        example.createCriteria().andIdIn(idList);

        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(ElectiveStudentSelected record) {

        if(record==null || (record.getTaskId()==null && record.getId()==null)){
            return 0;
        }

        ElectiveStudentSelectedExample example=new ElectiveStudentSelectedExample();
        ElectiveStudentSelectedExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }else {
            if(record.getTaskId()!=null){
                criteria.andTaskIdEqualTo(record.getTaskId());
            }

            if(record.getUserId()!=null){
                criteria.andUserIdEqualTo(record.getUserId());
            }
        }


        return this.baseSave(record,example);

    }

    public List<ElectiveStudentSelected> selectByTaskId(Long taskId,Long schoolId,boolean showCourseName){

        if(taskId==null || schoolId==null){
            return null;
        }

        ElectiveStudentSelected electiveStudentSelected=new ElectiveStudentSelected();
        electiveStudentSelected.setTaskId(taskId);
        electiveStudentSelected.setSchoolid(schoolId);

        return selectByObject(electiveStudentSelected,showCourseName);
    }

    public List<ElectiveStudentSelected> selectByObject(ElectiveStudentSelected record,boolean showCourseName){

        if(record==null || record.getSchoolid()==null){
            return null;
        }
        ElectiveStudentSelectedExample example=new ElectiveStudentSelectedExample();
        ElectiveStudentSelectedExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getTaskId()!=null){
            criteria.andTaskIdEqualTo(record.getTaskId());
        }

        if(record.getUserId()!=null){
            criteria.andUserIdEqualTo(record.getUserId());
        }

        if(record.getStatus()!=null && record.getStatus()>0){
            criteria.andStatusEqualTo(record.getStatus());
        }

        if(record.getClassId()!=null && record.getClassId()>0){
            criteria.andClassIdEqualTo(record.getClassId());
        }
        if(StringUtils.isNotBlank(record.getUserName())){
            criteria.andUserNameLike("%"+record.getUserName()+"%");
        }


        List<ElectiveStudentSelected> list=this.select(example);

        if(showCourseName && CollectionUtils.isNotEmpty(list)){
            for(ElectiveStudentSelected electiveStudentSelected:list){
                if(StringUtils.isBlank(electiveStudentSelected.getCourseIds())){
                    continue;
                }
                electiveStudentSelected.setCourseNameList(blueCourseService.selectByIds(electiveStudentSelected.getCourseIds(),record.getSchoolid()));
            }
        }

        for(ElectiveStudentSelected electiveStudentSelected:list){
            BlueClass blueClass=blueClassService.selectByPrimaryKey(electiveStudentSelected.getClassId(),
                    electiveStudentSelected.getSchoolid(),false);
            if(blueClass!=null){
                electiveStudentSelected.setClassName(blueClass.getName());
            }
        }

        return list;
    }

    /**
     * 获取当前任务各个课程的选择个数等情况
     * 注：接口比较慢
     * @param taskId
     * @param schoolId
     */
    public List<SelectedInfo> getCourseSelected(Long taskId,Long classId,Long schoolId,boolean showGenderCount){
       if(taskId==null || schoolId==null){
           return null;
       }
       ElectiveStudentSelected electiveStudentSelected=new ElectiveStudentSelected();
       electiveStudentSelected.setTaskId(taskId);
       electiveStudentSelected.setSchoolid(schoolId);
       if(classId!=null){
           electiveStudentSelected.setClassId(classId);
       }
       List<ElectiveStudentSelected> studentSelectedList=this.selectByObject(electiveStudentSelected,true);
       if(CollectionUtils.isEmpty(studentSelectedList)){
           return null;
       }
       List<SelectedInfo> result=new LinkedList<>();
       HashMap<String,SelectedInfo> course2Selected=new HashMap();
       for(ElectiveStudentSelected studentSelected:studentSelectedList){
          if(CollectionUtils.isNotEmpty(studentSelected.getCourseNameList())){
              for(BlueCourse blueCourse:studentSelected.getCourseNameList()){
                  SelectedInfo selectedInfo=course2Selected.get(blueCourse.getName());
                  if(selectedInfo==null){
                      selectedInfo=new SelectedInfo();
                      selectedInfo.setCourseName(blueCourse.getName());
                      selectedInfo.setCourseId(blueCourse.getId());
                      selectedInfo.setFemaleCount(0);
                      selectedInfo.setMaleCount(0);
                      selectedInfo.setSelectedCount(0);
                  }
                  selectedInfo.setSelectedCount(selectedInfo.getSelectedCount()+1);
                  if(showGenderCount){
                     Object sysUser=  sysUserService.selectByPrimaryKey(studentSelected.getUserId(),studentSelected.getSchoolid());
                     if(sysUser!=null && GenderEnum.getByValue(((SysUser)sysUser).getSex())!=null){
                         if(GenderEnum.Male.getValue().equalsIgnoreCase(((SysUser)sysUser).getSex())){
                             selectedInfo.setMaleCount(selectedInfo.getMaleCount()+1);
                         }else {
                             selectedInfo.setFemaleCount(selectedInfo.getFemaleCount()+1);
                         }
                     }
                  }
                  course2Selected.put(blueCourse.getName(),selectedInfo);
              }
          }
       }

       for(String courseName:course2Selected.keySet()){
           result.add(course2Selected.get(courseName));
       }

       return result;
    }


    @Override
    public BaseMapper getBaseMapper() {
        return this.electiveStudentSelectedMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }
}
