package blue.web.dao.service;

import blue.dao.base.BaseMapper;
import blue.erp.dao.BluePlacardMapper;
import blue.erp.model.BluePlacard;
import blue.erp.model.BluePlacardExample;
import blue.erp.model.SysUser;
import blue.tools.commons.BlueStringUtils;
import blue.tools.commons.Md5Utils;
import blue.tools.constants.BlueContants;
import blue.web.commons.service.OSSService;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class BluePlacardService extends BaseService {

    @Autowired
    private BluePlacardMapper bluePlacardMapper;

    @Autowired
    private OSSService ossService;

    private final String prefix_cache_Id="BluePlacardServiceId";

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        BluePlacardExample example=new BluePlacardExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(BluePlacard record) {
        if(record==null){
            return 0;
        }

        BluePlacardExample example=new BluePlacardExample();
        BluePlacardExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        clearCache(record.getSchoolid());

        if(StringUtils.isNotBlank(record.getContent())){
           String contentMd5= Md5Utils.getMD5(record.getContent());
           if(!contentMd5.equalsIgnoreCase(record.getContentMd5())){
               String fileName=String.format("%s_%s",record.getSchoolid(),record.getTitle());
               boolean result=ossService.putObject(fileName,new ByteArrayInputStream(record.getContent().getBytes()),BlueContants.oss_bucket_placard,null);
               if(result){
                   record.setDocUrl(String.format("%s/%s","https://blue-placard.oss-cn-beijing.aliyuncs.com",fileName));
                   record.setContentMd5(contentMd5);
               }
           }
        }

        return this.baseSave(record,example);

    }


    public List<BluePlacard> selectByObject(BluePlacard record){

        if(record.getSchoolid()==null){
            return null;
        }

        BluePlacardExample example=new BluePlacardExample();
        BluePlacardExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }else {
            if(StringUtils.isNotBlank(record.getTitle())){
                criteria.andTitleEqualTo(record.getTitle());
            }
            if(StringUtils.isNotBlank(record.getReadRole()) && !BlueContants.tag_all.equalsIgnoreCase(record.getReadRole())){
                criteria.andReadRoleLike("%"+record.getReadRole()+"%");
            }
            if(record.getType()!=null && record.getType()>0){
                criteria.andTypeEqualTo(record.getType());
            }
            if(record.getStatus()!=null && record.getStatus()>0){
                criteria.andStatusEqualTo(record.getStatus());
            }
            if(record.getStartTime()!=null){
                criteria.andStartTimeGreaterThanOrEqualTo(record.getStartTime());
            }
            if(record.getEndTime()!=null){
                criteria.andEndTimeLessThanOrEqualTo(record.getEndTime());
            }
            if(StringUtils.isNotBlank(record.getTag())){
                criteria.andTagLike("%"+record.getTag()+"%");
            }
        }

        List<BluePlacard>  resultList=this.select(example);


        return resultList;
    }



    public BluePlacard selectById(Long id,Long schoolId){
        if(id==null || id<=0 || schoolId==null || schoolId<=0){
            return null;
        }

        Object cacheObject=redisService.getValue(String.format("%s_%s_%s",prefix_cache_Id,schoolId,id));
        if(cacheObject!=null){
            return JSONObject.parseObject(cacheObject.toString(),BluePlacard.class);
        }

        BluePlacard bluePlacard= (BluePlacard) this.selectByPrimaryKey(id,schoolId);
        if(bluePlacard!=null){
            redisService.setValue(String.format("%s_%s_%s",prefix_cache_Id,schoolId,id),JSONObject.toJSONString(bluePlacard));
        }
        return bluePlacard;
    }



    @Override
    public BaseMapper getBaseMapper() {
        return this.bluePlacardMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }

}
