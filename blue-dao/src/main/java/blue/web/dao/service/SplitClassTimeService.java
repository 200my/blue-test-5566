package blue.web.dao.service;

import blue.dao.base.BaseMapper;
import blue.erp.model.BlueCourse;
import blue.erp.model.SysUser;
import blue.splitclass.dao.SplitClassTimeMapper;
import blue.splitclass.model.*;
import blue.tools.commons.BlueStringUtils;
import com.alibaba.fastjson.JSONArray;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Service
public class SplitClassTimeService extends BaseService {

    @Autowired
    private SplitClassTimeMapper splitClassTimeMapper;

    @Autowired
    private SplitClassLayerCourseService splitClassLayerCourseService;

    @Autowired
    private SplitClassLayerInfoService splitClassLayerInfoService;

    @Autowired
    private ElectivePublishCourseService electivePublishCourseService;

    @Autowired
    private ElectiveTaskService electiveTaskService;


    private String prefixCache="SplitClassTimeService_";

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        SplitClassTimeExample example=new SplitClassTimeExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(SplitClassTime record) {
        if(record==null || record.getTaskId()==null || record.getSchoolid()==null){
            return 0;
        }

        //单独处理课程数据，如果课程不存在，则创建课程
        if(StringUtils.isNotBlank(record.getTimeInfo())){

        }

        SplitClassTimeExample example=new SplitClassTimeExample();
        SplitClassTimeExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid()).andTaskIdEqualTo(record.getTaskId());

        clearCache(record.getSchoolid());
        return this.baseSave(record,example);

    }


    public List<SplitClassTime> selectByObject(SplitClassTime record){

        if(record==null){
            return null;
        }

        SplitClassTimeExample example=new SplitClassTimeExample();
        SplitClassTimeExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }

        return this.select(example);
    }

    public SplitClassTime selectByTaskId(Long taskId,Long schoolId){

        if(taskId==null || schoolId==null){
            return null;
        }

        SplitClassTime splitClassTime=new SplitClassTime();
        splitClassTime.setTaskId(taskId);
        splitClassTime.setSchoolid(schoolId);
        List<SplitClassTime> layerInfoList=selectByObject(splitClassTime);

        //如果为空，则说明是项目刚刚创建的时候的数据，需要根据课程 层次进行自动生成
        if(CollectionUtils.isEmpty(layerInfoList)){
            List<SplitClassLayerCourse> splitClassLayerCourseList=splitClassLayerCourseService.selectByTaskId(taskId,schoolId);
            if(CollectionUtils.isEmpty(splitClassLayerCourseList)){
                return null;
            }

            HashMap<String,SplitClassLayerCourse> name2LayerCourse=new HashMap();
            for(SplitClassLayerCourse splitClassLayerCourse:splitClassLayerCourseList){
                name2LayerCourse.put(splitClassLayerCourse.getCourseName(),splitClassLayerCourse);
            }

            //查询任务所有的课程
            ElectiveTask electiveTask=electiveTaskService.selectById(taskId,schoolId);
            if(electiveTask==null || CollectionUtils.isEmpty(electiveTask.getList())){
                return null;
            }
            for(BlueCourse blueCourse:electiveTask.getList()){
                if(name2LayerCourse.get(blueCourse.getName())==null){
                    SplitClassLayerCourse splitClassLayerCourse=new SplitClassLayerCourse();
                    splitClassLayerCourse.setCourseName(blueCourse.getName());
                    splitClassLayerCourse.setCourseId(blueCourse.getId());
                    splitClassLayerCourse.setTaskId(taskId);
                    splitClassLayerCourse.setSchoolid(schoolId);
                    splitClassLayerCourse.setLayerCount(1);
                    splitClassLayerCourseList.add(splitClassLayerCourse);
                }
            }


            SplitClassLayerInfo splitClassLayerInfo=splitClassLayerInfoService.selectByTaskId(taskId,schoolId);
            String[] layerInfos={};
            if(splitClassLayerInfo!=null && StringUtils.isNotBlank(splitClassLayerInfo.getNames())){
                layerInfos=splitClassLayerInfo.getNames().split(",");
            }

            List<CourseTime> courseTimeList=new LinkedList<>();
            for(SplitClassLayerCourse splitClassLayerCourse:splitClassLayerCourseList){
                if(splitClassLayerCourse.getLayerCount()>1){
                    for(int i=0;i<splitClassLayerCourse.getLayerCount();i++){
                        CourseTime courseTime=new CourseTime();
                        courseTime.setCourseName(String.format("%s_%s",splitClassLayerCourse.getCourseName(),layerInfos[i]));
                        courseTime.setTime(0);
                        courseTimeList.add(courseTime);
                    }
                }else {
                    CourseTime courseTime=new CourseTime();
                    courseTime.setCourseName(splitClassLayerCourse.getCourseName());
                    courseTime.setTime(0);
                    courseTimeList.add(courseTime);
                }
            }

            if(CollectionUtils.isNotEmpty(courseTimeList)){
                SplitClassTime splitClassTimeItem=new SplitClassTime();
                splitClassTimeItem.setTaskId(taskId);
                splitClassTimeItem.setSchoolid(schoolId);
                splitClassTimeItem.setTimeInfo(JSONArray.toJSONString(courseTimeList));
                layerInfoList.add(splitClassTimeItem);
            }
        }

        if(layerInfoList.size()==1){
            return layerInfoList.get(0);
        }

        return null;
    }

    public List<CourseTime> queryCompose(Long taskId,Long schoolId){
        SplitClassTime splitClassTime=new SplitClassTime();
        splitClassTime.setTaskId(taskId);
        splitClassTime.setSchoolid(schoolId);
        return this.queryCompose(splitClassTime);
    }

    public List<CourseTime> queryCompose(SplitClassTime record){

        if(record==null || record.getTaskId()==null || record.getSchoolid()==null){
            return null;
        }

        List<CourseTime> courseTimeList= splitClassLayerCourseService.getComposeByTaskFromCache(record.getTaskId(),record.getSchoolid());

        if(CollectionUtils.isEmpty(courseTimeList)){
            return null;
        }

        SplitClassTime splitClassTime=this.selectByTaskId(record.getTaskId(),record.getSchoolid());
        HashMap<String,Integer> name2Time=new HashMap<>();
        if(splitClassTime!=null && StringUtils.isNotBlank(splitClassTime.getTimeInfo())){
            List<CourseTime> baseCourseTimeList=JSONArray.parseArray(splitClassTime.getTimeInfo(),CourseTime.class);
            for(CourseTime courseTime:baseCourseTimeList){
                name2Time.put(courseTime.getCourseName(),courseTime.getTime());
            }
        }

        for(CourseTime courseTime:courseTimeList){
            Long timeCount=0L;
            if(CollectionUtils.isEmpty(courseTime.getSelectedCourseInfo())){
                continue;
            }
            for(CourseTime subItem:courseTime.getSelectedCourseInfo()){
                if(name2Time.get(subItem.getCourseName())!=null){
                    subItem.setTime(name2Time.get(subItem.getCourseName()));
                    timeCount+=name2Time.get(subItem.getCourseName());
                }
            }
            courseTime.setTime(timeCount.intValue());
        }

        return courseTimeList;

    }


    @Override
    public BaseMapper getBaseMapper() {
        return this.splitClassTimeMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }
}
