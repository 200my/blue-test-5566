package blue.web.dao.service;

import blue.tools.constants.BlueContants;
import blue.dao.base.BaseMapper;
import blue.erp.dao.SysRoleMapper;
import blue.erp.model.SysRole;
import blue.erp.model.SysRoleExample;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysRoleService extends BaseService{

    @Autowired
    private SysRoleMapper sysRoleMapper;


    public SysRole selectById(Long id,Long schoolId,boolean directFormDb){

        if(id==null || id.longValue()<=0){
            return null;
        }

        Object cacheObject=redisService.getValue(String.format("%s_%s_%s",BlueContants.prefix_role_id, schoolId,id));

        if(!directFormDb && cacheObject!=null){

            return JSONObject.parseObject(cacheObject.toString(),SysRole.class);
        }else {
            Object sysRole=  this.selectByPrimaryKey(id,schoolId);
            if(sysRole==null){
                return null;
            }
            redisService.setValue(String.format("%s_%s_%s",BlueContants.prefix_role_id,schoolId,id),JSONObject.toJSONString(sysRole));
            return (SysRole) sysRole;
        }

    }

    public int save(SysRole record) {

        if(record==null || (StringUtils.isBlank(record.getName()) && record.getId()==null)){
            return 0;
        }

        SysRoleExample example=new SysRoleExample();
        SysRoleExample.Criteria criteria= example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null && record.getId().longValue()>0){
            criteria.andIdEqualTo(record.getId());
        }

        if(StringUtils.isNotBlank(record.getName())){
            criteria.andNameEqualTo(record.getName());
        }


        return this.baseSave(record,example);
    }


    public List<SysRole> getRoles(SysRole record,boolean directFormDb){

        if(record==null){
            return null;
        }

        Object cacheObject=redisService.getValue(String.format("%s_%s_%s_%s",BlueContants.prefix_role_id_name,
                record.getSchoolid(),record.getId()==null?"null":record.getId(),record.getName()));

        if(!directFormDb && cacheObject!=null){

            return JSONArray.parseArray(cacheObject.toString(),SysRole.class);
        }else {
            SysRoleExample example=new SysRoleExample();
            SysRoleExample.Criteria criteria= example.createCriteria();

            if(record.getId()!=null && record.getId().longValue()>0){
                criteria.andIdEqualTo(record.getId());
            }

            if(StringUtils.isNotBlank(record.getName())){
                criteria.andNameEqualTo(record.getName());
            }

//            criteria.andSchoolidEqualTo(record.getSchoolid());
            List<SysRole> sysRoleList=this.select(example);

            if(CollectionUtils.isNotEmpty(sysRoleList)){
                redisService.setValue(String.format("%s_%s_%s_%s",BlueContants.prefix_role_id_name,
                        record.getSchoolid(),record.getId()==null?"null":record.getId(),record.getName()),JSONArray.toJSONString(sysRoleList));
            }


            return sysRoleList;
        }

    }



    @Override
    public BaseMapper getBaseMapper() {
        return sysRoleMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {
        redisService.vagueDelete(BlueContants.prefix_role_id_name+schoolId);
        redisService.vagueDelete(BlueContants.prefix_role_id+schoolId);
    }
}
