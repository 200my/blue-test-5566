package blue.web.dao.service;

import blue.dao.base.BaseMapper;
import blue.erp.dao.BlueSchoolMapper;
import blue.erp.model.BlueSchool;
import blue.erp.model.BlueSchoolExample;
import blue.erp.model.SysRole;
import blue.erp.model.SysRoleExample;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlueSchoolService extends BaseService{

    @Autowired
    private BlueSchoolMapper blueSchoolMapper;



    public int save(BlueSchool record) {

        if(record==null || StringUtils.isBlank(record.getName())){
            return 0;
        }
        BlueSchoolExample example=new BlueSchoolExample();
        BlueSchoolExample.Criteria criteria=example.createCriteria();
        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }else {
            criteria.andNameEqualTo(record.getName());
        }


        return this.baseSave(record,example);
    }


    public List<SysRole> query(BlueSchool record){

        if(record==null || StringUtils.isBlank(record.getName())){
            return null;
        }
        BlueSchoolExample example=new BlueSchoolExample();
        BlueSchoolExample.Criteria criteria=example.createCriteria();
        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }else {
            criteria.andNameLike("%"+record.getName()+"%");
        }

        return this.select(example);

    }



    @Override
    public BaseMapper getBaseMapper() {
        return this.blueSchoolMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }
}
