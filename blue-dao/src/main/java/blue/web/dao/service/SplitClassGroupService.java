package blue.web.dao.service;

import blue.commons.enums.SplitGroupEnum;
import blue.dao.base.BaseMapper;
import blue.erp.model.BlueClass;
import blue.erp.model.SysUser;
import blue.splitclass.dao.SplitClassGroupMapper;
import blue.splitclass.model.SplitClassGroup;
import blue.splitclass.model.SplitClassGroupExample;
import blue.tools.commons.BlueStringUtils;
import com.alibaba.fastjson.JSONArray;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class SplitClassGroupService extends BaseService {

    @Autowired
    private SplitClassGroupMapper splitClassTimeMapper;

    @Autowired
    private BlueClassService blueClassService;

    private String prefixCache="SplitClassGroupService_";

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        SplitClassGroupExample example=new SplitClassGroupExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(SplitClassGroup record) {
        if(record==null || record.getTaskId()==null){
            return 0;
        }

        SplitClassGroupExample example=new SplitClassGroupExample();
        SplitClassGroupExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getTaskId()!=null){
            criteria.andTaskIdEqualTo(record.getTaskId());
        }
        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }else {
            if(StringUtils.isNotBlank(record.getName())){
                criteria.andNameEqualTo(record.getName());
            }
        }
        clearCache(record.getSchoolid());
        return this.baseSave(record,example);

    }


    public List<SplitClassGroup> selectByObject(SplitClassGroup record){

        if(record==null || record.getTaskId()==null){
            return null;
        }

        SplitClassGroupExample example=new SplitClassGroupExample();
        SplitClassGroupExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid()).andTaskIdEqualTo(record.getTaskId());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }
        if(record.getType()!=null){
            criteria.andTypeEqualTo(record.getType());
        }


        List<SplitClassGroup> result= this.select(example);

        if(CollectionUtils.isNotEmpty(result) && record.getType()!=null && SplitGroupEnum.ClassGroup.getKey()==record.getType()){
            for(SplitClassGroup splitClassGroup:result){
                if(StringUtils.isNotBlank(splitClassGroup.getGroupIds())){
                    List<Long> classIdList=BlueStringUtils.convertString2Num(splitClassGroup.getGroupIds(),",");
                    List<String> classNameList=new LinkedList<>();
                    for(Long classId:classIdList){
                       BlueClass blueClass= blueClassService.selectByPrimaryKey(classId,record.getSchoolid(),false);
                       if(blueClass!=null){
                           classNameList.add(blueClass.getName());
                       }
                    }
                    splitClassGroup.setClassNameList(classNameList);
                }
            }
        }

        return result;
    }

    public List<SplitClassGroup> selectByTaskId(Long taskId,Long schoolId){

        Object cacheObject=redisService.getValue(String.format("%s_%s_%s",prefixCache,schoolId,taskId));
        if(cacheObject==null){
            SplitClassGroup splitClassGroup=new SplitClassGroup();
            splitClassGroup.setTaskId(taskId);
            splitClassGroup.setSchoolid(schoolId);
            List<SplitClassGroup> splitClassGroupList=selectByObject(splitClassGroup);
            if(CollectionUtils.isNotEmpty(splitClassGroupList)){
                redisService.setValue(String.format("%s_%s_%s",prefixCache,schoolId,taskId),JSONArray.toJSONString(splitClassGroupList));
                return splitClassGroupList;
            }

            return null;

        }else {
            return JSONArray.parseArray(cacheObject.toString(),SplitClassGroup.class);
        }

    }


    @Override
    public BaseMapper getBaseMapper() {
        return this.splitClassTimeMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }
}
