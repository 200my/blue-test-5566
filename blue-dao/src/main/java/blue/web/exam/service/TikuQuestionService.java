package blue.web.exam.service;

import blue.dao.base.BaseMapper;
import blue.erp.model.SysUser;
import blue.exam.dao.TikuQuestionMapper;
import blue.exam.model.TikuQuestion;
import blue.exam.model.TikuQuestionExample;
import blue.tools.commons.BlueStringUtils;
import blue.tools.commons.Md5Utils;
import blue.web.dao.service.BaseService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TikuQuestionService extends BaseService {

    @Autowired
    private TikuQuestionMapper tikuQuestionMapper;

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        TikuQuestionExample example=new TikuQuestionExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(TikuQuestion record) {
        if(record==null ){
            return 0;
        }

        TikuQuestionExample example=new TikuQuestionExample();
        TikuQuestionExample.Criteria criteria=example.createCriteria();
        if(record.getSchoolid()!=null){
            criteria.andSchoolidEqualTo(record.getSchoolid());
        }

        if(StringUtils.isNotBlank(record.getDataInfo())){
            String md5=Md5Utils.getMD5(record.getDataInfo());
            criteria.andDataMd5EqualTo(md5);
            record.setDataMd5(md5);
        }


        clearCache(record.getSchoolid());
        return this.baseSave(record,example);

    }


    public List<TikuQuestion> selectByObject(TikuQuestion record){

        if(record==null){
            return null;
        }

        TikuQuestionExample example=new TikuQuestionExample();
        TikuQuestionExample.Criteria criteria=example.createCriteria();

        if(record.getSchoolid()!=null && record.getSchoolid()>0){
            criteria.andSchoolidEqualTo(record.getSchoolid());
        }

        if(StringUtils.isNotBlank(record.getDataInfo())){
            String md5=Md5Utils.getMD5(record.getDataInfo());
            criteria.andDataMd5EqualTo(md5);
            record.setDataMd5(md5);
        }

        List<TikuQuestion>  resultList=this.select(example);


        return resultList;
    }



    @Override
    public BaseMapper getBaseMapper() {
        return this.tikuQuestionMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }

}
