package blue.web.exam.service;

import blue.dao.base.BaseMapper;
import blue.erp.model.SysUser;
import blue.exam.dao.TikuSourceMapper;
import blue.exam.model.TikuSource;
import blue.exam.model.TikuSourceExample;
import blue.tools.commons.BlueStringUtils;
import blue.web.dao.service.BaseService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TikuSourceService extends BaseService {

    @Autowired
    private TikuSourceMapper tikuSourceMapper;

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        TikuSourceExample example=new TikuSourceExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(TikuSource record) {
        if(record==null ){
            return 0;
        }

        TikuSourceExample example=new TikuSourceExample();
        TikuSourceExample.Criteria criteria=example.createCriteria();
        if(StringUtils.isNotBlank(record.getSourceId())){
            criteria.andSourceIdEqualTo(record.getSourceId());
        }

        if(StringUtils.isNotBlank(record.getName())){
            criteria.andNameEqualTo(record.getName());
        }

        if(StringUtils.isNotBlank(record.getCity())){
            criteria.andCityEqualTo(record.getCity());
        }

        if(StringUtils.isNotBlank(record.getRegion())){
            criteria.andRegionEqualTo(record.getRegion());
        }

        clearCache(record.getSchoolid());
        return this.baseSave(record,example);

    }


    public List<TikuSource> selectByObject(TikuSource record){

        if(record==null){
            return null;
        }

        TikuSourceExample example=new TikuSourceExample();
        TikuSourceExample.Criteria criteria=example.createCriteria();

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }else {
            if(StringUtils.isNotBlank(record.getSourceId())){
                criteria.andSourceIdEqualTo(record.getSourceId());
            }

            if(StringUtils.isNotBlank(record.getName())){
                criteria.andNameEqualTo(record.getName());
            }

            if(StringUtils.isNotBlank(record.getCity())){
                criteria.andCityEqualTo(record.getCity());
            }

            if(StringUtils.isNotBlank(record.getRegion())){
                criteria.andRegionEqualTo(record.getRegion());
            }
        }

        List<TikuSource>  resultList=this.select(example);


        return resultList;
    }


    @Async("ioTaskExecutor")
    public String getDataFromYunxiao() throws Exception{

        return "result";
    }

    @Override
    public BaseMapper getBaseMapper() {
        return this.tikuSourceMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }

}
