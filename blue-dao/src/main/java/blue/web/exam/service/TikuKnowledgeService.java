package blue.web.exam.service;

import blue.dao.base.BaseMapper;
import blue.erp.model.SysUser;
import blue.exam.dao.TikuKnowledgeMapper;
import blue.exam.model.TikuKnowledge;
import blue.exam.model.TikuKnowledgeExample;
import blue.tools.commons.BlueStringUtils;
import blue.web.dao.service.BaseService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TikuKnowledgeService extends BaseService {

    @Autowired
    private TikuKnowledgeMapper tikuKnowledgeMapper;

    public int deleteBatch(String ids, SysUser sysUser) {
        List idList= BlueStringUtils.convertString2Num(ids,",");
        TikuKnowledgeExample example=new TikuKnowledgeExample();
        example.createCriteria().andIdIn(idList);
        clearCache(sysUser.getSchoolid());
        return this.getBaseMapper().deleteByExample(example);
    }


    public int save(TikuKnowledge record) {
        if(record==null || StringUtils.isBlank(record.getName())){
            return 0;
        }

        TikuKnowledgeExample example=new TikuKnowledgeExample();
        TikuKnowledgeExample.Criteria criteria=example.createCriteria();
        if(record.getSchoolid()!=null){
            criteria.andSchoolidEqualTo(record.getSchoolid());
        }

        criteria.andNameEqualTo(record.getName());

        if(StringUtils.isNotBlank(record.getSourceId())){
            criteria.andSourceIdEqualTo(record.getSourceId());
        }
        clearCache(record.getSchoolid());
        return this.baseSave(record,example);

    }


    public List<TikuKnowledge> selectByObject(TikuKnowledge record){

        if(record==null){
            return null;
        }

        TikuKnowledgeExample example=new TikuKnowledgeExample();
        TikuKnowledgeExample.Criteria criteria=example.createCriteria().andSchoolidEqualTo(record.getSchoolid());

        if(record.getId()!=null){
            criteria.andIdEqualTo(record.getId());
        }

        List<TikuKnowledge>  resultList=this.select(example);


        return resultList;
    }



    @Override
    public BaseMapper getBaseMapper() {
        return this.tikuKnowledgeMapper;
    }

    @Override
    public void clearCache(Long schoolId,Object... extend) {

    }

}
