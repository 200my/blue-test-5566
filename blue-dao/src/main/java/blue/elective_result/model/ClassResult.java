package blue.elective_result.model;

import java.util.Comparator;

public class ClassResult implements Comparable<ClassResult> {

    private String name;
    private Long totalCount;
    private Long selectedCount;
    private Long unSelectedCount;
    private Long taskId;
    private Long id;
    private Float selectedRatio;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public Long getSelectedCount() {
        return selectedCount;
    }

    public void setSelectedCount(Long selectedCount) {
        this.selectedCount = selectedCount;
    }

    public Long getUnSelectedCount() {
        return unSelectedCount;
    }

    public void setUnSelectedCount(Long unSelectedCount) {
        this.unSelectedCount = unSelectedCount;
    }

    public Float getSelectedRatio() {
        return selectedRatio;
    }

    public void setSelectedRatio(Float selectedRatio) {
        this.selectedRatio = selectedRatio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Override
    public int compareTo(ClassResult o) {
        return (int) (this.id-o.id);
    }
}
