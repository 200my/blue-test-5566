package blue.workevaluate.dao;

import blue.dao.base.BaseMapper;
import blue.workevaluate.model.WorkTaskStudentQuestion;
import blue.workevaluate.model.WorkTaskStudentQuestionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WorkTaskStudentQuestionMapper extends BaseMapper {
    long countByExample(WorkTaskStudentQuestionExample example);

    int deleteByExample(WorkTaskStudentQuestionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(WorkTaskStudentQuestion record);

    int insertSelective(WorkTaskStudentQuestion record);

    List<WorkTaskStudentQuestion> selectByExample(WorkTaskStudentQuestionExample example);

    WorkTaskStudentQuestion selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") WorkTaskStudentQuestion record, @Param("example") WorkTaskStudentQuestionExample example);

    int updateByExample(@Param("record") WorkTaskStudentQuestion record, @Param("example") WorkTaskStudentQuestionExample example);

    int updateByPrimaryKeySelective(WorkTaskStudentQuestion record);

    int updateByPrimaryKey(WorkTaskStudentQuestion record);

    List<String> selectDistinctCourse(WorkTaskStudentQuestionExample example);

    List<String> selectDistinctGrade(WorkTaskStudentQuestionExample example);

}