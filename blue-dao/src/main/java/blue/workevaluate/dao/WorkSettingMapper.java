package blue.workevaluate.dao;

import blue.dao.base.BaseMapper;
import blue.workevaluate.model.WorkSetting;
import blue.workevaluate.model.WorkSettingExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WorkSettingMapper extends BaseMapper {
    long countByExample(WorkSettingExample example);

    int deleteByExample(WorkSettingExample example);

    int deleteByPrimaryKey(Long id);

    int insert(WorkSetting record);

    int insertSelective(WorkSetting record);

    List<WorkSetting> selectByExample(WorkSettingExample example);

    WorkSetting selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") WorkSetting record, @Param("example") WorkSettingExample example);

    int updateByExample(@Param("record") WorkSetting record, @Param("example") WorkSettingExample example);

    int updateByPrimaryKeySelective(WorkSetting record);

    int updateByPrimaryKey(WorkSetting record);
}