package blue.workevaluate.dao;

import blue.dao.base.BaseMapper;
import blue.workevaluate.model.WorkTask;
import blue.workevaluate.model.WorkTaskExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WorkTaskMapper extends BaseMapper {
    long countByExample(WorkTaskExample example);

    int deleteByExample(WorkTaskExample example);

    int deleteByPrimaryKey(Long id);

    int insert(WorkTask record);

    int insertSelective(WorkTask record);

    List<WorkTask> selectByExample(WorkTaskExample example);

    WorkTask selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") WorkTask record, @Param("example") WorkTaskExample example);

    int updateByExample(@Param("record") WorkTask record, @Param("example") WorkTaskExample example);

    int updateByPrimaryKeySelective(WorkTask record);

    int updateByPrimaryKey(WorkTask record);
}