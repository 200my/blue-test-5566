package blue.workevaluate.dao;

import blue.dao.base.BaseMapper;
import blue.workevaluate.model.WorkTaskQuestion;
import blue.workevaluate.model.WorkTaskQuestionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WorkTaskQuestionMapper extends BaseMapper {
    long countByExample(WorkTaskQuestionExample example);

    int deleteByExample(WorkTaskQuestionExample example);

    int deleteByPrimaryKey(String id);

    int insert(WorkTaskQuestion record);

    int insertSelective(WorkTaskQuestion record);

    List<WorkTaskQuestion> selectByExample(WorkTaskQuestionExample example);

    WorkTaskQuestion selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") WorkTaskQuestion record, @Param("example") WorkTaskQuestionExample example);

    int updateByExample(@Param("record") WorkTaskQuestion record, @Param("example") WorkTaskQuestionExample example);

    int updateByPrimaryKeySelective(WorkTaskQuestion record);

    int updateByPrimaryKey(WorkTaskQuestion record);
}