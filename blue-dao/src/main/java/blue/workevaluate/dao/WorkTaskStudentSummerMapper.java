package blue.workevaluate.dao;

import blue.dao.base.BaseMapper;
import blue.workevaluate.model.WorkTaskStudentSummer;
import blue.workevaluate.model.WorkTaskStudentSummerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WorkTaskStudentSummerMapper extends BaseMapper {
    long countByExample(WorkTaskStudentSummerExample example);

    int deleteByExample(WorkTaskStudentSummerExample example);

    int deleteByPrimaryKey(Long id);

    int insert(WorkTaskStudentSummer record);

    int insertSelective(WorkTaskStudentSummer record);

    List<WorkTaskStudentSummer> selectByExample(WorkTaskStudentSummerExample example);

    WorkTaskStudentSummer selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") WorkTaskStudentSummer record, @Param("example") WorkTaskStudentSummerExample example);

    int updateByExample(@Param("record") WorkTaskStudentSummer record, @Param("example") WorkTaskStudentSummerExample example);

    int updateByPrimaryKeySelective(WorkTaskStudentSummer record);

    int updateByPrimaryKey(WorkTaskStudentSummer record);
}