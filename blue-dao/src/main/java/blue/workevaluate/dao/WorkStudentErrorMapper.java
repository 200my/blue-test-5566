package blue.workevaluate.dao;

import blue.dao.base.BaseMapper;
import blue.workevaluate.model.WorkStudentError;
import blue.workevaluate.model.WorkStudentErrorExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WorkStudentErrorMapper extends BaseMapper {
    long countByExample(WorkStudentErrorExample example);

    int deleteByExample(WorkStudentErrorExample example);

    int deleteByPrimaryKey(Long id);

    int insert(WorkStudentError record);

    int insertSelective(WorkStudentError record);

    List<WorkStudentError> selectByExample(WorkStudentErrorExample example);

    WorkStudentError selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") WorkStudentError record, @Param("example") WorkStudentErrorExample example);

    int updateByExample(@Param("record") WorkStudentError record, @Param("example") WorkStudentErrorExample example);

    int updateByPrimaryKeySelective(WorkStudentError record);

    int updateByPrimaryKey(WorkStudentError record);
}