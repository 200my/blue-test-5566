package blue.workevaluate.model;

import blue.commons.models.TitleValue;
import blue.dao.base.BaseModel;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WorkTaskStudentQuestion extends BaseModel implements Serializable {
    private Long id;

    private String questionId;

    private Long taskId;

    /**
     * 标记结果
     */
    private Integer tagresult;

    private Byte status;

    private Long userId;

    private String fileurls;

    private Long schoolid;

    private String chapterids;

    private String knowledges;

    private String capabilities;

    /**
     * 题目类型
     */
    private String typeid;

    private String gradeLevel;

    private Date createTime;

    private Date updateTime;

    private String courseName;

    /**
     * 所属班级
     */
    private Long classId;

    //非db数据
    private String taskName;
    //非db数据
    private String name;

    //非db数据
    private String questionIndex;

    //非db数据
    private List<TitleValue> knowledgeList;

    //非db数据
    private List<TitleValue> capabilitieList;

    //拼接学生的作业附件
    private String studentAttachmenturls;
    //拼接教师的作业附件
    private String attachmenturls;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId == null ? null : questionId.trim();
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Integer getTagresult() {
        return tagresult;
    }

    public void setTagresult(Integer tagresult) {
        this.tagresult = tagresult;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFileurls() {
        return fileurls;
    }

    public void setFileurls(String fileurls) {
        this.fileurls = fileurls == null ? null : fileurls.trim();
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getChapterids() {
        return chapterids;
    }

    public void setChapterids(String chapterids) {
        this.chapterids = chapterids == null ? null : chapterids.trim();
    }

    public String getKnowledges() {
        return knowledges;
    }

    public void setKnowledges(String knowledges) {
        this.knowledges = knowledges == null ? null : knowledges.trim();
    }

    public String getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(String capabilities) {
        this.capabilities = capabilities == null ? null : capabilities.trim();
    }

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid == null ? null : typeid.trim();
    }

    public String getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(String gradeLevel) {
        this.gradeLevel = gradeLevel == null ? null : gradeLevel.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestionIndex() {
        return questionIndex;
    }

    public void setQuestionIndex(String questionIndex) {
        this.questionIndex = questionIndex;
    }

    public List<TitleValue> getKnowledgeList() {
        return knowledgeList;
    }

    public void setKnowledgeList(List<TitleValue> knowledgeList) {
        this.knowledgeList = knowledgeList;
    }

    public List<TitleValue> getCapabilitieList() {
        return capabilitieList;
    }

    public void setCapabilitieList(List<TitleValue> capabilitieList) {
        this.capabilitieList = capabilitieList;
    }

    public String getStudentAttachmenturls() {
        return studentAttachmenturls;
    }

    public void setStudentAttachmenturls(String studentAttachmenturls) {
        this.studentAttachmenturls = studentAttachmenturls;
    }

    public String getAttachmenturls() {
        return attachmenturls;
    }

    public void setAttachmenturls(String attachmenturls) {
        this.attachmenturls = attachmenturls;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", questionId=").append(questionId);
        sb.append(", taskId=").append(taskId);
        sb.append(", tagresult=").append(tagresult);
        sb.append(", status=").append(status);
        sb.append(", userId=").append(userId);
        sb.append(", fileurls=").append(fileurls);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", chapterids=").append(chapterids);
        sb.append(", knowledges=").append(knowledges);
        sb.append(", capabilities=").append(capabilities);
        sb.append(", typeid=").append(typeid);
        sb.append(", gradeLevel=").append(gradeLevel);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", courseName=").append(courseName);
        sb.append(", classId=").append(classId);
        sb.append("]");
        return sb.toString();
    }
}