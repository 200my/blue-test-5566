package blue.workevaluate.model;

import org.apache.commons.lang.StringUtils;

public class QuestionAttributes {

    /**
     * title : 标题1
     * question : 内容1
     * answer : 答案1
     * type : 多选题
     * capabilityIds : 78527632-9192-468e-989e-1d96cc748995
     * knowledgeIds : 8432717c-f3db-4bc5-b49a-c207c7280377
     */

    private String title;
    private String question;
    private String answer;
    private String type;
    private String capabilityIds;
    private String knowledgeIds;
    private String chapterIds;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCapabilityIds() {
        return capabilityIds;
    }

    public void setCapabilityIds(String capabilityIds) {
        this.capabilityIds = capabilityIds;
    }

    public String getKnowledgeIds() {
        return knowledgeIds;
    }

    public void setKnowledgeIds(String knowledgeIds) {
        this.knowledgeIds = knowledgeIds;
    }

    public String getChapterIds() {
        return chapterIds;
    }

    public void setChapterIds(String chapterIds) {
        this.chapterIds = chapterIds;
    }

    public void wrap2StudentQuestion(WorkTaskStudentQuestion record){
        if(StringUtils.isNotBlank(this.type)){
            record.setTypeid(this.type);
        }

        if(StringUtils.isNotBlank(this.capabilityIds)){
            record.setCapabilities(this.capabilityIds);
        }

        if(StringUtils.isNotBlank(this.knowledgeIds)){
            record.setKnowledges(this.knowledgeIds);
        }

    }
}
