package blue.workevaluate.model;

import blue.dao.base.BaseCourseModel;
import blue.dao.base.BaseModel;
import com.alibaba.fastjson.annotation.JSONField;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class WorkTask extends BaseCourseModel implements Serializable {
    private Long id;

    private String name;

    private String content;


    //拼接学生的作业附件
    private String studentAttachmenturls;

    private String attachmenturls;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JSONField(format="yyyy-MM-dd hh:mm:ss")
    private Date starttime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JSONField(format="yyyy-MM-dd hh:mm:ss")
    private Date endtime;

    /**
     * 类型 作业 试卷
     */
    private Integer type;

    private Byte status;

    private Integer completecount;

    private Integer studentcount;

    private Date createTime;

    private Date updateTime;

    private String operator;

    private Long courseid;

    private String coursename;

    private Long schoolid;

    /**
     * 记录题目信息
     */
    private String data;

    /**
     * 关联章节,可以关联多个
     */
    private String chapterids;

    /**
     * 问题个数
     */
    private Integer questionCount;

    /**
     * 年级名称，用于查询任务对应的配置信息
     */
    private String gradeLevel;

    /**
     * 多班级
     */
    private String classIds;

    //判定是否可以标记,默认为可标记
    private String canTag="1";

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getAttachmenturls() {
        return attachmenturls;
    }

    public void setAttachmenturls(String attachmenturls) {
        this.attachmenturls = attachmenturls == null ? null : attachmenturls.trim();
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
        if(starttime!=null && starttime.after(new Date())){
            this.setCanTag("未开始");
        }
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
        if(endtime!=null && endtime.before(new Date())){
           this.setCanTag("已结束");
        }
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Integer getCompletecount() {
        return completecount;
    }

    public void setCompletecount(Integer completecount) {
        this.completecount = completecount;
    }

    public Integer getStudentcount() {
        return studentcount;
    }

    public void setStudentcount(Integer studentcount) {
        this.studentcount = studentcount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public Long getCourseid() {
        return courseid;
    }

    public void setCourseid(Long courseid) {
        this.courseid = courseid;
    }

    public String getCoursename() {
        return coursename;
    }

    public void setCoursename(String coursename) {
        this.coursename = coursename == null ? null : coursename.trim();
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data == null ? null : data.trim();
    }

    public String getChapterids() {
        return chapterids;
    }

    public void setChapterids(String chapterids) {
        this.chapterids = chapterids == null ? null : chapterids.trim();
    }

    public Integer getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(Integer questionCount) {
        this.questionCount = questionCount;
    }

    public String getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(String gradeLevel) {
        this.gradeLevel = gradeLevel == null ? null : gradeLevel.trim();
    }

    public String getStudentAttachmenturls() {
        return studentAttachmenturls;
    }

    public String getClassIds() {
        return classIds;
    }

    public void setStudentAttachmenturls(String studentAttachmenturls) {
        this.studentAttachmenturls = studentAttachmenturls;
    }
    public void setClassIds(String classIds) {
        this.classIds = classIds == null ? null : classIds.trim();
    }

    public String getCanTag() {
        return canTag;
    }

    public void setCanTag(String canTag) {
        this.canTag = canTag;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", content=").append(content);
        sb.append(", attachmenturls=").append(attachmenturls);
        sb.append(", starttime=").append(starttime);
        sb.append(", endtime=").append(endtime);
        sb.append(", type=").append(type);
        sb.append(", status=").append(status);
        sb.append(", completecount=").append(completecount);
        sb.append(", studentcount=").append(studentcount);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", operator=").append(operator);
        sb.append(", courseid=").append(courseid);
        sb.append(", coursename=").append(coursename);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", data=").append(data);
        sb.append(", chapterids=").append(chapterids);
        sb.append(", questionCount=").append(questionCount);
        sb.append(", gradeLevel=").append(gradeLevel);
        sb.append(", classIds=").append(classIds);
        sb.append("]");
        return sb.toString();
    }
}