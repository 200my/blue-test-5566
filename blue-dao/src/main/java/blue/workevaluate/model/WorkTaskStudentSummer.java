package blue.workevaluate.model;

import blue.dao.base.BaseModel;
import java.io.Serializable;
import java.util.Date;

public class WorkTaskStudentSummer extends BaseModel implements Serializable {
    private Long id;

    private Long taskId;

    /**
     * 标记结果
     */
    private Integer type;

    private String data;

    private Byte status;

    private Long userId;

    private Date submitTime;

    private Long schoolid;

    private Date createTime;

    private Date updateTime;

    private Integer completeCount;
    /**
     * 用户上传的附件信息
     */
    private String attachmenturls;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data == null ? null : data.trim();
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCompleteCount() {
        return completeCount;
    }
    public String getAttachmenturls() {
        return attachmenturls;
    }

    public void setCompleteCount(Integer completeCount) {
        this.completeCount = completeCount;
    }

    public void setAttachmenturls(String attachmenturls) {
        this.attachmenturls = attachmenturls == null ? null : attachmenturls.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", taskId=").append(taskId);
        sb.append(", type=").append(type);
        sb.append(", data=").append(data);
        sb.append(", status=").append(status);
        sb.append(", userId=").append(userId);
        sb.append(", submitTime=").append(submitTime);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", attachmenturls=").append(attachmenturls);
        sb.append("]");
        return sb.toString();
    }
}