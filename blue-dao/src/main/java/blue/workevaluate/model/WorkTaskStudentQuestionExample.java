package blue.workevaluate.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WorkTaskStudentQuestionExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WorkTaskStudentQuestionExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andQuestionIdIsNull() {
            addCriterion("question_id is null");
            return (Criteria) this;
        }

        public Criteria andQuestionIdIsNotNull() {
            addCriterion("question_id is not null");
            return (Criteria) this;
        }

        public Criteria andQuestionIdEqualTo(String value) {
            addCriterion("question_id =", value, "questionId");
            return (Criteria) this;
        }

        public Criteria andQuestionIdNotEqualTo(String value) {
            addCriterion("question_id <>", value, "questionId");
            return (Criteria) this;
        }

        public Criteria andQuestionIdGreaterThan(String value) {
            addCriterion("question_id >", value, "questionId");
            return (Criteria) this;
        }

        public Criteria andQuestionIdGreaterThanOrEqualTo(String value) {
            addCriterion("question_id >=", value, "questionId");
            return (Criteria) this;
        }

        public Criteria andQuestionIdLessThan(String value) {
            addCriterion("question_id <", value, "questionId");
            return (Criteria) this;
        }

        public Criteria andQuestionIdLessThanOrEqualTo(String value) {
            addCriterion("question_id <=", value, "questionId");
            return (Criteria) this;
        }

        public Criteria andQuestionIdLike(String value) {
            addCriterion("question_id like", value, "questionId");
            return (Criteria) this;
        }

        public Criteria andQuestionIdNotLike(String value) {
            addCriterion("question_id not like", value, "questionId");
            return (Criteria) this;
        }

        public Criteria andQuestionIdIn(List<String> values) {
            addCriterion("question_id in", values, "questionId");
            return (Criteria) this;
        }

        public Criteria andQuestionIdNotIn(List<String> values) {
            addCriterion("question_id not in", values, "questionId");
            return (Criteria) this;
        }

        public Criteria andQuestionIdBetween(String value1, String value2) {
            addCriterion("question_id between", value1, value2, "questionId");
            return (Criteria) this;
        }

        public Criteria andQuestionIdNotBetween(String value1, String value2) {
            addCriterion("question_id not between", value1, value2, "questionId");
            return (Criteria) this;
        }

        public Criteria andTaskIdIsNull() {
            addCriterion("task_id is null");
            return (Criteria) this;
        }

        public Criteria andTaskIdIsNotNull() {
            addCriterion("task_id is not null");
            return (Criteria) this;
        }

        public Criteria andTaskIdEqualTo(Long value) {
            addCriterion("task_id =", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdNotEqualTo(Long value) {
            addCriterion("task_id <>", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdGreaterThan(Long value) {
            addCriterion("task_id >", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdGreaterThanOrEqualTo(Long value) {
            addCriterion("task_id >=", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdLessThan(Long value) {
            addCriterion("task_id <", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdLessThanOrEqualTo(Long value) {
            addCriterion("task_id <=", value, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdIn(List<Long> values) {
            addCriterion("task_id in", values, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdNotIn(List<Long> values) {
            addCriterion("task_id not in", values, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdBetween(Long value1, Long value2) {
            addCriterion("task_id between", value1, value2, "taskId");
            return (Criteria) this;
        }

        public Criteria andTaskIdNotBetween(Long value1, Long value2) {
            addCriterion("task_id not between", value1, value2, "taskId");
            return (Criteria) this;
        }

        public Criteria andTagresultIsNull() {
            addCriterion("tagResult is null");
            return (Criteria) this;
        }

        public Criteria andTagresultIsNotNull() {
            addCriterion("tagResult is not null");
            return (Criteria) this;
        }

        public Criteria andTagresultEqualTo(Integer value) {
            addCriterion("tagResult =", value, "tagresult");
            return (Criteria) this;
        }

        public Criteria andTagresultNotEqualTo(Integer value) {
            addCriterion("tagResult <>", value, "tagresult");
            return (Criteria) this;
        }

        public Criteria andTagresultGreaterThan(Integer value) {
            addCriterion("tagResult >", value, "tagresult");
            return (Criteria) this;
        }

        public Criteria andTagresultGreaterThanOrEqualTo(Integer value) {
            addCriterion("tagResult >=", value, "tagresult");
            return (Criteria) this;
        }

        public Criteria andTagresultLessThan(Integer value) {
            addCriterion("tagResult <", value, "tagresult");
            return (Criteria) this;
        }

        public Criteria andTagresultLessThanOrEqualTo(Integer value) {
            addCriterion("tagResult <=", value, "tagresult");
            return (Criteria) this;
        }

        public Criteria andTagresultIn(List<Integer> values) {
            addCriterion("tagResult in", values, "tagresult");
            return (Criteria) this;
        }

        public Criteria andTagresultNotIn(List<Integer> values) {
            addCriterion("tagResult not in", values, "tagresult");
            return (Criteria) this;
        }

        public Criteria andTagresultBetween(Integer value1, Integer value2) {
            addCriterion("tagResult between", value1, value2, "tagresult");
            return (Criteria) this;
        }

        public Criteria andTagresultNotBetween(Integer value1, Integer value2) {
            addCriterion("tagResult not between", value1, value2, "tagresult");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Long value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Long value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Long value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Long value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Long value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Long value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Long> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Long> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Long value1, Long value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Long value1, Long value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andFileurlsIsNull() {
            addCriterion("fileUrls is null");
            return (Criteria) this;
        }

        public Criteria andFileurlsIsNotNull() {
            addCriterion("fileUrls is not null");
            return (Criteria) this;
        }

        public Criteria andFileurlsEqualTo(String value) {
            addCriterion("fileUrls =", value, "fileurls");
            return (Criteria) this;
        }

        public Criteria andFileurlsNotEqualTo(String value) {
            addCriterion("fileUrls <>", value, "fileurls");
            return (Criteria) this;
        }

        public Criteria andFileurlsGreaterThan(String value) {
            addCriterion("fileUrls >", value, "fileurls");
            return (Criteria) this;
        }

        public Criteria andFileurlsGreaterThanOrEqualTo(String value) {
            addCriterion("fileUrls >=", value, "fileurls");
            return (Criteria) this;
        }

        public Criteria andFileurlsLessThan(String value) {
            addCriterion("fileUrls <", value, "fileurls");
            return (Criteria) this;
        }

        public Criteria andFileurlsLessThanOrEqualTo(String value) {
            addCriterion("fileUrls <=", value, "fileurls");
            return (Criteria) this;
        }

        public Criteria andFileurlsLike(String value) {
            addCriterion("fileUrls like", value, "fileurls");
            return (Criteria) this;
        }

        public Criteria andFileurlsNotLike(String value) {
            addCriterion("fileUrls not like", value, "fileurls");
            return (Criteria) this;
        }

        public Criteria andFileurlsIn(List<String> values) {
            addCriterion("fileUrls in", values, "fileurls");
            return (Criteria) this;
        }

        public Criteria andFileurlsNotIn(List<String> values) {
            addCriterion("fileUrls not in", values, "fileurls");
            return (Criteria) this;
        }

        public Criteria andFileurlsBetween(String value1, String value2) {
            addCriterion("fileUrls between", value1, value2, "fileurls");
            return (Criteria) this;
        }

        public Criteria andFileurlsNotBetween(String value1, String value2) {
            addCriterion("fileUrls not between", value1, value2, "fileurls");
            return (Criteria) this;
        }

        public Criteria andSchoolidIsNull() {
            addCriterion("schoolId is null");
            return (Criteria) this;
        }

        public Criteria andSchoolidIsNotNull() {
            addCriterion("schoolId is not null");
            return (Criteria) this;
        }

        public Criteria andSchoolidEqualTo(Long value) {
            addCriterion("schoolId =", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotEqualTo(Long value) {
            addCriterion("schoolId <>", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidGreaterThan(Long value) {
            addCriterion("schoolId >", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidGreaterThanOrEqualTo(Long value) {
            addCriterion("schoolId >=", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidLessThan(Long value) {
            addCriterion("schoolId <", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidLessThanOrEqualTo(Long value) {
            addCriterion("schoolId <=", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidIn(List<Long> values) {
            addCriterion("schoolId in", values, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotIn(List<Long> values) {
            addCriterion("schoolId not in", values, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidBetween(Long value1, Long value2) {
            addCriterion("schoolId between", value1, value2, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotBetween(Long value1, Long value2) {
            addCriterion("schoolId not between", value1, value2, "schoolid");
            return (Criteria) this;
        }

        public Criteria andChapteridsIsNull() {
            addCriterion("chapterIds is null");
            return (Criteria) this;
        }

        public Criteria andChapteridsIsNotNull() {
            addCriterion("chapterIds is not null");
            return (Criteria) this;
        }

        public Criteria andChapteridsEqualTo(String value) {
            addCriterion("chapterIds =", value, "chapterids");
            return (Criteria) this;
        }

        public Criteria andChapteridsNotEqualTo(String value) {
            addCriterion("chapterIds <>", value, "chapterids");
            return (Criteria) this;
        }

        public Criteria andChapteridsGreaterThan(String value) {
            addCriterion("chapterIds >", value, "chapterids");
            return (Criteria) this;
        }

        public Criteria andChapteridsGreaterThanOrEqualTo(String value) {
            addCriterion("chapterIds >=", value, "chapterids");
            return (Criteria) this;
        }

        public Criteria andChapteridsLessThan(String value) {
            addCriterion("chapterIds <", value, "chapterids");
            return (Criteria) this;
        }

        public Criteria andChapteridsLessThanOrEqualTo(String value) {
            addCriterion("chapterIds <=", value, "chapterids");
            return (Criteria) this;
        }

        public Criteria andChapteridsLike(String value) {
            addCriterion("chapterIds like", value, "chapterids");
            return (Criteria) this;
        }

        public Criteria andChapteridsNotLike(String value) {
            addCriterion("chapterIds not like", value, "chapterids");
            return (Criteria) this;
        }

        public Criteria andChapteridsIn(List<String> values) {
            addCriterion("chapterIds in", values, "chapterids");
            return (Criteria) this;
        }

        public Criteria andChapteridsNotIn(List<String> values) {
            addCriterion("chapterIds not in", values, "chapterids");
            return (Criteria) this;
        }

        public Criteria andChapteridsBetween(String value1, String value2) {
            addCriterion("chapterIds between", value1, value2, "chapterids");
            return (Criteria) this;
        }

        public Criteria andChapteridsNotBetween(String value1, String value2) {
            addCriterion("chapterIds not between", value1, value2, "chapterids");
            return (Criteria) this;
        }

        public Criteria andKnowledgesIsNull() {
            addCriterion("knowledges is null");
            return (Criteria) this;
        }

        public Criteria andKnowledgesIsNotNull() {
            addCriterion("knowledges is not null");
            return (Criteria) this;
        }

        public Criteria andKnowledgesEqualTo(String value) {
            addCriterion("knowledges =", value, "knowledges");
            return (Criteria) this;
        }

        public Criteria andKnowledgesNotEqualTo(String value) {
            addCriterion("knowledges <>", value, "knowledges");
            return (Criteria) this;
        }

        public Criteria andKnowledgesGreaterThan(String value) {
            addCriterion("knowledges >", value, "knowledges");
            return (Criteria) this;
        }

        public Criteria andKnowledgesGreaterThanOrEqualTo(String value) {
            addCriterion("knowledges >=", value, "knowledges");
            return (Criteria) this;
        }

        public Criteria andKnowledgesLessThan(String value) {
            addCriterion("knowledges <", value, "knowledges");
            return (Criteria) this;
        }

        public Criteria andKnowledgesLessThanOrEqualTo(String value) {
            addCriterion("knowledges <=", value, "knowledges");
            return (Criteria) this;
        }

        public Criteria andKnowledgesLike(String value) {
            addCriterion("knowledges like", value, "knowledges");
            return (Criteria) this;
        }

        public Criteria andKnowledgesNotLike(String value) {
            addCriterion("knowledges not like", value, "knowledges");
            return (Criteria) this;
        }

        public Criteria andKnowledgesIn(List<String> values) {
            addCriterion("knowledges in", values, "knowledges");
            return (Criteria) this;
        }

        public Criteria andKnowledgesNotIn(List<String> values) {
            addCriterion("knowledges not in", values, "knowledges");
            return (Criteria) this;
        }

        public Criteria andKnowledgesBetween(String value1, String value2) {
            addCriterion("knowledges between", value1, value2, "knowledges");
            return (Criteria) this;
        }

        public Criteria andKnowledgesNotBetween(String value1, String value2) {
            addCriterion("knowledges not between", value1, value2, "knowledges");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesIsNull() {
            addCriterion("capabilities is null");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesIsNotNull() {
            addCriterion("capabilities is not null");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesEqualTo(String value) {
            addCriterion("capabilities =", value, "capabilities");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesNotEqualTo(String value) {
            addCriterion("capabilities <>", value, "capabilities");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesGreaterThan(String value) {
            addCriterion("capabilities >", value, "capabilities");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesGreaterThanOrEqualTo(String value) {
            addCriterion("capabilities >=", value, "capabilities");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesLessThan(String value) {
            addCriterion("capabilities <", value, "capabilities");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesLessThanOrEqualTo(String value) {
            addCriterion("capabilities <=", value, "capabilities");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesLike(String value) {
            addCriterion("capabilities like", value, "capabilities");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesNotLike(String value) {
            addCriterion("capabilities not like", value, "capabilities");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesIn(List<String> values) {
            addCriterion("capabilities in", values, "capabilities");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesNotIn(List<String> values) {
            addCriterion("capabilities not in", values, "capabilities");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesBetween(String value1, String value2) {
            addCriterion("capabilities between", value1, value2, "capabilities");
            return (Criteria) this;
        }

        public Criteria andCapabilitiesNotBetween(String value1, String value2) {
            addCriterion("capabilities not between", value1, value2, "capabilities");
            return (Criteria) this;
        }

        public Criteria andTypeidIsNull() {
            addCriterion("typeId is null");
            return (Criteria) this;
        }

        public Criteria andTypeidIsNotNull() {
            addCriterion("typeId is not null");
            return (Criteria) this;
        }

        public Criteria andTypeidEqualTo(String value) {
            addCriterion("typeId =", value, "typeid");
            return (Criteria) this;
        }

        public Criteria andTypeidNotEqualTo(String value) {
            addCriterion("typeId <>", value, "typeid");
            return (Criteria) this;
        }

        public Criteria andTypeidGreaterThan(String value) {
            addCriterion("typeId >", value, "typeid");
            return (Criteria) this;
        }

        public Criteria andTypeidGreaterThanOrEqualTo(String value) {
            addCriterion("typeId >=", value, "typeid");
            return (Criteria) this;
        }

        public Criteria andTypeidLessThan(String value) {
            addCriterion("typeId <", value, "typeid");
            return (Criteria) this;
        }

        public Criteria andTypeidLessThanOrEqualTo(String value) {
            addCriterion("typeId <=", value, "typeid");
            return (Criteria) this;
        }

        public Criteria andTypeidLike(String value) {
            addCriterion("typeId like", value, "typeid");
            return (Criteria) this;
        }

        public Criteria andTypeidNotLike(String value) {
            addCriterion("typeId not like", value, "typeid");
            return (Criteria) this;
        }

        public Criteria andTypeidIn(List<String> values) {
            addCriterion("typeId in", values, "typeid");
            return (Criteria) this;
        }

        public Criteria andTypeidNotIn(List<String> values) {
            addCriterion("typeId not in", values, "typeid");
            return (Criteria) this;
        }

        public Criteria andTypeidBetween(String value1, String value2) {
            addCriterion("typeId between", value1, value2, "typeid");
            return (Criteria) this;
        }

        public Criteria andTypeidNotBetween(String value1, String value2) {
            addCriterion("typeId not between", value1, value2, "typeid");
            return (Criteria) this;
        }

        public Criteria andGradeLevelIsNull() {
            addCriterion("grade_level is null");
            return (Criteria) this;
        }

        public Criteria andGradeLevelIsNotNull() {
            addCriterion("grade_level is not null");
            return (Criteria) this;
        }

        public Criteria andGradeLevelEqualTo(String value) {
            addCriterion("grade_level =", value, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andGradeLevelNotEqualTo(String value) {
            addCriterion("grade_level <>", value, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andGradeLevelGreaterThan(String value) {
            addCriterion("grade_level >", value, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andGradeLevelGreaterThanOrEqualTo(String value) {
            addCriterion("grade_level >=", value, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andGradeLevelLessThan(String value) {
            addCriterion("grade_level <", value, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andGradeLevelLessThanOrEqualTo(String value) {
            addCriterion("grade_level <=", value, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andGradeLevelLike(String value) {
            addCriterion("grade_level like", value, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andGradeLevelNotLike(String value) {
            addCriterion("grade_level not like", value, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andGradeLevelIn(List<String> values) {
            addCriterion("grade_level in", values, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andGradeLevelNotIn(List<String> values) {
            addCriterion("grade_level not in", values, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andGradeLevelBetween(String value1, String value2) {
            addCriterion("grade_level between", value1, value2, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andGradeLevelNotBetween(String value1, String value2) {
            addCriterion("grade_level not between", value1, value2, "gradeLevel");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCourseNameIsNull() {
            addCriterion("course_name is null");
            return (Criteria) this;
        }

        public Criteria andCourseNameIsNotNull() {
            addCriterion("course_name is not null");
            return (Criteria) this;
        }

        public Criteria andCourseNameEqualTo(String value) {
            addCriterion("course_name =", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotEqualTo(String value) {
            addCriterion("course_name <>", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameGreaterThan(String value) {
            addCriterion("course_name >", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameGreaterThanOrEqualTo(String value) {
            addCriterion("course_name >=", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLessThan(String value) {
            addCriterion("course_name <", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLessThanOrEqualTo(String value) {
            addCriterion("course_name <=", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLike(String value) {
            addCriterion("course_name like", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotLike(String value) {
            addCriterion("course_name not like", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameIn(List<String> values) {
            addCriterion("course_name in", values, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotIn(List<String> values) {
            addCriterion("course_name not in", values, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameBetween(String value1, String value2) {
            addCriterion("course_name between", value1, value2, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotBetween(String value1, String value2) {
            addCriterion("course_name not between", value1, value2, "courseName");
            return (Criteria) this;
        }

        public Criteria andClassIdIsNull() {
            addCriterion("class_id is null");
            return (Criteria) this;
        }

        public Criteria andClassIdIsNotNull() {
            addCriterion("class_id is not null");
            return (Criteria) this;
        }

        public Criteria andClassIdEqualTo(Long value) {
            addCriterion("class_id =", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdNotEqualTo(Long value) {
            addCriterion("class_id <>", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdGreaterThan(Long value) {
            addCriterion("class_id >", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdGreaterThanOrEqualTo(Long value) {
            addCriterion("class_id >=", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdLessThan(Long value) {
            addCriterion("class_id <", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdLessThanOrEqualTo(Long value) {
            addCriterion("class_id <=", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdIn(List<Long> values) {
            addCriterion("class_id in", values, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdNotIn(List<Long> values) {
            addCriterion("class_id not in", values, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdBetween(Long value1, Long value2) {
            addCriterion("class_id between", value1, value2, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdNotBetween(Long value1, Long value2) {
            addCriterion("class_id not between", value1, value2, "classId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}