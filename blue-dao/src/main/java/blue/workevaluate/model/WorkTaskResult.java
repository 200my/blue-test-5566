package blue.workevaluate.model;

import blue.commons.models.TitleValue;

import java.io.Serializable;
import java.util.List;

public class WorkTaskResult implements Serializable {

    private List<TitleValue> capabilities;

    private List<TitleValue> knowledges;

    private String type;

    private String name;

    private String questionNumber;

    private String taskName;

    private Integer successCount;

    private Integer failedCount;

    private Integer allCount;

    public List<TitleValue> getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(List<TitleValue> capabilities) {
        this.capabilities = capabilities;
    }

    public List<TitleValue> getKnowledges() {
        return knowledges;
    }

    public void setKnowledges(List<TitleValue> knowledges) {
        this.knowledges = knowledges;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(String questionNumber) {
        this.questionNumber = questionNumber;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public Integer getFailedCount() {
        return failedCount;
    }

    public void setFailedCount(Integer failedCount) {
        this.failedCount = failedCount;
    }

    public Integer getAllCount() {
        return allCount;
    }

    public void setAllCount(Integer allCount) {
        this.allCount = allCount;
    }
}