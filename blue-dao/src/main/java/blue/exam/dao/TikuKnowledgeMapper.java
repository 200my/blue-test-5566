package blue.exam.dao;

import blue.dao.base.BaseMapper;
import blue.exam.model.TikuKnowledge;
import blue.exam.model.TikuKnowledgeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TikuKnowledgeMapper extends BaseMapper {
    long countByExample(TikuKnowledgeExample example);

    int deleteByExample(TikuKnowledgeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TikuKnowledge record);

    int insertSelective(TikuKnowledge record);

    List<TikuKnowledge> selectByExampleWithBLOBs(TikuKnowledgeExample example);

    List<TikuKnowledge> selectByExample(TikuKnowledgeExample example);

    TikuKnowledge selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TikuKnowledge record, @Param("example") TikuKnowledgeExample example);

    int updateByExampleWithBLOBs(@Param("record") TikuKnowledge record, @Param("example") TikuKnowledgeExample example);

    int updateByExample(@Param("record") TikuKnowledge record, @Param("example") TikuKnowledgeExample example);

    int updateByPrimaryKeySelective(TikuKnowledge record);

    int updateByPrimaryKeyWithBLOBs(TikuKnowledge record);

    int updateByPrimaryKey(TikuKnowledge record);
}