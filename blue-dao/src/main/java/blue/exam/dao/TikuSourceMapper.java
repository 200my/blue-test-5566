package blue.exam.dao;

import blue.dao.base.BaseMapper;
import blue.exam.model.TikuSource;
import blue.exam.model.TikuSourceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TikuSourceMapper extends BaseMapper {
    long countByExample(TikuSourceExample example);

    int deleteByExample(TikuSourceExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TikuSource record);

    int insertSelective(TikuSource record);

    List<TikuSource> selectByExample(TikuSourceExample example);

    TikuSource selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TikuSource record, @Param("example") TikuSourceExample example);

    int updateByExample(@Param("record") TikuSource record, @Param("example") TikuSourceExample example);

    int updateByPrimaryKeySelective(TikuSource record);

    int updateByPrimaryKey(TikuSource record);
}