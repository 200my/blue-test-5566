package blue.exam.dao;

import blue.dao.base.BaseMapper;
import blue.exam.model.TikuQuestion;
import blue.exam.model.TikuQuestionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TikuQuestionMapper extends BaseMapper {
    long countByExample(TikuQuestionExample example);

    int deleteByExample(TikuQuestionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TikuQuestion record);

    int insertSelective(TikuQuestion record);

    List<TikuQuestion> selectByExampleWithBLOBs(TikuQuestionExample example);

    List<TikuQuestion> selectByExample(TikuQuestionExample example);

    TikuQuestion selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TikuQuestion record, @Param("example") TikuQuestionExample example);

    int updateByExampleWithBLOBs(@Param("record") TikuQuestion record, @Param("example") TikuQuestionExample example);

    int updateByExample(@Param("record") TikuQuestion record, @Param("example") TikuQuestionExample example);

    int updateByPrimaryKeySelective(TikuQuestion record);

    int updateByPrimaryKeyWithBLOBs(TikuQuestion record);

    int updateByPrimaryKey(TikuQuestion record);
}