package blue.exam.model;

import blue.dao.base.BaseModel;
import java.io.Serializable;
import java.util.Date;

public class TikuKnowledge extends BaseModel implements Serializable {
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 获取来源
     */
    private String sourceFrom;

    /**
     * 来源id
     */
    private String sourceId;

    private Date createTime;

    private Date updateTime;

    /**
     * 学校 可选项
     */
    private Long schoolid;

    /**
     * 状态
     */
    private Byte status;

    /**
     * 是否公开
     */
    private Integer isPublic;

    /**
     * 树状结构图
     */
    private String data;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getSourceFrom() {
        return sourceFrom;
    }

    public void setSourceFrom(String sourceFrom) {
        this.sourceFrom = sourceFrom == null ? null : sourceFrom.trim();
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId == null ? null : sourceId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Integer getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Integer isPublic) {
        this.isPublic = isPublic;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data == null ? null : data.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", sourceFrom=").append(sourceFrom);
        sb.append(", sourceId=").append(sourceId);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", status=").append(status);
        sb.append(", isPublic=").append(isPublic);
        sb.append(", data=").append(data);
        sb.append("]");
        return sb.toString();
    }
}