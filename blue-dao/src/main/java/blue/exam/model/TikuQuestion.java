package blue.exam.model;

import blue.dao.base.BaseModel;
import java.io.Serializable;
import java.util.Date;

public class TikuQuestion extends BaseModel implements Serializable {
    private Long id;

    /**
     * 题目类型：填空 选择。。
     */
    private String questionType;

    /**
     * 科目
     */
    private String subject;

    /**
     * 题目引用次数
     */
    private Long referTimes;

    /**
     * 难度系数 
     */
    private Integer difficulty;

    /**
     * 来源
     */
    private String sourceIds;

    /**
     * 高中？初中？
     */
    private String period;

    private Date createTime;

    private Date updateTime;

    /**
     * 喜欢程度
     */
    private Long stars;

    /**
     * 题目的作者
     */
    private String authorId;

    /**
     * 是否公开
     */
    private Integer isPublic;

    /**
     * 状态
     */
    private Byte status;

    private Long schoolid;

    /**
     * 领域：  学校高中，学校初中，it技术 。。。
     */
    private Integer domain;

    /**
     * 题目md5值
     */
    private String dataMd5;

    /**
     * 提示信息
     */
    private String prompts;

    /**
     * 详细解决答案
     */
    private String thoughts;

    /**
     * 答案
     */
    private String answers;

    /**
     * 评论信息
     */
    private String comment;

    /**
     * 题目信息
     */
    private String dataInfo;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType == null ? null : questionType.trim();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject == null ? null : subject.trim();
    }

    public Long getReferTimes() {
        return referTimes;
    }

    public void setReferTimes(Long referTimes) {
        this.referTimes = referTimes;
    }

    public Integer getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Integer difficulty) {
        this.difficulty = difficulty;
    }

    public String getSourceIds() {
        return sourceIds;
    }

    public void setSourceIds(String sourceIds) {
        this.sourceIds = sourceIds == null ? null : sourceIds.trim();
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period == null ? null : period.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getStars() {
        return stars;
    }

    public void setStars(Long stars) {
        this.stars = stars;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId == null ? null : authorId.trim();
    }

    public Integer getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Integer isPublic) {
        this.isPublic = isPublic;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Integer getDomain() {
        return domain;
    }

    public void setDomain(Integer domain) {
        this.domain = domain;
    }

    public String getDataMd5() {
        return dataMd5;
    }

    public void setDataMd5(String dataMd5) {
        this.dataMd5 = dataMd5 == null ? null : dataMd5.trim();
    }

    public String getPrompts() {
        return prompts;
    }

    public void setPrompts(String prompts) {
        this.prompts = prompts == null ? null : prompts.trim();
    }

    public String getThoughts() {
        return thoughts;
    }

    public void setThoughts(String thoughts) {
        this.thoughts = thoughts == null ? null : thoughts.trim();
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers == null ? null : answers.trim();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    public String getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(String dataInfo) {
        this.dataInfo = dataInfo == null ? null : dataInfo.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", questionType=").append(questionType);
        sb.append(", subject=").append(subject);
        sb.append(", referTimes=").append(referTimes);
        sb.append(", difficulty=").append(difficulty);
        sb.append(", sourceIds=").append(sourceIds);
        sb.append(", period=").append(period);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", stars=").append(stars);
        sb.append(", authorId=").append(authorId);
        sb.append(", isPublic=").append(isPublic);
        sb.append(", status=").append(status);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", domain=").append(domain);
        sb.append(", dataMd5=").append(dataMd5);
        sb.append(", prompts=").append(prompts);
        sb.append(", thoughts=").append(thoughts);
        sb.append(", answers=").append(answers);
        sb.append(", comment=").append(comment);
        sb.append(", dataInfo=").append(dataInfo);
        sb.append("]");
        return sb.toString();
    }
}