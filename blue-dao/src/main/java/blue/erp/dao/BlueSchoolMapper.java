package blue.erp.dao;

import blue.dao.base.BaseMapper;
import blue.erp.model.BlueSchool;
import blue.erp.model.BlueSchoolExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BlueSchoolMapper extends BaseMapper {
    long countByExample(BlueSchoolExample example);

    int deleteByExample(BlueSchoolExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BlueSchool record);

    int insertSelective(BlueSchool record);

    List<BlueSchool> selectByExample(BlueSchoolExample example);

    BlueSchool selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BlueSchool record, @Param("example") BlueSchoolExample example);

    int updateByExample(@Param("record") BlueSchool record, @Param("example") BlueSchoolExample example);

    int updateByPrimaryKeySelective(BlueSchool record);

    int updateByPrimaryKey(BlueSchool record);
}