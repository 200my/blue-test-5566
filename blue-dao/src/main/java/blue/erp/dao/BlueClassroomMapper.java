package blue.erp.dao;

import blue.dao.base.BaseMapper;
import blue.erp.model.BlueClassroom;
import blue.erp.model.BlueClassroomExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BlueClassroomMapper extends BaseMapper {
    long countByExample(BlueClassroomExample example);

    int deleteByExample(BlueClassroomExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BlueClassroom record);

    int insertSelective(BlueClassroom record);

    List<BlueClassroom> selectByExample(BlueClassroomExample example);

    BlueClassroom selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BlueClassroom record, @Param("example") BlueClassroomExample example);

    int updateByExample(@Param("record") BlueClassroom record, @Param("example") BlueClassroomExample example);

    int updateByPrimaryKeySelective(BlueClassroom record);

    int updateByPrimaryKey(BlueClassroom record);
}