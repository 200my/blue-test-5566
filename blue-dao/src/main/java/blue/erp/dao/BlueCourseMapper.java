package blue.erp.dao;

import blue.dao.base.BaseMapper;
import blue.erp.model.BlueCourse;
import blue.erp.model.BlueCourseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BlueCourseMapper extends BaseMapper {
    long countByExample(BlueCourseExample example);

    int deleteByExample(BlueCourseExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BlueCourse record);

    int insertSelective(BlueCourse record);

    List<BlueCourse> selectByExample(BlueCourseExample example);

    BlueCourse selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BlueCourse record, @Param("example") BlueCourseExample example);

    int updateByExample(@Param("record") BlueCourse record, @Param("example") BlueCourseExample example);

    int updateByPrimaryKeySelective(BlueCourse record);

    int updateByPrimaryKey(BlueCourse record);
}