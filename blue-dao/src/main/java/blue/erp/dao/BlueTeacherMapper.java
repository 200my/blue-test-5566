package blue.erp.dao;

import blue.dao.base.BaseMapper;
import blue.erp.model.BlueTeacher;
import blue.erp.model.BlueTeacherExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BlueTeacherMapper extends BaseMapper {
    long countByExample(BlueTeacherExample example);

    int deleteByExample(BlueTeacherExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BlueTeacher record);

    int insertSelective(BlueTeacher record);

    List<BlueTeacher> selectByExample(BlueTeacherExample example);

    BlueTeacher selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BlueTeacher record, @Param("example") BlueTeacherExample example);

    int updateByExample(@Param("record") BlueTeacher record, @Param("example") BlueTeacherExample example);

    int updateByPrimaryKeySelective(BlueTeacher record);

    int updateByPrimaryKey(BlueTeacher record);
}