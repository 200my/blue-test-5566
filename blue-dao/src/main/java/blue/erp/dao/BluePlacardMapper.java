package blue.erp.dao;

import blue.dao.base.BaseMapper;
import blue.erp.model.BluePlacard;
import blue.erp.model.BluePlacardExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BluePlacardMapper extends BaseMapper {
    long countByExample(BluePlacardExample example);

    int deleteByExample(BluePlacardExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BluePlacard record);

    int insertSelective(BluePlacard record);

    List<BluePlacard> selectByExample(BluePlacardExample example);

    BluePlacard selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BluePlacard record, @Param("example") BluePlacardExample example);

    int updateByExample(@Param("record") BluePlacard record, @Param("example") BluePlacardExample example);

    int updateByPrimaryKeySelective(BluePlacard record);

    int updateByPrimaryKey(BluePlacard record);
}