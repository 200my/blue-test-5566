package blue.erp.dao;

import blue.dao.base.BaseMapper;
import blue.erp.model.BlueStudent;
import blue.erp.model.BlueStudentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BlueStudentMapper extends BaseMapper {
    long countByExample(BlueStudentExample example);

    int deleteByExample(BlueStudentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BlueStudent record);

    int insertSelective(BlueStudent record);

    List<BlueStudent> selectByExample(BlueStudentExample example);

    BlueStudent selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BlueStudent record, @Param("example") BlueStudentExample example);

    int updateByExample(@Param("record") BlueStudent record, @Param("example") BlueStudentExample example);

    int updateByPrimaryKeySelective(BlueStudent record);

    int updateByPrimaryKey(BlueStudent record);
}