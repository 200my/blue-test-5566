package blue.erp.dao;

import blue.dao.base.BaseMapper;
import blue.erp.model.BlueClass;
import blue.erp.model.BlueClassExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BlueClassMapper extends BaseMapper {
    long countByExample(BlueClassExample example);

    int deleteByExample(BlueClassExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BlueClass record);

    int insertSelective(BlueClass record);

    List<BlueClass> selectByExample(BlueClassExample example);

    BlueClass selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BlueClass record, @Param("example") BlueClassExample example);

    int updateByExample(@Param("record") BlueClass record, @Param("example") BlueClassExample example);

    int updateByPrimaryKeySelective(BlueClass record);

    int updateByPrimaryKey(BlueClass record);
}