package blue.erp.dao;

import blue.dao.base.BaseMapper;
import blue.erp.model.BlueGrade;
import blue.erp.model.BlueGradeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BlueGradeMapper extends BaseMapper {
    long countByExample(BlueGradeExample example);

    int deleteByExample(BlueGradeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BlueGrade record);

    int insertSelective(BlueGrade record);

    List<BlueGrade> selectByExample(BlueGradeExample example);

    BlueGrade selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BlueGrade record, @Param("example") BlueGradeExample example);

    int updateByExample(@Param("record") BlueGrade record, @Param("example") BlueGradeExample example);

    int updateByPrimaryKeySelective(BlueGrade record);

    int updateByPrimaryKey(BlueGrade record);
}