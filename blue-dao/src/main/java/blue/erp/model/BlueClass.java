package blue.erp.model;

import blue.dao.base.BaseGradeModel;
import java.io.Serializable;
import java.util.Date;

public class BlueClass extends BaseGradeModel implements Serializable {
    private Long id;

    private String name;

    /**
     * 年级id
     */
    private Long gradeid;

    /**
     * 系别 计算机系 机电系
     */
    private Integer deptid;

    /**
     * 班主任
     */
    private Long headTeacherid;

    /**
     * 名字不会频繁修改，冗余字段
     */
    private String headTeacherName;

    /**
     * 班长
     */
    private Long squadLeaderid;

    private String squadLeaderName;

    private Integer manCount;

    private Integer femanCount;

    /**
     * 学生人数
     */
    private Integer studentCount;

    private Byte status;

    private Date updateTime;

    private Date createTime;

    private Long schoolid;

    /**
     * 类型，包括行政班 教学班 教研组 等所有group的集合
     */
    private Integer type;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getGradeid() {
        return gradeid;
    }

    public void setGradeid(Long gradeid) {
        this.gradeid = gradeid;
    }

    public Integer getDeptid() {
        return deptid;
    }

    public void setDeptid(Integer deptid) {
        this.deptid = deptid;
    }

    public Long getHeadTeacherid() {
        return headTeacherid;
    }

    public void setHeadTeacherid(Long headTeacherid) {
        this.headTeacherid = headTeacherid;
    }

    public String getHeadTeacherName() {
        return headTeacherName;
    }

    public void setHeadTeacherName(String headTeacherName) {
        this.headTeacherName = headTeacherName == null ? null : headTeacherName.trim();
    }

    public Long getSquadLeaderid() {
        return squadLeaderid;
    }

    public void setSquadLeaderid(Long squadLeaderid) {
        this.squadLeaderid = squadLeaderid;
    }

    public String getSquadLeaderName() {
        return squadLeaderName;
    }

    public void setSquadLeaderName(String squadLeaderName) {
        this.squadLeaderName = squadLeaderName == null ? null : squadLeaderName.trim();
    }

    public Integer getManCount() {
        return manCount;
    }

    public void setManCount(Integer manCount) {
        this.manCount = manCount;
    }

    public Integer getFemanCount() {
        return femanCount;
    }

    public void setFemanCount(Integer femanCount) {
        this.femanCount = femanCount;
    }

    public Integer getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(Integer studentCount) {
        this.studentCount = studentCount;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", gradeid=").append(gradeid);
        sb.append(", deptid=").append(deptid);
        sb.append(", headTeacherid=").append(headTeacherid);
        sb.append(", headTeacherName=").append(headTeacherName);
        sb.append(", squadLeaderid=").append(squadLeaderid);
        sb.append(", squadLeaderName=").append(squadLeaderName);
        sb.append(", manCount=").append(manCount);
        sb.append(", femanCount=").append(femanCount);
        sb.append(", studentCount=").append(studentCount);
        sb.append(", status=").append(status);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", createTime=").append(createTime);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", type=").append(type);
        sb.append("]");
        return sb.toString();
    }
}