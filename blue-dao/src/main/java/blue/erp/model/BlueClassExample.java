package blue.erp.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BlueClassExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BlueClassExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andGradeidIsNull() {
            addCriterion("gradeId is null");
            return (Criteria) this;
        }

        public Criteria andGradeidIsNotNull() {
            addCriterion("gradeId is not null");
            return (Criteria) this;
        }

        public Criteria andGradeidEqualTo(Long value) {
            addCriterion("gradeId =", value, "gradeid");
            return (Criteria) this;
        }

        public Criteria andGradeidNotEqualTo(Long value) {
            addCriterion("gradeId <>", value, "gradeid");
            return (Criteria) this;
        }

        public Criteria andGradeidGreaterThan(Long value) {
            addCriterion("gradeId >", value, "gradeid");
            return (Criteria) this;
        }

        public Criteria andGradeidGreaterThanOrEqualTo(Long value) {
            addCriterion("gradeId >=", value, "gradeid");
            return (Criteria) this;
        }

        public Criteria andGradeidLessThan(Long value) {
            addCriterion("gradeId <", value, "gradeid");
            return (Criteria) this;
        }

        public Criteria andGradeidLessThanOrEqualTo(Long value) {
            addCriterion("gradeId <=", value, "gradeid");
            return (Criteria) this;
        }

        public Criteria andGradeidIn(List<Long> values) {
            addCriterion("gradeId in", values, "gradeid");
            return (Criteria) this;
        }

        public Criteria andGradeidNotIn(List<Long> values) {
            addCriterion("gradeId not in", values, "gradeid");
            return (Criteria) this;
        }

        public Criteria andGradeidBetween(Long value1, Long value2) {
            addCriterion("gradeId between", value1, value2, "gradeid");
            return (Criteria) this;
        }

        public Criteria andGradeidNotBetween(Long value1, Long value2) {
            addCriterion("gradeId not between", value1, value2, "gradeid");
            return (Criteria) this;
        }

        public Criteria andDeptidIsNull() {
            addCriterion("deptId is null");
            return (Criteria) this;
        }

        public Criteria andDeptidIsNotNull() {
            addCriterion("deptId is not null");
            return (Criteria) this;
        }

        public Criteria andDeptidEqualTo(Integer value) {
            addCriterion("deptId =", value, "deptid");
            return (Criteria) this;
        }

        public Criteria andDeptidNotEqualTo(Integer value) {
            addCriterion("deptId <>", value, "deptid");
            return (Criteria) this;
        }

        public Criteria andDeptidGreaterThan(Integer value) {
            addCriterion("deptId >", value, "deptid");
            return (Criteria) this;
        }

        public Criteria andDeptidGreaterThanOrEqualTo(Integer value) {
            addCriterion("deptId >=", value, "deptid");
            return (Criteria) this;
        }

        public Criteria andDeptidLessThan(Integer value) {
            addCriterion("deptId <", value, "deptid");
            return (Criteria) this;
        }

        public Criteria andDeptidLessThanOrEqualTo(Integer value) {
            addCriterion("deptId <=", value, "deptid");
            return (Criteria) this;
        }

        public Criteria andDeptidIn(List<Integer> values) {
            addCriterion("deptId in", values, "deptid");
            return (Criteria) this;
        }

        public Criteria andDeptidNotIn(List<Integer> values) {
            addCriterion("deptId not in", values, "deptid");
            return (Criteria) this;
        }

        public Criteria andDeptidBetween(Integer value1, Integer value2) {
            addCriterion("deptId between", value1, value2, "deptid");
            return (Criteria) this;
        }

        public Criteria andDeptidNotBetween(Integer value1, Integer value2) {
            addCriterion("deptId not between", value1, value2, "deptid");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridIsNull() {
            addCriterion("head_teacherId is null");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridIsNotNull() {
            addCriterion("head_teacherId is not null");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridEqualTo(Long value) {
            addCriterion("head_teacherId =", value, "headTeacherid");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridNotEqualTo(Long value) {
            addCriterion("head_teacherId <>", value, "headTeacherid");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridGreaterThan(Long value) {
            addCriterion("head_teacherId >", value, "headTeacherid");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridGreaterThanOrEqualTo(Long value) {
            addCriterion("head_teacherId >=", value, "headTeacherid");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridLessThan(Long value) {
            addCriterion("head_teacherId <", value, "headTeacherid");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridLessThanOrEqualTo(Long value) {
            addCriterion("head_teacherId <=", value, "headTeacherid");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridIn(List<Long> values) {
            addCriterion("head_teacherId in", values, "headTeacherid");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridNotIn(List<Long> values) {
            addCriterion("head_teacherId not in", values, "headTeacherid");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridBetween(Long value1, Long value2) {
            addCriterion("head_teacherId between", value1, value2, "headTeacherid");
            return (Criteria) this;
        }

        public Criteria andHeadTeacheridNotBetween(Long value1, Long value2) {
            addCriterion("head_teacherId not between", value1, value2, "headTeacherid");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameIsNull() {
            addCriterion("head_teacher_name is null");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameIsNotNull() {
            addCriterion("head_teacher_name is not null");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameEqualTo(String value) {
            addCriterion("head_teacher_name =", value, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameNotEqualTo(String value) {
            addCriterion("head_teacher_name <>", value, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameGreaterThan(String value) {
            addCriterion("head_teacher_name >", value, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameGreaterThanOrEqualTo(String value) {
            addCriterion("head_teacher_name >=", value, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameLessThan(String value) {
            addCriterion("head_teacher_name <", value, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameLessThanOrEqualTo(String value) {
            addCriterion("head_teacher_name <=", value, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameLike(String value) {
            addCriterion("head_teacher_name like", value, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameNotLike(String value) {
            addCriterion("head_teacher_name not like", value, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameIn(List<String> values) {
            addCriterion("head_teacher_name in", values, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameNotIn(List<String> values) {
            addCriterion("head_teacher_name not in", values, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameBetween(String value1, String value2) {
            addCriterion("head_teacher_name between", value1, value2, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andHeadTeacherNameNotBetween(String value1, String value2) {
            addCriterion("head_teacher_name not between", value1, value2, "headTeacherName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidIsNull() {
            addCriterion("squad_leaderId is null");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidIsNotNull() {
            addCriterion("squad_leaderId is not null");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidEqualTo(Long value) {
            addCriterion("squad_leaderId =", value, "squadLeaderid");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidNotEqualTo(Long value) {
            addCriterion("squad_leaderId <>", value, "squadLeaderid");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidGreaterThan(Long value) {
            addCriterion("squad_leaderId >", value, "squadLeaderid");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidGreaterThanOrEqualTo(Long value) {
            addCriterion("squad_leaderId >=", value, "squadLeaderid");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidLessThan(Long value) {
            addCriterion("squad_leaderId <", value, "squadLeaderid");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidLessThanOrEqualTo(Long value) {
            addCriterion("squad_leaderId <=", value, "squadLeaderid");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidIn(List<Long> values) {
            addCriterion("squad_leaderId in", values, "squadLeaderid");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidNotIn(List<Long> values) {
            addCriterion("squad_leaderId not in", values, "squadLeaderid");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidBetween(Long value1, Long value2) {
            addCriterion("squad_leaderId between", value1, value2, "squadLeaderid");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderidNotBetween(Long value1, Long value2) {
            addCriterion("squad_leaderId not between", value1, value2, "squadLeaderid");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameIsNull() {
            addCriterion("squad_leader_name is null");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameIsNotNull() {
            addCriterion("squad_leader_name is not null");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameEqualTo(String value) {
            addCriterion("squad_leader_name =", value, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameNotEqualTo(String value) {
            addCriterion("squad_leader_name <>", value, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameGreaterThan(String value) {
            addCriterion("squad_leader_name >", value, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameGreaterThanOrEqualTo(String value) {
            addCriterion("squad_leader_name >=", value, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameLessThan(String value) {
            addCriterion("squad_leader_name <", value, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameLessThanOrEqualTo(String value) {
            addCriterion("squad_leader_name <=", value, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameLike(String value) {
            addCriterion("squad_leader_name like", value, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameNotLike(String value) {
            addCriterion("squad_leader_name not like", value, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameIn(List<String> values) {
            addCriterion("squad_leader_name in", values, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameNotIn(List<String> values) {
            addCriterion("squad_leader_name not in", values, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameBetween(String value1, String value2) {
            addCriterion("squad_leader_name between", value1, value2, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andSquadLeaderNameNotBetween(String value1, String value2) {
            addCriterion("squad_leader_name not between", value1, value2, "squadLeaderName");
            return (Criteria) this;
        }

        public Criteria andManCountIsNull() {
            addCriterion("man_count is null");
            return (Criteria) this;
        }

        public Criteria andManCountIsNotNull() {
            addCriterion("man_count is not null");
            return (Criteria) this;
        }

        public Criteria andManCountEqualTo(Integer value) {
            addCriterion("man_count =", value, "manCount");
            return (Criteria) this;
        }

        public Criteria andManCountNotEqualTo(Integer value) {
            addCriterion("man_count <>", value, "manCount");
            return (Criteria) this;
        }

        public Criteria andManCountGreaterThan(Integer value) {
            addCriterion("man_count >", value, "manCount");
            return (Criteria) this;
        }

        public Criteria andManCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("man_count >=", value, "manCount");
            return (Criteria) this;
        }

        public Criteria andManCountLessThan(Integer value) {
            addCriterion("man_count <", value, "manCount");
            return (Criteria) this;
        }

        public Criteria andManCountLessThanOrEqualTo(Integer value) {
            addCriterion("man_count <=", value, "manCount");
            return (Criteria) this;
        }

        public Criteria andManCountIn(List<Integer> values) {
            addCriterion("man_count in", values, "manCount");
            return (Criteria) this;
        }

        public Criteria andManCountNotIn(List<Integer> values) {
            addCriterion("man_count not in", values, "manCount");
            return (Criteria) this;
        }

        public Criteria andManCountBetween(Integer value1, Integer value2) {
            addCriterion("man_count between", value1, value2, "manCount");
            return (Criteria) this;
        }

        public Criteria andManCountNotBetween(Integer value1, Integer value2) {
            addCriterion("man_count not between", value1, value2, "manCount");
            return (Criteria) this;
        }

        public Criteria andFemanCountIsNull() {
            addCriterion("feman_count is null");
            return (Criteria) this;
        }

        public Criteria andFemanCountIsNotNull() {
            addCriterion("feman_count is not null");
            return (Criteria) this;
        }

        public Criteria andFemanCountEqualTo(Integer value) {
            addCriterion("feman_count =", value, "femanCount");
            return (Criteria) this;
        }

        public Criteria andFemanCountNotEqualTo(Integer value) {
            addCriterion("feman_count <>", value, "femanCount");
            return (Criteria) this;
        }

        public Criteria andFemanCountGreaterThan(Integer value) {
            addCriterion("feman_count >", value, "femanCount");
            return (Criteria) this;
        }

        public Criteria andFemanCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("feman_count >=", value, "femanCount");
            return (Criteria) this;
        }

        public Criteria andFemanCountLessThan(Integer value) {
            addCriterion("feman_count <", value, "femanCount");
            return (Criteria) this;
        }

        public Criteria andFemanCountLessThanOrEqualTo(Integer value) {
            addCriterion("feman_count <=", value, "femanCount");
            return (Criteria) this;
        }

        public Criteria andFemanCountIn(List<Integer> values) {
            addCriterion("feman_count in", values, "femanCount");
            return (Criteria) this;
        }

        public Criteria andFemanCountNotIn(List<Integer> values) {
            addCriterion("feman_count not in", values, "femanCount");
            return (Criteria) this;
        }

        public Criteria andFemanCountBetween(Integer value1, Integer value2) {
            addCriterion("feman_count between", value1, value2, "femanCount");
            return (Criteria) this;
        }

        public Criteria andFemanCountNotBetween(Integer value1, Integer value2) {
            addCriterion("feman_count not between", value1, value2, "femanCount");
            return (Criteria) this;
        }

        public Criteria andStudentCountIsNull() {
            addCriterion("student_count is null");
            return (Criteria) this;
        }

        public Criteria andStudentCountIsNotNull() {
            addCriterion("student_count is not null");
            return (Criteria) this;
        }

        public Criteria andStudentCountEqualTo(Integer value) {
            addCriterion("student_count =", value, "studentCount");
            return (Criteria) this;
        }

        public Criteria andStudentCountNotEqualTo(Integer value) {
            addCriterion("student_count <>", value, "studentCount");
            return (Criteria) this;
        }

        public Criteria andStudentCountGreaterThan(Integer value) {
            addCriterion("student_count >", value, "studentCount");
            return (Criteria) this;
        }

        public Criteria andStudentCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("student_count >=", value, "studentCount");
            return (Criteria) this;
        }

        public Criteria andStudentCountLessThan(Integer value) {
            addCriterion("student_count <", value, "studentCount");
            return (Criteria) this;
        }

        public Criteria andStudentCountLessThanOrEqualTo(Integer value) {
            addCriterion("student_count <=", value, "studentCount");
            return (Criteria) this;
        }

        public Criteria andStudentCountIn(List<Integer> values) {
            addCriterion("student_count in", values, "studentCount");
            return (Criteria) this;
        }

        public Criteria andStudentCountNotIn(List<Integer> values) {
            addCriterion("student_count not in", values, "studentCount");
            return (Criteria) this;
        }

        public Criteria andStudentCountBetween(Integer value1, Integer value2) {
            addCriterion("student_count between", value1, value2, "studentCount");
            return (Criteria) this;
        }

        public Criteria andStudentCountNotBetween(Integer value1, Integer value2) {
            addCriterion("student_count not between", value1, value2, "studentCount");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andSchoolidIsNull() {
            addCriterion("schoolId is null");
            return (Criteria) this;
        }

        public Criteria andSchoolidIsNotNull() {
            addCriterion("schoolId is not null");
            return (Criteria) this;
        }

        public Criteria andSchoolidEqualTo(Long value) {
            addCriterion("schoolId =", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotEqualTo(Long value) {
            addCriterion("schoolId <>", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidGreaterThan(Long value) {
            addCriterion("schoolId >", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidGreaterThanOrEqualTo(Long value) {
            addCriterion("schoolId >=", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidLessThan(Long value) {
            addCriterion("schoolId <", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidLessThanOrEqualTo(Long value) {
            addCriterion("schoolId <=", value, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidIn(List<Long> values) {
            addCriterion("schoolId in", values, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotIn(List<Long> values) {
            addCriterion("schoolId not in", values, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidBetween(Long value1, Long value2) {
            addCriterion("schoolId between", value1, value2, "schoolid");
            return (Criteria) this;
        }

        public Criteria andSchoolidNotBetween(Long value1, Long value2) {
            addCriterion("schoolId not between", value1, value2, "schoolid");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}