package blue.erp.model;

import blue.dao.base.BaseModel;
import com.alibaba.fastjson.JSONArray;

import java.io.Serializable;
import java.util.List;

public class SysResourceExtend implements Serializable {

    private String title;
    private String key;
    private List<SysResourceExtend> children;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<SysResourceExtend> getChildren() {
        return children;
    }

    public void setChildren(List<SysResourceExtend> children) {
        this.children = children;
    }
}