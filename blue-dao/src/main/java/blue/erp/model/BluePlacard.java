package blue.erp.model;

import blue.dao.base.BaseModel;
import com.alibaba.fastjson.annotation.JSONField;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class BluePlacard extends BaseModel implements Serializable {
    private Long id;

    /**
     * 标题
     */
    private String title;

    /**
     * 文章地址
     */
    private String docUrl;

    /**
     * 阅读次数
     */
    private Long readCount;

    /**
     * 类型 公告，导读 通知 。。
     */
    private Integer type;

    private String tag;

    private Byte status;

    private String operator;

    /**
     * 学校id
     */
    private Long schoolid;

    private Date createTime;

    private Date updateTime;

    /**
     * 0~100的整数，数值越大，优先级越高
     */
    private Integer priority;

    /**
     * 文章有效期，开始时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JSONField(format="yyyy-MM-dd hh:mm:ss")
    private Date startTime;

    /**
     * 文章有效期，开始时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JSONField(format="yyyy-MM-dd hh:mm:ss")
    private Date endTime;

    /**
     * 阅读者的权限
     */
    private String readRole;

    //用于接收前端文本数据
    private String content;
    /**
     * 内容的md5数据
     */
    private String contentMd5;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getDocUrl() {
        return docUrl;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl == null ? null : docUrl.trim();
    }

    public Long getReadCount() {
        return readCount;
    }

    public void setReadCount(Long readCount) {
        this.readCount = readCount;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag == null ? null : tag.trim();
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getReadRole() {
        return readRole;
    }

    public void setReadRole(String readRole) {
        this.readRole = readRole == null ? null : readRole.trim();
    }

    public String getContent() {
        return content;
    }
    public String getContentMd5() {
        return contentMd5;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public void setContentMd5(String contentMd5) {
        this.contentMd5 = contentMd5 == null ? null : contentMd5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", title=").append(title);
        sb.append(", docUrl=").append(docUrl);
        sb.append(", readCount=").append(readCount);
        sb.append(", type=").append(type);
        sb.append(", tag=").append(tag);
        sb.append(", status=").append(status);
        sb.append(", operator=").append(operator);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", priority=").append(priority);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", readRole=").append(readRole);
        sb.append(", contentMd5=").append(contentMd5);
        sb.append("]");
        return sb.toString();
    }
}