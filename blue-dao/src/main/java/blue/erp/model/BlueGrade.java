package blue.erp.model;

import blue.dao.base.BaseModel;
import java.io.Serializable;
import java.util.Date;

public class BlueGrade extends BaseModel implements Serializable {
    private Long id;

    /**
     * 2017届
     */
    private String name;

    /**
     * 年级主任
     */
    private Long gradeDirectorid;

    private Long schoolid;

    /**
     * 等级名称 比如 高一 高二
     */
    private String gradeLevel;

    private Byte status;

    private Date createTime;

    private Date updateTime;

    private String type;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getGradeDirectorid() {
        return gradeDirectorid;
    }

    public void setGradeDirectorid(Long gradeDirectorid) {
        this.gradeDirectorid = gradeDirectorid;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(String gradeLevel) {
        this.gradeLevel = gradeLevel == null ? null : gradeLevel.trim();
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", gradeDirectorid=").append(gradeDirectorid);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", gradeLevel=").append(gradeLevel);
        sb.append(", status=").append(status);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", type=").append(type);
        sb.append("]");
        return sb.toString();
    }
}