package blue.erp.model.extend;

import blue.commons.models.KeyValue;
import blue.erp.model.SysUser;

import java.util.List;

public class BlueTeacherExtend extends SysUser {

    private List<String> courseNames;

    private List<Long> gradeIds;
    private List<String> gradeNames;

    private List<Long> classIds;
    private List<String> classNames;

    private List<KeyValue> courseList;
    private List<KeyValue> classList;


    public List<String> getCourseNames() {
        return courseNames;
    }

    public void setCourseNames(List<String> courseNames) {
        this.courseNames = courseNames;
    }


    public List<String> getGradeNames() {
        return gradeNames;
    }

    public void setGradeNames(List<String> gradeNames) {
        this.gradeNames = gradeNames;
    }

    public List<String> getClassNames() {
        return classNames;
    }

    public void setClassNames(List<String> classNames) {
        this.classNames = classNames;
    }


    public List<KeyValue> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<KeyValue> courseList) {
        this.courseList = courseList;
    }

    public List<KeyValue> getClassList() {
        return classList;
    }

    public void setClassList(List<KeyValue> classList) {
        this.classList = classList;
    }
}
