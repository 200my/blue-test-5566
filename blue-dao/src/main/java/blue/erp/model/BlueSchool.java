package blue.erp.model;

import blue.dao.base.BaseModel;
import java.io.Serializable;
import java.util.Date;

public class BlueSchool extends BaseModel implements Serializable {
    private Long id;

    private String name;

    /**
     * 位置信息
     */
    private String location;

    /**
     * 联系信息
     */
    private String contact;

    /**
     * 超级管理员
     */
    private Long superUserid;

    private Byte status;

    private Date updateTime;

    private Date createTime;

    /**
     * 录入人员
     */
    private String operator;

    /**
     * 每个学校的学生，注册的登录账号的前缀不同
     */
    private String usernameSuffix;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location == null ? null : location.trim();
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact == null ? null : contact.trim();
    }

    public Long getSuperUserid() {
        return superUserid;
    }

    public void setSuperUserid(Long superUserid) {
        this.superUserid = superUserid;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public String getUsernameSuffix() {
        return usernameSuffix;
    }

    public void setUsernameSuffix(String usernameSuffix) {
        this.usernameSuffix = usernameSuffix == null ? null : usernameSuffix.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", location=").append(location);
        sb.append(", contact=").append(contact);
        sb.append(", superUserid=").append(superUserid);
        sb.append(", status=").append(status);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", createTime=").append(createTime);
        sb.append(", operator=").append(operator);
        sb.append(", usernameSuffix=").append(usernameSuffix);
        sb.append("]");
        return sb.toString();
    }
}