package blue.erp.model;

import blue.dao.base.BaseClassModel;

import java.io.Serializable;
import java.util.Date;

public class BlueClassroom extends BaseClassModel implements Serializable {
    private Long id;

    private String name;

    /**
     * 教室容量
     */
    private Short capacity;

    /**
     * 位置信息
     */
    private String location;

    /**
     * 所属学校
     */
    private Long schoolid;

    /**
     * 编码
     */
    private String roomno;

    /**
     * 所属班级
     */
    private String underclassids;

    private Long undergradeid;

    private Byte status;

    /**
     * 扩展字段
     */
    private String extend;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Short getCapacity() {
        return capacity;
    }

    public void setCapacity(Short capacity) {
        this.capacity = capacity;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location == null ? null : location.trim();
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getRoomno() {
        return roomno;
    }

    public void setRoomno(String roomno) {
        this.roomno = roomno == null ? null : roomno.trim();
    }

    public String getUnderclassids() {
        return underclassids;
    }

    public void setUnderclassids(String underclassids) {
        this.underclassids = underclassids == null ? null : underclassids.trim();
    }

    public Long getUndergradeid() {
        return undergradeid;
    }

    public void setUndergradeid(Long undergradeid) {
        this.undergradeid = undergradeid;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend == null ? null : extend.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", capacity=").append(capacity);
        sb.append(", location=").append(location);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", roomno=").append(roomno);
        sb.append(", underclassids=").append(underclassids);
        sb.append(", undergradeid=").append(undergradeid);
        sb.append(", status=").append(status);
        sb.append(", extend=").append(extend);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append("]");
        return sb.toString();
    }
}