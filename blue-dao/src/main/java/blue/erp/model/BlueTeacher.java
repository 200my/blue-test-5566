package blue.erp.model;

import blue.dao.base.BaseModel;
import blue.erp.model.extend.BlueTeacherExtend;

import java.io.Serializable;
import java.util.Date;

public class BlueTeacher extends BlueTeacherExtend implements Serializable {
    private Long id;

    private Long userid;

    /**
     * 教师所属班级列表
     */
    private String classids;

    /**
     * 教师的课程列表
     */
    private String courseids;

    /**
     * 教师所属的年级
     */
    private String gradeids;

    /**
     * 所属学校
     */
    private Long schoolid;



    /**
     * 教师类型，1：普通代课教师，2：教务教师 
     */
    private Byte type;

    private Date createTime;

    private Date updateTime;

    private String realName;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getClassids() {
        return classids;
    }

    public void setClassids(String classids) {
        this.classids = classids == null ? null : classids.trim();
    }

    public String getCourseids() {
        return courseids;
    }

    public void setCourseids(String courseids) {
        this.courseids = courseids == null ? null : courseids.trim();
    }

    public String getGradeids() {
        return gradeids;
    }

    public void setGradeids(String gradeids) {
        this.gradeids = gradeids == null ? null : gradeids.trim();
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }


    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userid=").append(userid);
        sb.append(", classids=").append(classids);
        sb.append(", courseids=").append(courseids);
        sb.append(", gradeids=").append(gradeids);
        sb.append(", schoolid=").append(schoolid);
        sb.append(", type=").append(type);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", realName=").append(realName);
        sb.append("]");
        return sb.toString();
    }
}