package blue.web.cache.service;

import blue.cache.constants.BlueContants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by jianshan on 2018/1/13.
 */
@Service
public class RedisService {

    @Autowired
    StringRedisTemplate stringRedisTemplate;


    public void setValue(String key,String value){
       setValue(key,value, BlueContants.day);
    }
    @Async
    public void setValue(String key,String value,long timeout){
        ValueOperations<String, String> valueOperations= stringRedisTemplate.opsForValue();
        valueOperations.set(key,value,timeout, TimeUnit.SECONDS);
    }

    public Object getValue(String key){
        return stringRedisTemplate.opsForValue().get(key);
    }


    public Long increase(String key){

        return stringRedisTemplate.opsForValue().increment(key,1);
    }

    public boolean delete(String key){

        Long result= stringRedisTemplate.delete(stringRedisTemplate.keys(key));
        if(result>0){
            return true;
        }

        return false;
    }
    @Async
    public boolean vagueDelete(String key){

        Long result= stringRedisTemplate.delete(stringRedisTemplate.keys(key+"*"));
        if(result>0){
            return true;
        }

        return false;
    }

    public Set<String> getVagueKeys(String key){
        return stringRedisTemplate.keys(key+"*");
    }

    public Long add2Set(String key, String value){
        return stringRedisTemplate.opsForSet().add(key, value);
    }

    public Long remove2Set(String key, String value){
        return stringRedisTemplate.opsForSet().remove(key,value);
    }

    public Set getFromSet(String key){
        return stringRedisTemplate.opsForSet().members(key);
    }

    public Long add2Set(String key, Set set){

        if(StringUtils.isBlank(key) || CollectionUtils.isEmpty(set)){
            return 0L;
        }

        return stringRedisTemplate.opsForSet().add(key, StringUtils.join(set,",").split(","));
    }

    public Long getSetSize(String key){
        return stringRedisTemplate.opsForSet().size(key);
    }

    //获取集合并集数量
    public int getSetSinter(String prefix, List keys){

        List completeKeys=new LinkedList();
        for(Object key:keys){
            completeKeys.add(String.format("%s_%s",prefix,key));
        }

        return stringRedisTemplate.opsForSet().intersect(completeKeys.get(0).toString(),completeKeys.subList(1,completeKeys.size())).size();
    }

}
