package blue.web.cache.service;


import blue.cache.constants.BlueContants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by jianshan on 2018/1/14.
 */
@Service
public class SecurityUtils {

    @Autowired
    private RedisService redisService;


    public boolean isFrequentlyVisit(String key,String ip,int frequentThreshold){

        if(redisService.getValue(key+ip)!=null){

            if(Integer.decode(redisService.getValue(key+ip).toString())> frequentThreshold){
                return true;
            }
            int visitCount=Integer.decode(redisService.getValue(key+ip).toString())+1;

            redisService.setValue(key+ip,String.valueOf(visitCount),BlueContants.day);
        }else {
            redisService.setValue(key+ip,String.valueOf(1), BlueContants.day);
        }

        return false;
    }

}
