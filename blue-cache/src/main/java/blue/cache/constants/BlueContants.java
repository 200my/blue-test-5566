package blue.cache.constants;

public class BlueContants {

    public static final String PARAM_ERROR="E0001";

    //中文转体  错误码
    public static final String DATA_TOO_LONG="E0002";


    public static final String SUCCESS="A0000";

    public static final String ERROR="E0000";

    //时间耗时
    public static final long day=86400;
    public static final long hour=3600;
    public static final long week=604800;
    public static final long month=2419200;
    public static final long year=31536000;


    public static final String ipBlackList="ipBlackList";

    public static final String securityIp="securityIp_";


    public static final String separator=",";





}
