package blue.tools.model;

import blue.tools.constants.BlueContants;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * Created by 2018-08-01 19:39:13
 */
public class PageResponseData {

    private String code;
    private String msg;
    private List list;
    private Pagination pagination;

    public PageResponseData() {
    }

    public PageResponseData(String code, String msg, PageInfo pageInfo) {
        this.code = code;
        this.msg = msg;

        if(pageInfo!=null){
            this.list=pageInfo.getList();
            Pagination paginationData=new Pagination();
            paginationData.setCurrent(pageInfo.getPageNum());
            paginationData.setPageSize(pageInfo.getPageSize());
            paginationData.setTotal((int) pageInfo.getTotal());
            this.pagination=paginationData;
        }

    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public boolean isSucess(){
        if(StringUtils.isNotBlank(this.code) && BlueContants.SUCCESS.equalsIgnoreCase(this.code)){
            return true;
        }

        return false;
    }



}
