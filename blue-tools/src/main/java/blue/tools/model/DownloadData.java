package blue.tools.model;

/**
 * Created by jianshan on 2017/12/17.
 */
public class DownloadData {

    private String fileName;
    private byte[] data;
    private Integer length;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
        if(data!=null && data.length>0){
            setLength(data.length);
        }
    }

    public Integer getLength() {
        return length;
    }

    private void setLength(Integer length) {
        this.length = length;
    }
}
