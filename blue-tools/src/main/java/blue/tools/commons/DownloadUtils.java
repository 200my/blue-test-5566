package blue.tools.commons;



import blue.tools.model.DownloadData;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.OutputStream;

/**
 * Created by jianshan on 2017/12/17.
 */
public class DownloadUtils {

    public static void download(DownloadData data, final HttpServletResponse response) throws Exception{
        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"" + data.getFileName() + "\"");
        response.addHeader("Content-Length", "" + data.getLength());
        response.setContentType("application/octet-stream;charset=UTF-8");
        OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
        outputStream.write(data.getData());
        outputStream.flush();
        outputStream.close();
    }


//    @ResponseBody
//    @RequestMapping(value = "downPhotoById")
//    public void down(final HttpServletResponse response) throws Exception{
//
//
//    }
}
