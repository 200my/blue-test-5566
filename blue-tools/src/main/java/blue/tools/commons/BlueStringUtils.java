package blue.tools.commons;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class BlueStringUtils {


    public static List<Long> convertString2Num(String data, String split){

        List result=new LinkedList();

        if(StringUtils.isBlank(data)){
            return result;
        }

        List<String> idStringList= Arrays.asList(data.trim().split(split));

        for(String id:idStringList){
            result.add(Long.decode(id));
        }

        return result;

    }

    public static List<Long> getIntersection(String data1,String data2){

        List list1=convertString2Num(data1,",");
        List list2=convertString2Num(data2,",");
        if(CollectionUtils.isEmpty(list1) || CollectionUtils.isEmpty(list2)){
            return null;
        }

        boolean hasItem=list1.retainAll(list2);
        if(hasItem){
            return list1;
        }

        return null;
    }

    public static int containChineseNumber(String data){
        int result=-1;
        if(StringUtils.isBlank(data)){
            return result;
        }
        HashMap<String,Integer> chineseMap=BlueListUtils.getChineseNumberMap();
        for(char c:data.toCharArray()){
            if(chineseMap.get(c)!=null){
                return chineseMap.get(c);
            }
        }

        return result;
    }

    public static int sortString(String first,String second){
        if(StringUtils.isBlank(first)){
            return -1;
        }
        if(StringUtils.isBlank(second)){
            return 1;
        }
        char[] firstchar=first.toCharArray();
        char[] secondchar=second.toCharArray();
        int length=firstchar.length>secondchar.length?secondchar.length:firstchar.length;
        HashMap<String,Integer> chineseMap=BlueListUtils.getChineseNumberMap();
        for(int i=0;i<length;i++){
            if(chineseMap.get(String.valueOf(firstchar[i]))!=null && chineseMap.get(String.valueOf(secondchar[i]))!=null){
                return chineseMap.get(String.valueOf(firstchar[i]))-chineseMap.get(String.valueOf(secondchar[i]));
            }
            if(firstchar[i]!=secondchar[i]){
                return firstchar[i]-secondchar[i];
            }
        }

        return firstchar.length-secondchar.length;
    }

}
