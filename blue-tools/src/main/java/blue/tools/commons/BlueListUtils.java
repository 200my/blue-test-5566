package blue.tools.commons;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class BlueListUtils {

    private static final String[] simpleDigits = { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九" };

    public static HashMap<String,Integer> getChineseNumberMap(){
        HashMap result=new HashMap();
        int index=0;
        for(String chineseNumber:Arrays.asList(simpleDigits)){
            result.put(chineseNumber,index++);
        }
        return result;
    }


}
