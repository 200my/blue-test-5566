package blue.tools.commons;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jianshan on 2017/12/13.
 */
public class DateUtils {

    private static final String defaultDateTimePattern = "yyyy-MM-dd HH:mm:ss";

    private static final String defaultDatePattern = "yyyy-MM-dd";


    public static Long getTodayTime(){
        SimpleDateFormat fo = new SimpleDateFormat();
        Date date = new Date(System.currentTimeMillis());
        fo.applyPattern("yyyy-MM-dd");

        try {
            date = fo.parse(DateFormatUtils.format(date, "yyyy-MM-dd"));
        } catch (Exception e) {
        }

        return date.getTime();
    }

    public static String dateHuman(Date date){
        if(date==null){
            return StringUtils.EMPTY;
        }

        return DateFormatUtils.format(date,defaultDateTimePattern);

    }

    public static void main(String[] args) {

        System.out.println(DateUtils.getTodayTime());
    }

}
