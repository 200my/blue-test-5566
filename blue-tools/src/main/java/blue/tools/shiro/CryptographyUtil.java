package blue.tools.shiro;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * @author 作者 E-mail：
 * @version 创建时间：2016年2月16日上午9:58:01 类说明
 */
public class CryptographyUtil {

    public static String encBase64(String str) {
        return Base64.encodeToString(str.getBytes());
    }
    public static String decBase64(String str){
        return Base64.decodeToString(str);
    }
    public static String md5(String str,String salt){
        return new Md5Hash(str,salt).toString();
    }
    public static void main(String[] args) {
        String password="123456";
        String salt="8d78869f470951332959580424d4bf4f";
//        System.out.println(CryptographyUtil.encBase64(password));
//        System.out.println(CryptographyUtil.decBase64("MTIzNA=="));
        System.out.println(CryptographyUtil.md5(password, "8d78869f470951332959580424d4bf4f"));
    }
}