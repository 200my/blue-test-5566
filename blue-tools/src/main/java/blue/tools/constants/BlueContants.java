package blue.tools.constants;

public class BlueContants {

    public static final String PARAM_ERROR="E0001";

    //中文转体  错误码
    public static final String DATA_TOO_LONG="E0002";


    public static final String SUCCESS="A0000";

    public static final String ERROR="E0000";

    //时间耗时
    public static final long twoDay=172800;
    public static final long day=86400;
    public static final long hour=3600;
    public static final long week=604800;
    public static final long month=2419200;
    public static final long year=31536000;


    public static final String ipBlackList="ipBlackList";

    public static final String securityIp="securityIp_";


    public static final String separator=",";



    //cache前缀
    public static final String prefix_gradeId="gradeId_";
    public static final String prefix_gradeName="gradeName_";

    public static final String prefix_school_gradeList="school_gradeList_";
    public static final String prefix_schoolInfo="school_info";

    public static final String prefix_className_GradeList_="className_GradeList_";

    public static final String prefix_role_id_name="role_id_name_";
    public static final String prefix_role_id="role_id";

    public static final String prefix_courseName="courseName_";
    public static final String prefix_courseId="courseId_";
    public static final String prefix_courseIdList="courseIdList_";
    public static final String prefix_courseSchool="courseSchool_";

    public static final String prefix_classId="classId_";

    //文件缓存
    public static final String prefix_fileMd5_2Url="file_md5_2_url";

    //用户相关
    public static final String prefix_studentCount="studentCount";

    //选课任务相关
    public static final String prefix_elective_taskId="elective_taskId";


    //选课结果 cache前缀
    public static final String prefix_selected_class="selected_class";//班级选课数量
    public static final String prefix_selected_oneSubject_total="selected_oneSubject_total";//单科总的选课数量
    public static final String prefix_selected_oneSubject_male="selected_oneSubject_male";//单科男生选课数量
    public static final String prefix_selected_oneSubject_female="selected_oneSubject_female";//单科女生选课数量

    //定时任务
    public static final String prefix_timertask_studentquestion="timertask_studentquestion";//单科女生选课数量


    public static final String prefix_courseLayerCompose="TASK_COURSE_LAYER_COMPOSE_";


    public static final String special_role_student="学生";
    public static final String special_role_teacher="教师";

    public static final String import_excel_student="学生账号";
    public static final String import_excel_teacher="教师账号";
    public static final String import_excel_virtualGroup="虚拟班组";
    public static final String import_excel_courselayer="课程分层模板-勿改";

    public static final String[] junior_default_course={"语文","数学","英语","化学","物理","生物","历史","政治","地理","体育","美术","音乐"};

    public static final String[] senior_default_course={"语文","数学","英语","化学","物理","生物","历史","政治","地理","体育","美术","音乐"};


    //oss
    public static final String oss_blue_erp_ui="blue-erp-ui";
    public static final String oss_shanghai_url="https://blue-erp-ui.oss-cn-shanghai.aliyuncs.com";
    public static final String oss_path_workevaluate="workevaluate";
    public static final String oss_bucket_rundata="blue-rundata";
    public static final String oss_bucket_placard="blue-placard";
    public static final String oss_url_rundata="https://blue-rundata.oss-cn-beijing.aliyuncs.com";
    public static final String oss_region_beijing="beijing";
    public static final String oss_region_shanghai="shanghai";
    public static final String oss_region_hangzhou="hangzhou";


    public static final String tag_all="-1";

}
