package blue.commons.enums;

public enum OperatorEnum {

    ADD(1,"增加"),MODIFY(2,"修改"),DELETE(3,"删除");

    int key;
    String value;

    OperatorEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static OperatorEnum getByValue(String value) {
        for (OperatorEnum gradeEnum : values()) {
            if (gradeEnum.getValue().equalsIgnoreCase(value)) return gradeEnum;
        }
        return null;
    }

    public static OperatorEnum getByKey(int key) {
        for (OperatorEnum gradeEnum : values()) {
            if (gradeEnum.getKey()==key) return gradeEnum;
        }
        return null;
    }

}
