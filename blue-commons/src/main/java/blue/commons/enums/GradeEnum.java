package blue.commons.enums;

import blue.commons.models.KeyValue;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public enum GradeEnum {

    Primary(1,"小学"),Junior(2,"初中"),Senior(3,"高中"),College(4,"大学");

    int key;
    String value;

    GradeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    GradeEnum() {
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private static HashMap<String, List> name2Details=new HashMap<>();//类型到详情的映射，比如高中 映射为 高一 高二 高三
    private static final String[] simpleDigits = { "一", "二", "三", "四", "五", "六", "七", "八", "九" };
    private static Integer index=1;//年级明细的id，保证id唯一
    public static HashMap<String, KeyValue> detailName2Object=new HashMap<>();//
    public static HashMap<Long, KeyValue> detailId2Object=new HashMap<>();//
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(GradeEnum.class);
    static {

        try{
            //初始化大学
            name2Details.put(College.getValue(),getFormatList("大%s", Arrays.asList(simpleDigits),4));
            //初始化高中
            name2Details.put(Senior.getValue(),getFormatList("高%s", Arrays.asList(simpleDigits),3));
            //初始化初中
            name2Details.put(Junior.getValue(),getFormatList("初%s", Arrays.asList(simpleDigits),3));
            //初始化小学
            name2Details.put(Primary.getValue(),getFormatList("%s年级", Arrays.asList(simpleDigits),6));
        }catch(Exception e){
            logger.error(e.getMessage());
        }

    }

    public static GradeEnum getGradeByValue(String value) {
        for (GradeEnum gradeEnum : values()) {
            if (gradeEnum.getValue().equalsIgnoreCase(value)) return gradeEnum;
        }
        return null;
    }

    public static GradeEnum getGradeByKey(int key) {
        for (GradeEnum gradeEnum : values()) {
            if (gradeEnum.getKey()==key) return gradeEnum;
        }
        return null;
    }

    public static List<KeyValue> getGradeDetailsByValue(String value) {

        if(StringUtils.isBlank(value) || name2Details==null ||name2Details.size()==0){
            return null;
        }
        return name2Details.get(value);
    }

    public static List<KeyValue> getGradeDetailsByKey(int key) {
        if(getGradeByKey(key)==null || name2Details==null ||name2Details.size()==0){
            return null;
        }
        return name2Details.get(getGradeByKey(key).value);
    }

    private static List<KeyValue> getFormatList(String format,List<String> formatList,int count){
        List<KeyValue> result=new LinkedList<>();
        if(StringUtils.isBlank(format) || CollectionUtils.isEmpty(formatList) || count<=0){
            return result;
        }

        for(int i=0;i<count;i++){
            KeyValue keyValue=new KeyValue();
            keyValue.setKey(index++);
            keyValue.setValue(String.format(format,formatList.get(i)));
            detailName2Object.put(keyValue.getValue().toString(),keyValue);
            detailId2Object.put(Long.decode(keyValue.getKey().toString()),keyValue);
            result.add(keyValue);
        }

        return result;
    }


}
