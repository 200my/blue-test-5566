package blue.commons.enums;

public enum UserTypeEnum {

    Student(1,"学生"),Teacher(2,"教师");

    int key;
    String value;

    UserTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static UserTypeEnum getByValue(String value) {
        for (UserTypeEnum gradeEnum : values()) {
            if (gradeEnum.getValue().equalsIgnoreCase(value)) return gradeEnum;
        }
        return null;
    }

    public static UserTypeEnum getByKey(int key) {
        for (UserTypeEnum gradeEnum : values()) {
            if (gradeEnum.getKey()==key) return gradeEnum;
        }
        return null;
    }

}
