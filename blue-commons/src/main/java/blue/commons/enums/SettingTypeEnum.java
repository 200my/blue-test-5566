package blue.commons.enums;

public enum SettingTypeEnum {

    grade(1,"grade","年级"),course(2,"course","课程"),knowledge(3,"knowledge","知识点"),capability(4,"capability","能力素养"),chapter(5,"chapter","教材目录");

    int key;
    String value;
    String desc;

    SettingTypeEnum(int key, String value,String desc) {
        this.key = key;
        this.value = value;
        this.desc=desc;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static SettingTypeEnum getByValue(String value) {
        for (SettingTypeEnum gradeEnum : values()) {
            if (gradeEnum.getValue().equalsIgnoreCase(value)) return gradeEnum;
        }
        return null;
    }

    public static SettingTypeEnum getByKey(int key) {
        for (SettingTypeEnum gradeEnum : values()) {
            if (gradeEnum.getKey()==key) return gradeEnum;
        }
        return null;
    }

}
