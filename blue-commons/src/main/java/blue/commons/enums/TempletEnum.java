package blue.commons.enums;

public enum TempletEnum {

    ADD(1,"增加"),MODIFY(2,"修改"),DELETE(3,"删除");

    int key;
    String value;

    TempletEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static TempletEnum getByValue(String value) {
        for (TempletEnum itemEnum : values()) {
            if (itemEnum.getValue().equalsIgnoreCase(value)) return itemEnum;
        }
        return null;
    }

    public static TempletEnum getByKey(int key) {
        for (TempletEnum itemEnum : values()) {
            if (itemEnum.getKey()==key) return itemEnum;
        }
        return null;
    }

}
