package blue.commons.enums;

public enum PermissionEnum {

    All("*","全部"),Create("create","创建"),Update("update","更新"),
    Delete("delete","删除"),View("view","查看");

    String value;
    String desc;

    PermissionEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static PermissionEnum getStatusByValue(String value) {
        for (PermissionEnum statusEnum : values()) {
            if (value.equalsIgnoreCase(statusEnum.getValue())) return statusEnum;
        }
        return PermissionEnum.View;
    }

}
