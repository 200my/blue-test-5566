package blue.commons.enums;

public enum SplitGroupEnum {

    SubjectGroup(1,"课程组"),ClassGroup(2,"班级组");

    int key;
    String value;

    SplitGroupEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static SplitGroupEnum getByValue(String value) {
        for (SplitGroupEnum gradeEnum : values()) {
            if (gradeEnum.getValue().equalsIgnoreCase(value)) return gradeEnum;
        }
        return null;
    }

    public static SplitGroupEnum getByKey(int key) {
        for (SplitGroupEnum gradeEnum : values()) {
            if (gradeEnum.getKey()==key) return gradeEnum;
        }
        return null;
    }

}
