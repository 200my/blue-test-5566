package blue.commons.enums;

public enum GenderEnum {

    Male(1,"男"),Female(2,"女"),Default(3,"默认");

    int key;
    String value;

    GenderEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static GenderEnum getByValue(String value) {
        for (GenderEnum gradeEnum : values()) {
            if (gradeEnum.getValue().equalsIgnoreCase(value)) return gradeEnum;
        }
        return null;
    }

    public static GenderEnum getByKey(int key) {
        for (GenderEnum gradeEnum : values()) {
            if (gradeEnum.getKey()==key) return gradeEnum;
        }
        return null;
    }

}
