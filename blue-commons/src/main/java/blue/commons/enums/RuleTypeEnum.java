package blue.commons.enums;

public enum RuleTypeEnum {

    Main(1,"主规则"),Required(2,"必选");

    int key;
    String value;

    RuleTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static RuleTypeEnum getByValue(String value) {
        for (RuleTypeEnum gradeEnum : values()) {
            if (gradeEnum.getValue().equalsIgnoreCase(value)) return gradeEnum;
        }
        return null;
    }

    public static RuleTypeEnum getByKey(int key) {
        for (RuleTypeEnum gradeEnum : values()) {
            if (gradeEnum.getKey()==key) return gradeEnum;
        }
        return null;
    }

}
