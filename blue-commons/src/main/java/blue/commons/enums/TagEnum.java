package blue.commons.enums;

public enum TagEnum {

    Wrong(1,"Wrong","错误"),Right(2,"Right","正确");

    int key;
    String value;
    String desc;

    TagEnum(int key, String value, String desc) {
        this.key = key;
        this.value = value;
        this.desc=desc;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static TagEnum getByValue(String value) {
        for (TagEnum gradeEnum : values()) {
            if (gradeEnum.getValue().equalsIgnoreCase(value)) return gradeEnum;
        }
        return null;
    }

    public static TagEnum getByKey(int key) {
        for (TagEnum gradeEnum : values()) {
            if (gradeEnum.getKey()==key) return gradeEnum;
        }
        return null;
    }

}
