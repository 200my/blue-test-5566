package blue.commons.enums;

public enum StatusEnum {
    //值越大，代表状态越稳定
    Init(1,"初始化"),Normal(2,"正常"),Deleted(0,"删除"),RuleAlter(3,"规则修改"),Handled(4,"已处理"),Complete(10,"已完成");

    int value;
    String desc;

    StatusEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static StatusEnum getStatusByValue(int value) {
        for (StatusEnum statusEnum : values()) {
            if (statusEnum.getValue() == value) return statusEnum;
        }
        return StatusEnum.Normal;
    }

}
