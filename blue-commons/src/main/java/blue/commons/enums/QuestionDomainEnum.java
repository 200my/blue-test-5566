package blue.commons.enums;

public enum QuestionDomainEnum {

    Primary(1,"小学"),Junior(2,"初中"),Senior(3,"高中"),College(4,"大学"),IT_WRITE(2,"计算机笔试"),IT_INTERVIEW(3,"计算机面试");

    int key;
    String value;

    QuestionDomainEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static QuestionDomainEnum getByValue(String value) {
        for (QuestionDomainEnum itemEnum : values()) {
            if (itemEnum.getValue().equalsIgnoreCase(value)) return itemEnum;
        }
        return null;
    }

    public static QuestionDomainEnum getByKey(int key) {
        for (QuestionDomainEnum itemEnum : values()) {
            if (itemEnum.getKey()==key) return itemEnum;
        }
        return null;
    }

}
