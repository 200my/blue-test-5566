package blue.commons.enums;

public enum TaskTypeEnum {

    Course(1,"课程"),Compose(2,"组合"),layerCompose(3,"分层组合");

    int key;
    String value;

    TaskTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static TaskTypeEnum getByValue(String value) {
        for (TaskTypeEnum gradeEnum : values()) {
            if (gradeEnum.getValue().equalsIgnoreCase(value)) return gradeEnum;
        }
        return null;
    }

    public static TaskTypeEnum getByKey(int key) {
        for (TaskTypeEnum gradeEnum : values()) {
            if (gradeEnum.getKey()==key) return gradeEnum;
        }
        return null;
    }

}
