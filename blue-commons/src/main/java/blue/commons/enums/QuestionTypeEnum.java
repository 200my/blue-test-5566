package blue.commons.enums;

public enum QuestionTypeEnum {

    SINGLE_CHOICE(1,"单选"),MULTI_CHOICE(2,"多选"),DELETE(3,"删除");

    int key;
    String value;

    QuestionTypeEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static QuestionTypeEnum getByValue(String value) {
        for (QuestionTypeEnum itemEnum : values()) {
            if (itemEnum.getValue().equalsIgnoreCase(value)) return itemEnum;
        }
        return null;
    }

    public static QuestionTypeEnum getByKey(int key) {
        for (QuestionTypeEnum itemEnum : values()) {
            if (itemEnum.getKey()==key) return itemEnum;
        }
        return null;
    }

}
