package blue.commons.models;

import blue.tools.commons.BlueStringUtils;

public class KeyValue implements Comparable<KeyValue>{

    private Object key;
    private Object value;

    public KeyValue() {
    }

    public KeyValue(Object key, Object value) {
        this.key = key;
        this.value = value;
    }

    public Object getKey() {
        return key;
    }

    public void setKey(Object key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public int compareTo(KeyValue o) {
        if(o.getValue()==null || this.getValue()==null){
            return -1;
        }

        return BlueStringUtils.sortString(this.getValue().toString(),o.getValue().toString());
    }
}
