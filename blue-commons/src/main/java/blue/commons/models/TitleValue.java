package blue.commons.models;

import java.util.List;

public class TitleValue {

    /**
     * title : Node1
     * value : 0-0
     * key : 0-0
     * children : [{"title":"Child Node1","value":"0-0-1","key":"0-0-1"},{"title":"Child Node2","value":"0-0-2","key":"0-0-2"}]
     */

    private String title;
    private String value;
    private String key;
    private String extend;
    private String index;//顺序信息

    public TitleValue() {
    }

    public TitleValue(String title, String value, String key, String extend) {
        this.title = title;
        this.value = value;
        this.key = key;
        this.extend = extend;
    }

    private List<TitleValue> children;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        this.setKey(value);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    public List<TitleValue> getChildren() {
        return children;
    }

    public void setChildren(List<TitleValue> children) {
        this.children = children;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}


