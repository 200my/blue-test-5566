package blue.web.monitor;

import blue.web.model.DingTalkRobot;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedList;
import java.util.List;

@Service
public class DingTalkService {

    @Autowired
    private RestTemplate restTemplate;


    public void sendMsg2Dingtalk(String data){
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("token","3bukemengyutest5566");
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());

        //封装dingtalk消息体
        DingTalkRobot dingTalkRobot=new DingTalkRobot();
        DingTalkRobot.TextBean textBean=new DingTalkRobot.TextBean();
        textBean.setContent(data);
        dingTalkRobot.setText(textBean);
        dingTalkRobot.setMsgtype("text");
        DingTalkRobot.AtBean atBean=new DingTalkRobot.AtBean();
        List mobileList=new LinkedList();
        mobileList.add("15010757627");
        atBean.setAtMobiles(mobileList);
        dingTalkRobot.setAt(atBean);


        HttpEntity<String> formEntity = new HttpEntity<String>(JSONObject.toJSONString(dingTalkRobot), headers);
        String result = restTemplate.postForObject("https://oapi.dingtalk.com/robot/send?access_token=191ea8f67a756bc1e1d1c7ec03c629f555e14c0984d2b77ffbabbef8b50aa418", formEntity, String.class);
    }

}
