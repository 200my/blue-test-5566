package blue.web.shiro;

import blue.erp.model.SysResource;
import blue.erp.model.SysRole;
import blue.erp.model.SysUser;
import blue.web.commons.UserContext;
import blue.web.dao.service.SysResourceService;
import blue.web.dao.service.SysRoleService;
import blue.web.dao.service.SysUserService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

public class MyShiroRealm extends AuthorizingRealm {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysResourceService sysResourceService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        SysUser userInfo  = (SysUser)principals.getPrimaryPrincipal();

        if(StringUtils.isNotBlank(userInfo.getRoleIds())){
            String[] roleIds=userInfo.getRoleIds().split(",");
            for(String roleId:roleIds){
                SysRole sysRole= (SysRole) sysRoleService.selectByPrimaryKey(Long.decode(roleId),UserContext.getSchoolId());
                authorizationInfo.addRole(sysRole.getName());

                if(StringUtils.isNotBlank(sysRole.getResourceIds())){
                    String[] resourceIds=sysRole.getResourceIds().split(",");
                   for(String resourceId:resourceIds){
                       SysResource sysResource= (SysResource) sysResourceService.selectByPrimaryKey(Long.decode(resourceId),UserContext.getSchoolId());
                       authorizationInfo.addStringPermission(sysResource.getPermission());
                   }
                }
            }
        }

        return authorizationInfo;
    }

    /*主要是用来进行身份认证的，也就是说验证用户输入的账号和密码是否正确。*/
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token)
            throws AuthenticationException {
        //获取用户的输入的账号.
        String username = (String)token.getPrincipal();

        //通过username从数据库中查找 User对象，如果找到，没找到.
        //实际项目中，这里可以根据实际情况做缓存，如果不做，Shiro自己也是有时间间隔机制，2分钟内不会重复执行该方法
        SysUser userInfo = sysUserService.selectByLoginName(username);

        if(userInfo == null){
            return null;
        }
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                userInfo, //用户名
                userInfo.getPassword(), //密码
                ByteSource.Util.bytes(userInfo.getSalt()),//salt=username+salt
                getName()  //realm name
        );
        return authenticationInfo;
    }




}