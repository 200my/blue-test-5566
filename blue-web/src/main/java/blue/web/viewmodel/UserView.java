package blue.web.viewmodel;

public class UserView {


    /**
     * name : Serati Ma
     * avatar : https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png
     * userid : 00000001
     * notifyCount : 12
     */

    private String name;
    private String avatar;
    private String userid;
    private int notifyCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getNotifyCount() {
        return notifyCount;
    }

    public void setNotifyCount(int notifyCount) {
        this.notifyCount = notifyCount;
    }
}
