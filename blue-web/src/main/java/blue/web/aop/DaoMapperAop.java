package blue.web.aop;

import blue.dao.base.BaseModel;
import blue.web.commons.UserContext;
import cn.hutool.core.convert.Convert;
import com.alibaba.fastjson.JSONObject;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class DaoMapperAop {

    private org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @Pointcut("execution(* blue.dao.base..*.*(..)))")
    public void daoMapper() {

    }

    @Before(value = "daoMapper()")
    public void beforeDaoMapper() {
    }

    @After(value = "daoMapper()")
    public void afterDaoMapper() {

    }

    @Around(value="daoMapper()")
    public Object doAroundDaoMapper(ProceedingJoinPoint pjp) throws Throwable {

        //对参数做操作
        Object[] args=pjp.getArgs();

        // result的值就是被拦截方法的返回值
        Object result = pjp.proceed();

        logger.info(String.format("beforeDaoMapper end,param=%s,and userId=%s,result : %s ",
                args==null?"":JSONObject.toJSONString(args),UserContext.getUserId(),result==null?"":JSONObject.toJSONString(result)));

        return result;
    }


}
