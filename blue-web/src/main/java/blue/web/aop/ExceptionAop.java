package blue.web.aop;

import blue.tools.constants.BlueContants;
import blue.tools.model.ResponseData;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionAop {

    private org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(value = Exception.class)
    public ResponseData defaultErrorHandler(HttpServletRequest request, Exception e) {
        logger.error("系统异常", e);

        return new ResponseData(BlueContants.ERROR,"异常",null);
    }


}
