package blue.web.commons;

import blue.erp.model.SysUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;

public class UserContext {

    public static SysUser getContext(){
        Subject subject = SecurityUtils.getSubject();

        if(subject!=null){
            PrincipalCollection principalCollection=subject.getPrincipals();

            if(principalCollection!=null){
                SysUser userInfo  = (SysUser)principalCollection.getPrimaryPrincipal();
                return userInfo;
            }

        }

        return null;
    }

    public static Long getSchoolId(){

        SysUser sysUser=getContext();
        if(sysUser!=null){
            return sysUser.getSchoolid();
        }

        return null;
    }

    public static Long getUserId(){

        SysUser sysUser=getContext();
        if(sysUser!=null){
            return sysUser.getId();
        }

        return null;
    }



}
