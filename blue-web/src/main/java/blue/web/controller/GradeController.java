package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.commons.enums.StatusEnum;
import blue.erp.model.BlueGrade;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.BlueGradeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "grade")
public class GradeController {

    @Autowired
    private BlueGradeService blueGradeService;

    @RequestMapping("saveGrade")
    public ResponseData addGrade(BlueGrade blueGrade){

       blueGrade.setStatus((byte) StatusEnum.Normal.getValue());

       int result=blueGradeService.save(blueGrade);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",null);
       }
    }

    @RequestMapping("deleteGrades")
    public ResponseData deleteGrades(String ids){

        int result=blueGradeService.deleteBatch(ids,UserContext.getSchoolId());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }


    @RequestMapping("queryGrades")
    public PageResponseData queryGrades(Integer currentPage,Integer pageSize){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);

        List<BlueGrade> blueGradeList=blueGradeService.selectBySchoolIdCache(UserContext.getSchoolId(),false);

        if(CollectionUtils.isEmpty(blueGradeList)){
            return new PageResponseData(BlueContants.SUCCESS,"成功",null);
        }
        PageInfo pageInfo = new PageInfo(blueGradeList);

        return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);

    }

    @RequestMapping("queryGradeLevels")
    public PageResponseData queryGradeLevels(Integer currentPage,Integer pageSize){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);

        List<String> gradeLevels=blueGradeService.queryGradeLevels(UserContext.getSchoolId());

        if(CollectionUtils.isEmpty(gradeLevels)){
            return new PageResponseData(BlueContants.SUCCESS,"成功",null);
        }
        PageInfo pageInfo = new PageInfo(gradeLevels);

        return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);

    }

}
