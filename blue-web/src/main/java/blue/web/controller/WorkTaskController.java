package blue.web.controller;

import blue.commons.enums.StatusEnum;
import blue.commons.models.TitleValue;
import blue.tools.constants.BlueContants;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.WorkTaskService;
import blue.workevaluate.model.WorkTask;
import blue.workevaluate.model.WorkTaskResult;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "workTask")
public class WorkTaskController {

    @Autowired
    private WorkTaskService workTaskService;


    @RequestMapping("save")
    public ResponseData save(WorkTask record){

       int result=workTaskService.save(record);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
       }
    }
    //对任务节点进行操作 增加 或者删除 修改
    @RequestMapping("handleNode")
    public ResponseData handleNode(Long taskId,Integer nodeNum,String key,TitleValue newNode,Integer operatorType){

        int result=workTaskService.handleNode(taskId,nodeNum,key,newNode,operatorType,UserContext.getSchoolId());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",result);
        }
    }

    @RequestMapping("delete")
    public ResponseData deleteClasses(String ids){

        int result=workTaskService.deleteBatch(ids,UserContext.getContext());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }


    @RequestMapping("queryById")
    public ResponseData queryById(Long id){

        WorkTask record= workTaskService.selectById(id,UserContext.getSchoolId());

        if(record==null){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            //如果根节点不存在，则根据名称创建一个根节点，有时候只有[]这种空数组的也不行
            if((StringUtils.isBlank(record.getData()) || !record.getData().contains("value"))&& StringUtils.isNotBlank(record.getName())){
                List data=new LinkedList();
                TitleValue titleValue=new TitleValue();
                titleValue.setValue(UUID.randomUUID().toString());
                titleValue.setTitle(record.getName());
                data.add(titleValue);
                record.setData(JSONArray.toJSONString(data));
                workTaskService.save(record);
            }
            return new ResponseData(BlueContants.SUCCESS,"成功",record);
        }
    }

    @RequestMapping("queryNodeByKey")
    public ResponseData queryNodeByKey(Long taskId,String key){

        TitleValue titleValue=workTaskService.queryNodeByKey(taskId,key,UserContext.getSchoolId());
        if(titleValue==null){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }
        return new ResponseData(BlueContants.SUCCESS,"成功",titleValue);
    }

    @RequestMapping("query")
    public PageResponseData query(Integer currentPage, Integer pageSize, WorkTask record){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        List<WorkTask> extendList= workTaskService.selectByObject(record,true,UserContext.getContext());

        if(CollectionUtils.isNotEmpty(extendList)){
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(extendList));
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }

    //教师查询选课结果
    @RequestMapping("queryResult")
    public PageResponseData queryStudentResult(WorkTask record,Long classid){
        List<WorkTaskResult> extendList= workTaskService.queryStudentResult(record.getId(),classid,UserContext.getContext());

        if(CollectionUtils.isNotEmpty(extendList)){
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(extendList));
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }


}
