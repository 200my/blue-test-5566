package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.splitclass.model.CourseTime;
import blue.splitclass.model.SplitClassTime;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.SplitClassTimeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "split_class_time")
public class SplitClassTimeController {

    @Autowired
    private SplitClassTimeService splitClassTimeService;


    @RequestMapping("save")
    public ResponseData saveClass(SplitClassTime record){

       int result=splitClassTimeService.save(record);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
       }
    }


    @RequestMapping("delete")
    public ResponseData deleteClasses(String ids){

        int result=splitClassTimeService.deleteBatch(ids,UserContext.getContext());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }



    @RequestMapping("query")
    public PageResponseData query(Integer currentPage, Integer pageSize, SplitClassTime record){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        List<SplitClassTime> extendList= splitClassTimeService.selectByObject(record);

        if(CollectionUtils.isNotEmpty(extendList)){
            return new PageResponseData(BlueContants.ERROR,"成功",new PageInfo(extendList));
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"失败",null);
        }
    }

    @RequestMapping("queryByTaskId")
    public ResponseData queryByTaskId(SplitClassTime record){

        SplitClassTime splitClassTime= splitClassTimeService.selectByTaskId(record.getTaskId(),record.getSchoolid());

        if(splitClassTime!=null){
            return new ResponseData(BlueContants.SUCCESS,"成功",splitClassTime);
        }else {
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }
    }

    @RequestMapping("queryCompose")
    public PageResponseData queryCompose(Integer currentPage, Integer pageSize,SplitClassTime record){
        pageSize=(pageSize==null?10:pageSize);
        currentPage=(currentPage==null?1:currentPage);
        PageHelper.startPage(currentPage,pageSize);
        List<CourseTime> courseTimeList= splitClassTimeService.queryCompose(record);

        if(courseTimeList!=null){
            PageInfo pageInfo=new PageInfo(courseTimeList);
            pageInfo.setPageSize(pageSize);
            pageInfo.setPageNum(currentPage);
            return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }

}
