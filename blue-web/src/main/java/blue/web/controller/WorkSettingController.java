package blue.web.controller;

import blue.commons.enums.SettingTypeEnum;
import blue.commons.enums.StatusEnum;
import blue.commons.models.TitleValue;
import blue.splitclass.model.ElectiveTask;
import blue.tools.constants.BlueContants;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.ElectiveTaskService;
import blue.web.dao.service.WorkSettingService;
import blue.workevaluate.model.WorkSetting;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "workSetting")
public class WorkSettingController {

    @Autowired
    private WorkSettingService workSettingService;


    @RequestMapping("save")
    public ResponseData saveClass(WorkSetting record,String parentId,String nodeName,Integer operateType,Integer nodeNum){

       int result=workSettingService.save(record,parentId,nodeName,operateType,nodeNum);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
       }
    }

    @RequestMapping("delete")
    public ResponseData deleteClasses(String ids){

        int result=workSettingService.deleteBatch(ids,UserContext.getContext());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }



//    @RequestMapping("query")
//    public Object query(Integer currentPage, Integer pageSize, WorkSetting record){
//        List<WorkSetting> extendList= workSettingService.selectByObject(record,false);
//
//        if(CollectionUtils.isNotEmpty(extendList)){
//            if(SettingTypeEnum.grade.getKey() == extendList.get(0).getType() || SettingTypeEnum.course.getKey() == extendList.get(0).getType()){
//                return new ResponseData(BlueContants.SUCCESS,"成功",extendList.get(0));
//            }else {
//                return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(extendList));
//            }
//        }else {
//            if(SettingTypeEnum.grade.getKey() == record.getType() || SettingTypeEnum.course.getKey() == record.getType()){
//                return new ResponseData(BlueContants.ERROR,"失败",null);
//            }else {
//                return new PageResponseData(BlueContants.SUCCESS,"成功",null);
//            }
//
//        }
//    }

    @RequestMapping("query")
    public ResponseData query(Integer currentPage, Integer pageSize, WorkSetting record){
        List<WorkSetting> extendList= workSettingService.selectByObject(record,false);

        //如果根节点不存在，则根据名称创建一个根节点，有时候只有[]这种空数组的也不行
        if((CollectionUtils.isEmpty (extendList) || StringUtils.isBlank(extendList.get(0).getData()) || !extendList.get(0).getData().contains("value"))
                && StringUtils.isNotBlank(record.getCoursename()) && StringUtils.isNotBlank(record.getGradename()) && record.getType()!=null && record.getType()>2){
            List data=new LinkedList();
            TitleValue titleValue=new TitleValue();
            titleValue.setValue(UUID.randomUUID().toString());
            titleValue.setTitle(String.format("%s%s%s",record.getGradename(),record.getCoursename(),SettingTypeEnum.getByKey(record.getType()).getDesc()));
            data.add(titleValue);
            record.setData(JSONArray.toJSONString(data));
            if(extendList==null){
                extendList=new LinkedList<>();
            }
            extendList.add(record);
            workSettingService.save(record,null,null,null,null);
        }

        if(CollectionUtils.isNotEmpty(extendList)){
            return new ResponseData(BlueContants.SUCCESS,"成功",extendList.get(0));
        }else {
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }
    }

    @RequestMapping("queryByTaskId")
    public ResponseData query(Long taskId){
        HashMap result= workSettingService.selectByTaskId(taskId,UserContext.getSchoolId());

        if(result!=null){
            return new ResponseData(BlueContants.SUCCESS,"成功",result);
        }else {
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }
    }

}
