package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.commons.enums.StatusEnum;
import blue.erp.model.BlueClass;
import blue.erp.model.BlueGrade;
import blue.tools.commons.BlueStringUtils;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.BlueClassService;
import blue.web.dao.service.BlueGradeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(value = "class")
public class ClassController {

    @Autowired
    private BlueClassService blueClassService;

    @Autowired
    private BlueGradeService blueGradeService;

    @RequestMapping("saveClass")
    public ResponseData saveClass(BlueClass blueClass){

        blueClass.setStatus((byte) StatusEnum.Normal.getValue());

       int result=blueClassService.save(blueClass);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",null);
       }
    }

    @RequestMapping("deleteClasses")
    public ResponseData deleteClasses(String ids){

        int result=blueClassService.deleteBatch(ids,UserContext.getSchoolId());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }

    @RequestMapping("queryById")
    public ResponseData queryById(Long id){

        BlueClass blueClass=blueClassService.selectByPrimaryKey(id,UserContext.getSchoolId(),false);

        if(blueClass==null){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",blueClass);
        }
    }

    @RequestMapping("queryBySplitTask")
    public PageResponseData queryBySplitTask(Long taskId){

        List<BlueClass> blueClassList=blueClassService.selectClassBySplitTask(taskId,UserContext.getSchoolId());

        if(CollectionUtils.isEmpty(blueClassList)){
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(blueClassList));
        }
    }


    @RequestMapping("queryClasses")
    public PageResponseData queryClasses(Integer currentPage,Integer pageSize,String name,String gradeIds){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);

        List<Long> ids=new LinkedList<>();
        if(StringUtils.isNotBlank(gradeIds)){
            List gradeList=BlueStringUtils.convertString2Num(gradeIds,",");
            if(CollectionUtils.isNotEmpty(gradeList)){
                ids=gradeList;
            }
        }else {
            List<BlueGrade> blueGradeList=blueGradeService.selectBySchoolIdCache(UserContext.getSchoolId(),false);

            if(CollectionUtils.isEmpty(blueGradeList)){
                return new PageResponseData(BlueContants.SUCCESS,"成功",null);
            }

            for(BlueGrade blueGrade:blueGradeList){
                ids.add(blueGrade.getId());
            }
        }

        BlueClass blueClass=new BlueClass();
        blueClass.setName(name);
        blueClass.setSchoolid(UserContext.getSchoolId());
        List<BlueClass> blueClassList=blueClassService.selectByBlueClass(blueClass,ids,false);

        if(CollectionUtils.isEmpty(blueClassList)){
            return new PageResponseData(BlueContants.SUCCESS,"成功",null);
        }

        PageInfo pageInfo = new PageInfo(blueClassList);

        return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);

    }

}
