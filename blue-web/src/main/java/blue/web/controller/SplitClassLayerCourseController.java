package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.splitclass.model.CourseTime;
import blue.splitclass.model.SplitClassLayerCourse;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.SplitClassLayerCourseService;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "layer_course")
public class SplitClassLayerCourseController {

    @Autowired
    private SplitClassLayerCourseService splitClassLayerCourseService;



    @RequestMapping(value="save", method=RequestMethod.POST)
    public ResponseData saveClass(@RequestBody List<SplitClassLayerCourse> data,Long taskId){
        int result=0;
       if(CollectionUtils.isNotEmpty(data)){
           for(SplitClassLayerCourse splitClassLayerCourse:data){
               splitClassLayerCourse.setSchoolid(UserContext.getSchoolId());
               taskId=splitClassLayerCourse.getTaskId();
               result+=splitClassLayerCourseService.save(splitClassLayerCourse);
           }
       }
       if(result==0){
           return new ResponseData(BlueContants.ERROR,"没有保存数据",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",taskId);
       }
    }

    @RequestMapping("delete")
    public ResponseData deleteClasses(String ids){

        int result=splitClassLayerCourseService.deleteBatch(ids,UserContext.getContext());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }



    @RequestMapping("query")
    public PageResponseData query(Integer currentPage, Integer pageSize, SplitClassLayerCourse record){
        List<SplitClassLayerCourse> layerCourseList= splitClassLayerCourseService.selectByObject(record,true);

        if(CollectionUtils.isNotEmpty(layerCourseList)){
            return new PageResponseData(BlueContants.ERROR,"成功",new PageInfo(layerCourseList));
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"失败",null);
        }
    }

    /**
     * 获取当前任务的待选课程
     * @param record
     * @return
     */
    @RequestMapping("queryOptions")
    public PageResponseData queryOptions(SplitClassLayerCourse record){
        List<SplitClassLayerCourse> layerCourseList= splitClassLayerCourseService.selectLayerOptions(record.getTaskId(),record.getSchoolid());

        if(CollectionUtils.isNotEmpty(layerCourseList)){
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(layerCourseList));
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }

    @RequestMapping("getCourseLayerTemplet")
    public ResponseData getCourseLayerTemplet(SplitClassLayerCourse record){
        String url= splitClassLayerCourseService.getCourseLayerTemplet(record);

        if(StringUtils.isNotBlank(url)){
            return new ResponseData(BlueContants.SUCCESS,"成功",url);
        }else {
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }
    }

    @RequestMapping("getComposeByTask")
    public PageResponseData getComposeByTask(SplitClassLayerCourse record){
        List<CourseTime> layerCourseList= splitClassLayerCourseService.getComposeByTask(record.getTaskId(),record.getSchoolid());

        if(CollectionUtils.isNotEmpty(layerCourseList)){
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(layerCourseList));
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }
    //获取每个班级的课程组合和选择人数
    @RequestMapping("getClassCompose")
    public ResponseData getClassCompose(SplitClassLayerCourse record){
        HashMap layerCourseList= splitClassLayerCourseService.getClassCompose(record.getTaskId(),record.getSchoolid());

        if(layerCourseList!=null){
            return new ResponseData(BlueContants.SUCCESS,"成功",JSONObject.toJSONString(layerCourseList,SerializerFeature.DisableCircularReferenceDetect));
        }else {
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }
    }



}
