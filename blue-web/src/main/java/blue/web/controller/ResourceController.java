package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.dao.service.SysResourceService;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "resource")
public class ResourceController {

    @Autowired
    private SysResourceService sysResourceService;


    @RequestMapping("save")
    public ResponseData save(String data){

        if(StringUtils.isBlank(data)){
            return new ResponseData(BlueContants.ERROR,"数据为空",null);
        }

        JSONArray jsonArray=JSONArray.parseArray(data);

        if(jsonArray!=null && jsonArray.size()>0){
            sysResourceService.putJson2Db(null,"0",jsonArray);
        }

        return new ResponseData(BlueContants.SUCCESS,"成功",null);
    }




//    @RequestMapping("deleteClasses")
//    public ResponseData delete(String ids){
//
//        int result=blueClassService.deleteBatch(ids);
//
//        if(result==0){
//            return new ResponseData(BlueContants.ERROR,"失败",null);
//        }else {
//            return new ResponseData(BlueContants.SUCCESS,"成功",null);
//        }
//    }
//
//
    @RequestMapping("queryAllResources")
    public PageResponseData query(){

        PageInfo pageInfo = new PageInfo(sysResourceService.getDb2Json(null));

        return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);

    }



}
