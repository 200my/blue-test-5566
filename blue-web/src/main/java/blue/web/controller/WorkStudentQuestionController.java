package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.WorkStudentQuestionService;
import blue.workevaluate.model.WorkTaskStudentQuestion;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "work_student_question")
public class WorkStudentQuestionController {

    @Autowired
    private WorkStudentQuestionService workStudentQuestionService;


    @RequestMapping("save")
    public ResponseData save(WorkTaskStudentQuestion record){

       int result=workStudentQuestionService.save(record);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
       }
    }

    @RequestMapping("batchSave")
    public ResponseData batchSave(WorkTaskStudentQuestion record){

        int result=workStudentQuestionService.batchSave(record);

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
        }
    }

    @RequestMapping("delete")
    public ResponseData deleteClasses(String ids){

        int result=workStudentQuestionService.deleteBatch(ids,UserContext.getContext());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }


    @RequestMapping("queryById")
    public ResponseData queryById(Long id){

        WorkTaskStudentQuestion record= workStudentQuestionService.selectById(id,UserContext.getSchoolId());

        if(record==null){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",record);
        }
    }

    @RequestMapping("query")
    public PageResponseData query(Integer currentPage, Integer pageSize, WorkTaskStudentQuestion record){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        record.setUserId(UserContext.getUserId());
        List<WorkTaskStudentQuestion> extendList= workStudentQuestionService.selectByObject(record,true,true);

        if(CollectionUtils.isNotEmpty(extendList)){
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(extendList));
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }

    @RequestMapping("queryDistinctInfo")
    public PageResponseData queryDistinctInfo(WorkTaskStudentQuestion record){
        record.setUserId(UserContext.getUserId());
        List<String> extendList= workStudentQuestionService.selectDistinct(record,true);

        if(CollectionUtils.isNotEmpty(extendList)){
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(extendList));
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }
}
