package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.commons.enums.StatusEnum;
import blue.splitclass.model.ElectiveRule;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.ElectiveRuleService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "elective_rule")
public class ElectiveRuleController {

    @Autowired
    private ElectiveRuleService electiveRuleService;


    @RequestMapping("save")
    public ResponseData save(ElectiveRule record){

        record.setStatus((byte) StatusEnum.Normal.getValue());

       int result=electiveRuleService.save(record);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
       }
    }

    @RequestMapping("delete")
    public ResponseData deleteClasses(String ids){

        int result=electiveRuleService.deleteBatch(ids,UserContext.getContext());
        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }



    @RequestMapping("queryById")
    public ResponseData queryById(Long id){

        ElectiveRule electiveRule=new ElectiveRule();
        electiveRule.setId(id);
        electiveRule.setSchoolid(UserContext.getSchoolId());

        List<ElectiveRule> ruleList= electiveRuleService.selectByObject(electiveRule,false);

        if(CollectionUtils.isEmpty(ruleList)){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",ruleList.get(0));
        }
    }

    @RequestMapping("query")
    public PageResponseData queryById(ElectiveRule record){

        List<ElectiveRule> extendList= electiveRuleService.selectByObject(record,true);

        if(CollectionUtils.isNotEmpty(extendList)){
            PageInfo pageInfo = new PageInfo(extendList);
            return new PageResponseData(BlueContants.ERROR,"成功",pageInfo);
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"失败",null);
        }
    }


}
