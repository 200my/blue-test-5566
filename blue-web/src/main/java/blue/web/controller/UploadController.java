package blue.web.controller;

import blue.dao.uploadexcel.ExcelVirtualGroup;
import blue.tools.constants.BlueContants;
import blue.dao.uploadexcel.ExcelAccountStudent;
import blue.dao.uploadexcel.ExcelAccountTeacher;
import blue.splitclass.model.SplitClassLayerStudent;
import blue.tools.model.ResponseData;
import blue.web.cache.service.RedisService;
import blue.web.commons.UserContext;
import blue.web.commons.service.OSSService;
import blue.web.dao.service.SplitClassLayerStudentService;
import blue.web.dao.service.SysUserService;
import cn.hutool.poi.excel.ExcelReader;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("upload")
public class UploadController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SplitClassLayerStudentService splitClassLayerStudentService;

    @Autowired
    private OSSService ossService;

    @Autowired
    private RedisService redisService;


    @RequestMapping(value="/excel", method=RequestMethod.POST)
    public ResponseData uploadExcel(@RequestParam("file") MultipartFile file, HttpServletRequest request,String extend) {

        try {
            if(file==null || file.getInputStream()==null){
                return new ResponseData(BlueContants.ERROR, "文件有问题", null);
            }

            return handleExcel(file,extend);
        } catch (Exception e) {
            logger.error("uploadExcel",e);
        }

        return new ResponseData(BlueContants.ERROR, "处理失败", null);
    }

    @RequestMapping(value="/file", method=RequestMethod.POST)
    public ResponseData uploadFile(@RequestParam("file") MultipartFile file, HttpServletRequest request,String extend,String type) {

        try {
            if(file==null || file.getInputStream()==null){
                return new ResponseData(BlueContants.ERROR, "文件有问题", null);
            }

            return handleFile(file,extend,type);
        } catch (Exception e) {
            logger.error("uploadExcel",e);
        }

        return new ResponseData(BlueContants.ERROR, "处理失败", null);
    }


    private ResponseData handleFile(MultipartFile file,String extend,String type) {

        String result= StringUtils.EMPTY;
        try {
            switch (type){
                case "teacherWorkPublish":
                case "uploadOss":
                    String filename=String.format("%s/%s|%s",BlueContants.oss_path_workevaluate,UUID.randomUUID().toString(),file.getOriginalFilename());
                    boolean ossResult=ossService.putObject(filename, file.getInputStream(),BlueContants.oss_bucket_rundata,ossService.getOSSClient(BlueContants.oss_region_beijing));

                    if(ossResult){
                        result=String.format("%s/%s",BlueContants.oss_url_rundata,filename);
                    }
                    break;

            }

        } catch (Exception e) {
            logger.error("handleExcel",e);
        }

        if(StringUtils.isBlank(result)){
            return new ResponseData(BlueContants.ERROR, "失败", null);
        }

        return new ResponseData(BlueContants.SUCCESS, "成功", result);
    }

    private ResponseData handleExcel(MultipartFile file,String extend) {

        int result=0;
        try {
            ExcelReader excelReader = new ExcelReader(file.getInputStream(), 0, true);
            String type=excelReader.getSheetNames().get(0);
            switch (type){
                case BlueContants.import_excel_student:
                    ExcelAccountStudent excelAccountStudent=new ExcelAccountStudent();
                    List<ExcelAccountStudent> excelAccountStudentList=excelAccountStudent.convertExcel2Object(excelReader,ExcelAccountStudent.class);
                    result=sysUserService.createStudentAccountByExcel(excelAccountStudentList,UserContext.getSchoolId());
                    break;
                case BlueContants.import_excel_teacher:
                    ExcelAccountTeacher excelAccountTeacher=new ExcelAccountTeacher();
                    List<ExcelAccountTeacher> excelAccountTeacherList=excelAccountTeacher.convertExcel2Object(excelReader,ExcelAccountTeacher.class);
                    result=sysUserService.createTeacherAccountByExcel(excelAccountTeacherList,UserContext.getSchoolId());
                    break;

                case BlueContants.import_excel_courselayer:
                    JSONObject jsonObject=JSONObject.parseObject(extend);
                    SplitClassLayerStudent splitClassLayerStudent=new SplitClassLayerStudent();
                    List<SplitClassLayerStudent> splitClassLayerStudentList=splitClassLayerStudent.convertExcel2Object(excelReader);
                    result=splitClassLayerStudentService.saveByExcel(splitClassLayerStudentList,jsonObject.getLong("taskId"),UserContext.getSchoolId());
                    break;
                case BlueContants.import_excel_virtualGroup:
                    ExcelVirtualGroup excelVirtualGroup=new ExcelVirtualGroup();
                    List<ExcelVirtualGroup> excelVirtualGroupList=excelVirtualGroup.convertExcel2Object(excelReader,ExcelVirtualGroup.class);
                    result=sysUserService.createVirtualGroupByExcel(excelVirtualGroupList,UserContext.getContext());
                    break;
            }

        } catch (Exception e) {
            logger.error("handleExcel",e);
        }

        return new ResponseData(BlueContants.SUCCESS, "成功个数"+result, null);
    }

}
