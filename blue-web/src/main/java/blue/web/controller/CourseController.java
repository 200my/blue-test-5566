package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.commons.enums.GradeEnum;
import blue.commons.models.KeyValue;
import blue.erp.model.BlueCourse;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.BlueCourseService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(value = "course")
public class CourseController {

    @Autowired
    private BlueCourseService blueCourseService;


    @RequestMapping("save")
    public ResponseData save(BlueCourse blueCourse){

        //用户名只允许在创建的时候修改
        if(StringUtils.isBlank(blueCourse.getName())){
            return new ResponseData(BlueContants.SUCCESS,"参数缺失",null);
        }

        return new ResponseData(BlueContants.SUCCESS,"成功",blueCourseService.save(blueCourse));
    }

    @RequestMapping("query")
    public PageResponseData query(Integer currentPage,Integer pageSize,BlueCourse blueCourse){

        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        List<BlueCourse> blueCourseList=blueCourseService.select(blueCourse,false);

        PageInfo pageInfo=null;

        if(CollectionUtils.isNotEmpty(blueCourseList)){
            pageInfo = new PageInfo(blueCourseList);
        }

        return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);
    }

    @RequestMapping("queryAll")
    public PageResponseData queryAll(Integer currentPage,Integer pageSize,BlueCourse blueCourse){
        currentPage=(currentPage==null?1:currentPage);
        pageSize=(pageSize==null?10:pageSize);
        PageHelper.startPage(currentPage, pageSize);
        List blueCourseList=new LinkedList();
        PageInfo pageInfo=new PageInfo();
        if(pageSize!=null || StringUtils.isNotBlank(blueCourse.getName()) || StringUtils.isNotBlank(blueCourse.getType())){
            blueCourseList=blueCourseService.select(blueCourse,false);
        }else {
            pageInfo.setPageSize(pageSize);
            pageInfo.setPageNum(currentPage);
            blueCourseList=blueCourseService.selectBySchoolIdCache(blueCourse.getSchoolid(),false);
        }

        if(CollectionUtils.isNotEmpty(blueCourseList)){
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(blueCourseList));
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"成功",null);
        }

    }


    @RequestMapping("delete")
    public ResponseData delete(String ids){

        int result=blueCourseService.deleteBatch(ids,UserContext.getSchoolId());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }

    @RequestMapping("initCourse")
    public PageResponseData initCourse(int type,String token){

        if(!"abc_test_master_111092".equalsIgnoreCase(token)){
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }

        List<BlueCourse> courseList=blueCourseService.initCourse(type,UserContext.getSchoolId());

        if(CollectionUtils.isEmpty(courseList)){
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(courseList));
        }
    }
   //根据年级类型获取年级列表，比如 高中 返回 高一 高二 高三
    @RequestMapping("getGradeListByType")
    public PageResponseData getGradeListByType(Integer type,String typeName){
        List<KeyValue> courseList=null;
        if(type!=null && type>0){
            courseList= GradeEnum.getGradeDetailsByKey(type);
        }else if(StringUtils.isNotBlank(typeName)){
            courseList= GradeEnum.getGradeDetailsByValue(typeName);
        }

        if(CollectionUtils.isEmpty(courseList)){
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(courseList));
        }
    }
    //获取当前学校的年级类型列表，比如 高一 高二 。。
    @RequestMapping("getUniqueGradeList")
    public PageResponseData getUniqueGradeList(){

        List result=blueCourseService.getUniqueGradeList(UserContext.getSchoolId());

        if(CollectionUtils.isEmpty(result)){
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(new LinkedList(result)));
        }
    }

    //获取当前学校的所有课程，比如 语文 数学 。。
    @RequestMapping("getUniqeCourseList")
    public PageResponseData getUniqeCourseList(){
        List result=blueCourseService.getUniqeCourseList(UserContext.getSchoolId());

        if(CollectionUtils.isEmpty(result)){
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(new LinkedList(result)));
        }
    }

}
