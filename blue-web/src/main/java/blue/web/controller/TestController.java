package blue.web.controller;

import blue.erp.dao.SysUserMapper;
import blue.web.cache.service.RedisService;
import blue.web.commons.service.ExcelService;
import blue.web.commons.service.SendMail;
import blue.web.exam.service.TikuGetDataService;
import blue.web.monitor.DingTalkService;
import blue.workevaluate.dao.WorkSettingMapper;
import blue.workevaluate.model.WorkSetting;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(value = "test")
public class TestController {

    private Logger logger = LoggerFactory.getLogger(TestController.class);


    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private RedisService redisService;

    @Autowired
    private SendMail sendMail;

    @Autowired
    ExcelService excelUtils;

    @Autowired
    private WorkSettingMapper workSettingMapper;

    @Autowired
    private DingTalkService dingTalkService;

    @Autowired
    private TikuGetDataService tikuGetDataService;

    @RequestMapping("yunxiao")
    public void yunxiao(String name){

        tikuGetDataService.getFromYunxiao(name);
    }

    @RequestMapping("dingtalk")
    public void dingtalk(String data){

        dingTalkService.sendMsg2Dingtalk(data);
    }

    @RequestMapping("index")
    public String index(){

        return "index";
    }

//    @RequestMapping("oss")
//    public String writeOss(String fileName){
//        List data=new LinkedList();
//        data.add("111111");
////        int result=excelUtils.writeData2Excel(data,null,fileName,"blue-erp-ui");
//        return "ok"+result;
//    }

    @RequestMapping("sendmail")
    public String sendMail(String email){
        sendMail.send(email);
        return "ok";
    }

    @RequestMapping("403")
    public String error(){

        return "403";
    }

    @RequestMapping("shiroPermissionRole")
    @RequiresPermissions("role:create")
    public String shiroPermissionRole(){

        return "shiroPermissionRole";
    }

    @RequestMapping("shiroPermissionUser")
    @RequiresPermissions("user:create")
    public String shiroPermissionUser(){

        return "shiroPermissionUser";
    }



    @RequestMapping("erpdb")
    public String erpdb(){

        return sysUserMapper.selectByPrimaryKey(1l).getUsername();
    }

    @RequestMapping("wedb")
    public int wedb(){
        WorkSetting workSetting=new WorkSetting();
        workSetting.setCourseid((long) 1);
        workSetting.setType(1);
        return workSettingMapper.insert(workSetting);
    }

    @RequestMapping("redisput")
    public String redisput(String key,String value){

         redisService.setValue(key,value);
         return "ok";
    }

    @RequestMapping("redisget")
    public String redisget(String key){

        return (String) redisService.getValue(key);
    }



    public static void main(String[] args) {

//        CryptographyUtil.md5(password, "java")
        System.out.println("8d78869f470951332959580424d4bf4f".length());
        SimpleHash simpleHash=new SimpleHash("md5","123456", ByteSource.Util.bytes("8d78869f470951332959580424d4bf4f"),2);
        System.out.println(simpleHash);
    }
}
