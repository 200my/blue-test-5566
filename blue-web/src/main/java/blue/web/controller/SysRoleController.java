package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.erp.model.SysRole;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.dao.service.SysRoleService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "role")
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;


    @RequestMapping("save")
    public ResponseData save(SysRole sysRole){

        return new ResponseData(BlueContants.SUCCESS,"成功",sysRoleService.save(sysRole));
    }


    @RequestMapping("query")
    public ResponseData queryByName(SysRole sysRole){


        return new ResponseData(BlueContants.SUCCESS,"成功",sysRoleService.getRoles(sysRole,false));

    }

    @RequestMapping("delete")
    public ResponseData delete(SysRole sysRole){

        return new ResponseData(BlueContants.SUCCESS,"成功",sysRoleService.deleteByPrimaryKey(sysRole.getId(),sysRole.getSchoolid()));

    }


    @RequestMapping("queryAllRoles")
    public PageResponseData queryAllRoles(SysRole sysRole){

        List<SysRole> sysRoleList=sysRoleService.getRoles(sysRole,false);

        PageInfo pageInfo=null;

        if(CollectionUtils.isNotEmpty(sysRoleList)){
             pageInfo = new PageInfo(sysRoleList);
        }

        return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);

    }



}
