package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.commons.enums.StatusEnum;
import blue.commons.enums.UserTypeEnum;
import blue.splitclass.model.ElectiveTask;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.ElectiveTaskService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "elective_task")
public class ElectiveTaskController {

    @Autowired
    private ElectiveTaskService electiveTaskService;


    @RequestMapping("save")
    public ResponseData saveClass(ElectiveTask record){

        record.setStatus((byte) StatusEnum.Normal.getValue());

       int result=electiveTaskService.save(record);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
       }
    }

    @RequestMapping("delete")
    public ResponseData deleteClasses(String ids){

        int result=electiveTaskService.deleteBatch(ids,UserContext.getContext());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }

    @RequestMapping("deleteSelectedCourse")
    public ResponseData deleteSelectedCourse(Long courseId,Long taskId){

        int result=electiveTaskService.deleteSelectedCourse(courseId,taskId,UserContext.getSchoolId());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",taskId);
        }
    }

    @RequestMapping("queryById")
    public ResponseData queryById(Long id){

        ElectiveTask record= electiveTaskService.selectById(id,UserContext.getSchoolId());

        if(record==null){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",record);
        }
    }

    @RequestMapping("query")
    public PageResponseData query(Integer currentPage, Integer pageSize, ElectiveTask record){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        List<ElectiveTask> extendList= electiveTaskService.selectByObject(record);

        if(CollectionUtils.isNotEmpty(extendList)){
            return new PageResponseData(BlueContants.ERROR,"成功",new PageInfo(extendList));
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"失败",null);
        }
    }

    @RequestMapping("queryStudent")
    public PageResponseData queryStudent(Integer currentPage, Integer pageSize, ElectiveTask record){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        record.setGradeid(Long.decode(UserContext.getContext().getGradeIds()));
        List<ElectiveTask> extendList= electiveTaskService.selectByObject(record);

        if(CollectionUtils.isNotEmpty(extendList)){
            return new PageResponseData(BlueContants.ERROR,"成功",new PageInfo(extendList));
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"失败",null);
        }
    }

    @RequestMapping("test")
    public int test(Long taskId){

        return electiveTaskService.createSelectedDbByTask(taskId,UserContext.getSchoolId(), (byte) UserTypeEnum.Student.getKey());

    }

}
