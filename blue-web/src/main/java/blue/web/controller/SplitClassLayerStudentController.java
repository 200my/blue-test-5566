package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.splitclass.model.SplitClassLayerStudent;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.SplitClassLayerStudentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "split_class_layer_student")
public class SplitClassLayerStudentController {

    @Autowired
    private SplitClassLayerStudentService splitClassLayerStudentService;


    @RequestMapping("delete")
    public ResponseData deleteClasses(SplitClassLayerStudent record){

        int result=splitClassLayerStudentService.deleteByTask(record);

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }



    @RequestMapping("query")
    public PageResponseData query(Integer currentPage, Integer pageSize, SplitClassLayerStudent record,String subjectName,String layerName){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        List<SplitClassLayerStudent> extendList= splitClassLayerStudentService.selectBySearchKey(record,subjectName,layerName);

        if(CollectionUtils.isNotEmpty(extendList)){
            return new PageResponseData(BlueContants.ERROR,"成功",new PageInfo(extendList));
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"失败",null);
        }
    }

//    @RequestMapping("exportData")
//    public ResponseData exportData(SplitClassLayerStudent record){
//
//        if(CollectionUtils.isNotEmpty(extendList)){
//            return new PageResponseData(BlueContants.ERROR,"成功",new PageInfo(extendList));
//        }else {
//            return new PageResponseData(BlueContants.SUCCESS,"失败",null);
//        }
//    }

    @RequestMapping("deleteByTask")
    public ResponseData deleteByTask(SplitClassLayerStudent record){

        int result=splitClassLayerStudentService.deleteByTask(record);
        return new ResponseData(BlueContants.SUCCESS,"失败",result);
    }

    //当前任务的所有班级，课程，分层
    @RequestMapping("queryGroup")
    public ResponseData queryGroup(SplitClassLayerStudent record){

        Map result = splitClassLayerStudentService.getGroupInfo(record.getTaskId(),UserContext.getSchoolId());

        return new ResponseData(BlueContants.SUCCESS,"成功",result);
    }

}
