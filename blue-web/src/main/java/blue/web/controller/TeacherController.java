package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.erp.model.BlueTeacher;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.BlueTeacherService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "teacher")
public class TeacherController {

    @Autowired
    private BlueTeacherService blueTeacherService;


    @RequestMapping("save")
    public ResponseData save(BlueTeacher blueTeacher){

        return new ResponseData(BlueContants.SUCCESS,"成功",blueTeacherService.save(blueTeacher));
    }


    @RequestMapping("query")
    public PageResponseData query(Integer currentPage,Integer pageSize,Long gradeid,Long classid,Long courseId,String realName){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        BlueTeacher blueTeacher=new BlueTeacher();
        if(gradeid!=null && gradeid>0){
            blueTeacher.setGradeids(gradeid.toString());
        }

        if(classid!=null && classid>0){
            blueTeacher.setClassids(classid.toString());
        }

        if(courseId!=null && courseId>0){
            blueTeacher.setCourseids(courseId.toString());
        }

        if(StringUtils.isNotBlank(realName)){
            blueTeacher.setRealName(realName);
        }
        blueTeacher.setSchoolid(UserContext.getSchoolId());

        List<BlueTeacher> blueTeacherList=blueTeacherService.select(blueTeacher);

        PageInfo pageInfo=null;

        if(CollectionUtils.isNotEmpty(blueTeacherList)){
            pageInfo = new PageInfo(blueTeacherList);
        }

        return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);

    }


    @RequestMapping("queryCurrent")
    public ResponseData queryCurrent(){
        BlueTeacher blueTeacher=new BlueTeacher();
        blueTeacher.setUserid(UserContext.getUserId());
        blueTeacher.setSchoolid(UserContext.getSchoolId());

        List<BlueTeacher> blueTeacherList=blueTeacherService.select(blueTeacher);

        PageInfo pageInfo=null;

        if(CollectionUtils.isNotEmpty(blueTeacherList)){
            blueTeacher = blueTeacherList.get(0);
        }

        return new ResponseData(BlueContants.SUCCESS,"成功",blueTeacher);

    }

    @RequestMapping("delete")
    public ResponseData delete(String ids){

        int result=blueTeacherService.deleteBatch(ids,UserContext.getSchoolId());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }

}
