package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.commons.enums.StatusEnum;
import blue.splitclass.model.ElectiveCourse;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.ElectiveCourseService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "elective_course")
public class ElectiveCourseController {

    @Autowired
    private ElectiveCourseService electiveCourseService;


    @RequestMapping("save")
    public ResponseData saveClass(ElectiveCourse record, String selectedList, Long taskId){

        record.setStatus((byte) StatusEnum.Normal.getValue());

       int result=electiveCourseService.save(record);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",null);
       }
    }

    @RequestMapping("delete")
    public ResponseData deleteClasses(String ids){

        int result=electiveCourseService.deleteBatch(ids,UserContext.getSchoolId());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }

    @RequestMapping("queryById")
    public ResponseData queryById(Long id){

        ElectiveCourse record=electiveCourseService.selectByPrimaryKey(id,UserContext.getSchoolId(),false);

        if(record==null){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",record);
        }
    }

    @RequestMapping("queryByTaskId")
    public PageResponseData queryByTaskId(Long taskId){

        List<ElectiveCourse> courseList=electiveCourseService.selectByTaskId(taskId,UserContext.getSchoolId());

        if(CollectionUtils.isEmpty(courseList)){
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }else {
            PageInfo pageInfo = new PageInfo(courseList);
            return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);
        }
    }

}
