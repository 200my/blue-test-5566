package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.commons.enums.StatusEnum;
import blue.commons.enums.TaskTypeEnum;
import blue.splitclass.model.ElectivePublishCourse;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.ElectivePublishCourseService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "elective_publish_course")
public class ElectivePublishCourseController {

    @Autowired
    private ElectivePublishCourseService electivePublishCourseService;


    @RequestMapping("save")
    public ResponseData saveClass(ElectivePublishCourse record){

        record.setStatus((byte) StatusEnum.Normal.getValue());

       int result=electivePublishCourseService.save(record);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
       }
    }

    @RequestMapping("delete")
    public ResponseData deleteClasses(String ids){

        int result=electivePublishCourseService.deleteBatch(ids);

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }
    //为了实现预览，type由前端传递
    @RequestMapping("selectByTaskId")
    public PageResponseData selectByTaskId(Integer currentPage, Integer pageSize, Long taskId,Short type){

        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);

        List<ElectivePublishCourse> extendList=electivePublishCourseService.selectByTaskId(taskId,type,UserContext.getSchoolId());

        if(CollectionUtils.isNotEmpty(extendList)){
            PageInfo pageInfo = new PageInfo(extendList);
            return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }

    @RequestMapping("createByRule")
    public ResponseData createByRule(Long taskId){

       electivePublishCourseService.createByRule(taskId,UserContext.getSchoolId(),TaskTypeEnum.Compose.getKey());
       return new ResponseData(BlueContants.ERROR,"成功",null);
    }

    @RequestMapping("queryById")
    public ResponseData queryById(Long id){

        ElectivePublishCourse record= (ElectivePublishCourse) electivePublishCourseService.selectByPrimaryKey(id,UserContext.getSchoolId());

        if(record==null){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",record);
        }
    }


}
