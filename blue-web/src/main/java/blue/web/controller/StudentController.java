package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.erp.model.BlueStudent;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.BlueStudentService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "student")
public class StudentController {

    @Autowired
    private BlueStudentService blueStudentService;


    @RequestMapping("save")
    public ResponseData save(BlueStudent blueStudent){


        return new ResponseData(BlueContants.SUCCESS,"成功",blueStudentService.save(blueStudent));
    }


    @RequestMapping("query")
    public PageResponseData query(List classIds){

        List<BlueStudent> blueStudentList=blueStudentService.selectByClassList(classIds, UserContext.getSchoolId());

        PageInfo pageInfo=null;

        if(CollectionUtils.isNotEmpty(blueStudentList)){
            pageInfo = new PageInfo(blueStudentList);
        }


        return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);

    }

}
