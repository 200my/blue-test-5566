package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.commons.enums.SplitGroupEnum;
import blue.splitclass.model.CourseTime;
import blue.splitclass.model.SplitClassGroup;
import blue.tools.commons.BlueStringUtils;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.SplitClassGroupService;
import blue.web.dao.service.SplitClassLayerCourseService;
import blue.web.dao.service.SplitClassTimeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(value = "split_class_group")
public class SplitClassGroupController {

    @Autowired
    private SplitClassGroupService splitClassGroupService;

    @Autowired
    private SplitClassLayerCourseService splitClassLayerCourseService;

    @Autowired
    private SplitClassTimeService splitClassTimeService;

    @RequestMapping("save")
    public ResponseData save(SplitClassGroup record){

       int result=splitClassGroupService.save(record);
       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
       }
    }

    @RequestMapping("delete")
    public ResponseData delete(String ids){

        int result=splitClassGroupService.deleteBatch(ids,UserContext.getContext());
        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }


    //分组数据，包括课程组和班级组列表
    @RequestMapping("query")
    public PageResponseData query(Integer currentPage, Integer pageSize, SplitClassGroup record){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        List<SplitClassGroup> extendList= splitClassGroupService.selectByObject(record);

        if(CollectionUtils.isNotEmpty(extendList)){

            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(extendList));
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }

    //分组对应的组合列表，包括选课和课时
    @RequestMapping("queryCompose")
    public PageResponseData queryCompose(Integer currentPage, Integer pageSize, SplitClassGroup record){

        if(currentPage==null){
            currentPage=1;
        }
        if(pageSize==null){
            pageSize=10;
        }

        PageHelper.startPage(currentPage,pageSize);
        List<CourseTime> result=new LinkedList<>();
        List<CourseTime> taskCompose=splitClassTimeService.queryCompose(record.getTaskId(),record.getSchoolid());
        if(record.getId()==null){//默认的全部数据
            result=taskCompose;
        }else {//某一个组合下的数据
            Object splitClassGroup= splitClassGroupService.selectByPrimaryKey(record.getId(),record.getSchoolid());
            if(splitClassGroup!=null && StringUtils.isNotBlank(((SplitClassGroup)splitClassGroup).getGroupIds())){
                 if(((SplitClassGroup)splitClassGroup).getType()!=null && SplitGroupEnum.ClassGroup.getKey()==((SplitClassGroup)splitClassGroup).getType()){//班级组

                     HashMap<Long, List<CourseTime>> layer2Course= splitClassLayerCourseService.getClassCompose(record.getTaskId(),record.getSchoolid());
                     HashMap<String,CourseTime> name2CourseTime=new HashMap();
                     //计算给定班级列表的课程选课人数
                     for(Long classId: BlueStringUtils.convertString2Num(((SplitClassGroup)splitClassGroup).getGroupIds(),",")){
                         if(CollectionUtils.isNotEmpty(layer2Course.get(classId))){
                            for(CourseTime courseTime:layer2Course.get(classId)){
                                if(CollectionUtils.isEmpty(courseTime.getSelectedCourseInfo())){
                                    continue;
                                }
                                for(CourseTime subItem:courseTime.getSelectedCourseInfo()){
                                    CourseTime mapedCourseTime=name2CourseTime.get(subItem.getCourseName());
                                    if(mapedCourseTime==null){
                                        mapedCourseTime=new CourseTime(subItem.getCourseName());
                                        mapedCourseTime.setTime(subItem.getTime());
                                        mapedCourseTime.setSelectedCount(0L);
                                    }

                                    if(subItem.getSelectedCount()!=null){
                                        mapedCourseTime.setSelectedCount(mapedCourseTime.getSelectedCount()+subItem.getSelectedCount());
                                    }
                                    name2CourseTime.put(subItem.getCourseName(),mapedCourseTime);
                                }
                            }
                         }
                     }

                     //根据全部组合和计算的每科的课时/选课人数，重新计算组合数据
                     for(CourseTime courseTime:taskCompose){
                         Integer time=0;
                         Long selectedCount=0L;
                         for(String courseName:StringUtils.split(courseTime.getCourseName(),",")){
                             if(name2CourseTime.get(courseName)!=null){
                                 time+=(name2CourseTime.get(courseName).getTime()==null?0:name2CourseTime.get(courseName).getTime());
                                 selectedCount+=(name2CourseTime.get(courseName).getSelectedCount()==null?0:name2CourseTime.get(courseName).getSelectedCount());
                             }
                         }
                         if(selectedCount==0){
                             continue;
                         }
                         courseTime.setSelectedCount(selectedCount);
                         courseTime.setTime(time);
                         result.add(courseTime);
                     }

                 }else {//课程组

                     List subjectList= Arrays.asList(((SplitClassGroup) splitClassGroup).getGroupIds().split(","));
                     for(CourseTime courseTime:taskCompose){
                         if(subjectList.containsAll(Arrays.asList(courseTime.getCourseName().split(",")))){
                             result.add(courseTime);
                         }
                     }
                 }
            }

        }



        if(CollectionUtils.isNotEmpty(result)){
            PageInfo pageInfo=new PageInfo(result);
            pageInfo.setPageSize(pageSize);
            pageInfo.setPageNum(currentPage);
            return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }

}
