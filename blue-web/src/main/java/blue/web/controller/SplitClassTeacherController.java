package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.splitclass.model.SplitClassTeacher;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.SplitClassTeacherService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "split_class_teacher")
public class SplitClassTeacherController {

    @Autowired
    private SplitClassTeacherService splitClassTeacherService;

    @RequestMapping("save")
    public ResponseData save(SplitClassTeacher record){

        int result=splitClassTeacherService.save(record);
        if(result==0){
            return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
        }
    }

    @RequestMapping("delete")
    public ResponseData delete(String ids){

        int result=splitClassTeacherService.deleteBatch(ids,UserContext.getContext());
        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }


    @RequestMapping("query")
    public PageResponseData query(Integer currentPage, Integer pageSize, SplitClassTeacher record){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        List<SplitClassTeacher> extendList= splitClassTeacherService.selectByObject(record,true);

        if(CollectionUtils.isNotEmpty(extendList)){

            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(extendList));
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }


}
