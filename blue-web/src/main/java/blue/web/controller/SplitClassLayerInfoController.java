package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.splitclass.model.SplitClassLayerInfo;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.SplitClassLayerInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(value = "layer_info")
public class SplitClassLayerInfoController {

    @Autowired
    private SplitClassLayerInfoService splitClassLayerInfoService;


    @RequestMapping("save")
    public ResponseData saveClass(SplitClassLayerInfo record){

       int result=splitClassLayerInfoService.save(record);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
       }
    }

    @RequestMapping("delete")
    public ResponseData deleteClasses(String ids){

        int result=splitClassLayerInfoService.deleteBatch(ids,UserContext.getContext());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }


    //获取当前任务的层次信息
    @RequestMapping("query")
    public PageResponseData query(Integer currentPage, Integer pageSize, SplitClassLayerInfo record){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        List<SplitClassLayerInfo> extendList= splitClassLayerInfoService.selectByObject(record);

        if(CollectionUtils.isNotEmpty(extendList) && StringUtils.isNotBlank(extendList.get(0).getNames())){

            List result=new LinkedList();
            int index=0;
            for(String name:extendList.get(0).getNames().split(",")){
                HashMap data=new HashMap();
                data.put("name",name);
                data.put("id",index++);
                result.add(data);
            }

            return new PageResponseData(BlueContants.ERROR,"成功",new PageInfo(result));
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"失败",null);
        }
    }


}
