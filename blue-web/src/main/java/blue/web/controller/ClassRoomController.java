package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.erp.model.BlueClassroom;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.dao.service.BlueClassRoomService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "classRoom")
public class ClassRoomController {

    @Autowired
    private BlueClassRoomService blueClassRoomService;

    @RequestMapping("save")
    public ResponseData save(BlueClassroom blueClassroom){

       int result=blueClassRoomService.save(blueClassroom);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",null);
       }
    }

    @RequestMapping("deleteByIds")
    public ResponseData deleteGrades(String ids){

        int result=blueClassRoomService.deleteBatch(ids);

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }


    @RequestMapping("query")
    public PageResponseData query(Integer currentPage,Integer pageSize,BlueClassroom blueClassroom){
        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);

        List<BlueClassroom> blueClassroomList=blueClassRoomService.select(blueClassroom);

        if(CollectionUtils.isEmpty(blueClassroomList)){
            return new PageResponseData(BlueContants.SUCCESS,"成功",null);
        }
        PageInfo pageInfo = new PageInfo(blueClassroomList);

        return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);

    }

}
