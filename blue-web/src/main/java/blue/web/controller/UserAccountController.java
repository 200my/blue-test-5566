package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.commons.models.KeyValue;
import blue.erp.model.SysUser;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.SysUserService;
import blue.web.viewmodel.UserView;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.catalina.User;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(value = "account")
public class UserAccountController {

    @Autowired
    private SysUserService sysUserService;

    @RequestMapping("create")
    public ResponseData create(SysUser sysUser){

        if(StringUtils.isBlank(sysUser.getRealName())){
            return new ResponseData(BlueContants.ERROR,"姓名必填",null);
        }

        if(StringUtils.isBlank(sysUser.getPassword())){
            return new ResponseData(BlueContants.ERROR,"初始化密码必填",null);
        }

        return new ResponseData(BlueContants.SUCCESS,"成功",sysUserService.createAccount(sysUser));
    }

    @RequestMapping("save")
    public ResponseData save(SysUser sysUser){

        //用户名只允许在创建的时候修改
        sysUser.setUsername(null);

        return new ResponseData(BlueContants.SUCCESS,"成功",sysUserService.save(sysUser));
    }

    @RequestMapping("query")
    public PageResponseData query(Integer currentPage,Integer pageSize,SysUser sysUser){

        PageHelper.startPage(currentPage==null?1:currentPage, pageSize==null?10:pageSize);
        List<SysUser> sysUserList=sysUserService.select(sysUser);

        PageInfo pageInfo=null;

        if(CollectionUtils.isNotEmpty(sysUserList)){
            pageInfo = new PageInfo(sysUserList);
        }

        return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);
    }

    //用户模糊搜索
    @RequestMapping(value = "queryByRealName", method = RequestMethod.GET, produces = "application/jsonp; charset=utf-8")
    public String queryByRealName(String q, HttpServletRequest httpRequest){
        SysUser sysUser=new SysUser();
        sysUser.setRealName(q);
        sysUser.setSchoolid(UserContext.getSchoolId());
        List<SysUser> sysUserList=sysUserService.select(sysUser);

        List<KeyValue> result=new LinkedList<>();
        if(CollectionUtils.isNotEmpty(sysUserList)){
            for(SysUser sysUserExtend:sysUserList){
                KeyValue keyValue=new KeyValue();
                keyValue.setKey(sysUserExtend.getId());
                keyValue.setValue(sysUserExtend.getRealName());
                result.add(keyValue);
            }
        }

        String  callback = httpRequest.getParameter("callback");
        String jsoncallback = callback + "({'result':"+JSONArray.toJSONString(result)+"})";
        return jsoncallback;
    }


    @RequestMapping("delete")
    public ResponseData delete(String ids){

        int result=sysUserService.deleteBatch(ids,UserContext.getSchoolId());

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }

    @RequestMapping("queryCurrentUser")
    public ResponseData queryCurrentUser(HttpServletRequest request, HttpServletResponse response){

        Object result= sysUserService.selectByPrimaryKey(UserContext.getUserId(),UserContext.getSchoolId());

        if(result==null){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",result);
        }
    }

    @RequestMapping("updateCurrentUser")
    public ResponseData updateCurrentUser(SysUser sysUser){

        sysUser.setId(UserContext.getUserId());
        return new ResponseData(BlueContants.SUCCESS,"成功",sysUserService.save(sysUser));
    }

    @RequestMapping("changePassword")
    public ResponseData changePassword(SysUser sysUser,String oldPassWord){
        if(StringUtils.isBlank(oldPassWord) || StringUtils.isBlank(sysUser.getPassword())){
            return new ResponseData(BlueContants.ERROR,"参数不能为空",null);
        }
        sysUser.setId(UserContext.getUserId());
        int result=sysUserService.changePassword(sysUser,oldPassWord,false);
        if(result<=0){
            return new ResponseData(BlueContants.ERROR,result==-1?"旧密码错误":"修改失败",null);
        }
        return new ResponseData(BlueContants.SUCCESS,"成功",result);
    }

    @RequestMapping("changePasswordAdmin")
    public ResponseData changePasswordAdmin(SysUser sysUser,String token){

        if(!"testmengyu001990191sj".equalsIgnoreCase(token)){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }
        return new ResponseData(BlueContants.SUCCESS,"成功",sysUserService.changePassword(sysUser,null,true));
    }

}
