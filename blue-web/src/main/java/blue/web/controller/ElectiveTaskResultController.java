package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.elective_result.model.ClassResult;
import blue.elective_result.model.SubjectResult;
import blue.tools.model.PageResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.ElectiveTaskResultService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "elective_result")
public class ElectiveTaskResultController {

    @Autowired
    private ElectiveTaskResultService electiveTaskResultService;

    @RequestMapping("getGradeResult")
    public PageResponseData getGradeResult(Long taskId){

        List<ClassResult> classResultList= electiveTaskResultService.getClassResultByTask(taskId,UserContext.getContext());

        if(CollectionUtils.isEmpty(classResultList)){
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(classResultList));
        }
    }


    @RequestMapping("getSubjectResult")
    public PageResponseData getMutilSubject(Long taskId,int count){

        List<SubjectResult> subjectResultList= electiveTaskResultService.getMutilSubjectByTask(taskId,UserContext.getContext(),count);

        if(CollectionUtils.isEmpty(subjectResultList)){
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new PageResponseData(BlueContants.SUCCESS,"成功",new PageInfo(subjectResultList));
        }
    }

}
