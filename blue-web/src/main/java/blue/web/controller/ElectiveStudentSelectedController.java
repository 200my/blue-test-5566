package blue.web.controller;

import blue.tools.constants.BlueContants;
import blue.commons.enums.StatusEnum;
import blue.splitclass.model.ElectiveStudentSelected;
import blue.tools.model.PageResponseData;
import blue.tools.model.ResponseData;
import blue.web.commons.UserContext;
import blue.web.dao.service.ElectiveStudentSelectedService;
import blue.web.dao.service.ElectiveTaskResultService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "elective_student_selected")
public class ElectiveStudentSelectedController {

    @Autowired
    private ElectiveStudentSelectedService electiveStudentSelectedService;

    @Autowired
    private ElectiveTaskResultService electiveTaskResultService;

    @RequestMapping("save")
    public ResponseData save(ElectiveStudentSelected record){

        record.setStatus((byte) StatusEnum.Normal.getValue());

       int result=electiveStudentSelectedService.save(record);

       if(result==0){
           return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
       }else {
           return new ResponseData(BlueContants.SUCCESS,"成功",record.getId());
       }
    }

    @RequestMapping("saveSelected")
    public ResponseData saveSelected(String courseIds,Long taskId){

        if(StringUtils.isBlank(courseIds) || taskId==null){
            return new ResponseData(BlueContants.ERROR,"参数不合法",null);
        }

        ElectiveStudentSelected electiveStudentSelected=new ElectiveStudentSelected();
        electiveStudentSelected.setUserId(UserContext.getUserId());
        electiveStudentSelected.setSchoolid(UserContext.getSchoolId());
        electiveStudentSelected.setTaskId(taskId);
        electiveStudentSelected.setCourseIds(courseIds);
        electiveStudentSelected.setUserno(UserContext.getContext().getUserno());
        electiveStudentSelected.setUserName(UserContext.getContext().getRealName());
        electiveStudentSelected.setClassId(Long.decode(UserContext.getContext().getClassIds()));
        int result=electiveStudentSelectedService.save(electiveStudentSelected);

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"数据保存失败",null);
        }else {
            electiveTaskResultService.saveData(electiveStudentSelected,UserContext.getContext());
            return new ResponseData(BlueContants.SUCCESS,"成功",result);
        }
    }

    @RequestMapping("delete")
    public ResponseData delete(String ids){

        int result=electiveStudentSelectedService.deleteBatch(ids);

        if(result==0){
            return new ResponseData(BlueContants.ERROR,"失败",null);
        }else {
            return new ResponseData(BlueContants.SUCCESS,"成功",null);
        }
    }



    @RequestMapping("query")
    public PageResponseData query(ElectiveStudentSelected record){

        List<ElectiveStudentSelected> extendList= electiveStudentSelectedService.selectByObject(record,true);

        if(CollectionUtils.isNotEmpty(extendList)){
            PageInfo pageInfo = new PageInfo(extendList);
            return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);
        }else {
            return new PageResponseData(BlueContants.ERROR,"失败",null);
        }
    }

    @RequestMapping("test")
    public PageResponseData test(ElectiveStudentSelected record){
        List result=electiveStudentSelectedService.getCourseSelected(record.getTaskId(),record.getClassId(),record.getSchoolid(),true);

        PageInfo pageInfo = new PageInfo(result);
        return new PageResponseData(BlueContants.SUCCESS,"成功",pageInfo);
    }

}
