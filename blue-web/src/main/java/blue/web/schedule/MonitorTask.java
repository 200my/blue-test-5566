package blue.web.schedule;

import blue.erp.model.BlueSchool;
import blue.tools.commons.IpUtils;
import blue.tools.constants.BlueContants;
import blue.web.cache.service.RedisService;
import blue.web.dao.service.BlueSchoolService;
import blue.web.monitor.DingTalkService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class MonitorTask {

    @Autowired
    private RedisService redisService;

    @Autowired
    private DingTalkService dingTalkService;

    @Autowired
    private BlueSchoolService blueSchoolService;

    @Scheduled(cron="0 */10 * * * ?")
    private void redisHealth(){
        Object cacheObject=null;
        try {
            cacheObject=redisService.getValue("redisHealth");
            if(cacheObject==null){
                redisService.setValue("redisHealth","health",BlueContants.month);
                cacheObject=redisService.getValue("redisHealth");
            }
        }catch (Exception e){

        }finally {
            if(cacheObject==null){
                dingTalkService.sendMsg2Dingtalk(String.format("机器 %s 的 redis 挂了！！！！！，赶快看一下", IpUtils.getLocalIp()));
            }
        }
    }

    @Scheduled(cron="0 */10 * * * ?")
    private void dbHealth(){
        List result=new LinkedList();
        try {
            BlueSchool blueSchool=new BlueSchool();
            blueSchool.setName("heathTest");

            result=blueSchoolService.query(blueSchool);
            if(CollectionUtils.isEmpty(result)){
                blueSchool.setUsernameSuffix("heathTest");
                blueSchoolService.save(blueSchool);
                result=blueSchoolService.query(blueSchool);
            }

        }catch (Exception e){

        }finally {
            if(CollectionUtils.isEmpty(result)){
                dingTalkService.sendMsg2Dingtalk(String.format("机器 %s 的 db 挂了！！！！！，赶快看一下", IpUtils.getLocalIp()));
            }
        }
    }

}
