package blue.web.schedule;

import blue.tools.constants.BlueContants;
import blue.web.cache.service.RedisService;
import blue.web.dao.service.BlueClassService;
import blue.web.dao.service.WorkStudentQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
@Lazy
@Component
public class SchedulerTask {

    @Lazy
    @Autowired
    private WorkStudentQuestionService workStudentQuestionService;

    @Autowired
    private BlueClassService blueClassService;

    @Autowired
    private RedisService redisService;

    private final String checkClassInfo="SchedulerTask-checkClassInfo";

    //学生的属性字段数据补充,有保存数据激发的时候，十分钟扫描一次，否则 一个小时扫描一次
    @Scheduled(cron="0 */10 * * * ?")
    private void scanAddAttributes(){
        if(redisService.getValue(BlueContants.prefix_timertask_studentquestion)!=null){

            return;
        }
        workStudentQuestionService.scanAddAttributes(1000);
    }

    //校验班级人数是否发生变化,每天1点触发一次
    @Scheduled(cron="0 0 1 * * ?")
    private void checkClassInfo(){
        if(redisService.getValue(checkClassInfo)!=null){
            return;
        }
        redisService.setValue(checkClassInfo,"run",BlueContants.hour);

        blueClassService.sanClassTable();
    }

}
